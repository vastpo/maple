import datetime
import os

dirname = 'db/migrate/'
delimiter = '_'
format = '%Y%m%d%H%M%S'
migration_scripts = []
result = []

# user inputted variables
cutoff = ''
sec, min, hour, day = None, None, None, None

print("Please specify cut-off time (" + format + "), only migrations created on or after cut-off time will be processed [all]:", end=' ')

while True:
  try:
    cutoff = input()
    if (cutoff.lower() not in ['', 'all']):
      cutoff = datetime.datetime.strptime(cutoff, format)

    for entry in os.scandir(dirname):
      if entry.is_file():
        timestamp, migration_name = entry.name.split(delimiter, 1)
        old_datetime = datetime.datetime.strptime(timestamp, format)

        if (isinstance(cutoff, datetime.datetime) and old_datetime < cutoff):
          continue

        migration_scripts.append(entry.name)

    if len(migration_scripts) == 0:
      raise Exception("No migration script to be renamed on or after " + cutoff.strftime(format))

    break

  except (ValueError, Exception) as e:
    print("\n" + str(e) + ", please enter again [all]:", end=' ')


print("Please input time delta [sec min hour day]:", end=' ')
sec, min, hour, day = input().split(' ')

for entry in migration_scripts:
  timestamp, migration_name = entry.split(delimiter, 1)
  old_datetime = datetime.datetime.strptime(timestamp, format)
  new_datetime = old_datetime + datetime.timedelta(
    seconds = int(sec),
    minutes = int(min),
    hours = int(hour),
    days = int(day)
  )
  new_filename = new_datetime.strftime(format) + delimiter + migration_name
  result.append({'old': entry, 'new': new_filename})

result.sort(key=lambda r:r['old'])

print("The migration files will be renamed as followings:\n")

for entry in result:
  print(entry['old'] + ' => ' + entry['new'])

print("\nContinue [yN] ?", end=' ')


if (input() == 'y'):
  for entry in result:
    os.rename(dirname + entry['old'], dirname + entry['new'])
else:
  print('Aborted...')
