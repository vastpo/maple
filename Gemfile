source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end


# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.2.3'
# Use postgresql as the database for Active Record
gem 'pg', '~> 0.21.0'
# Use Puma as the app server
gem 'puma', '~> 3.7'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
# gem 'jbuilder', '~> 2.5'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 4.0'
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# Use Rack CORS for handling Cross-Origin Resource Sharing (CORS), making cross-origin AJAX possible
# gem 'rack-cors'

gem 'aasm'
gem 'active_record-acts_as'
gem 'activerecord_json_validator'
gem 'amqp'
gem 'bcrypt'
gem 'breadcrumbs_on_rails'
gem 'bugsnag'
gem 'bunny'
gem 'coffee-rails'
gem 'coffee_routes'
gem 'composite_primary_keys'
gem 'countries'
gem 'delayed_job_active_record'
gem 'dotenv-rails'
gem 'execjs'
gem 'faker', :git => 'https://github.com/stympy/faker.git', :branch => 'master'
gem 'firebase'
gem 'streamio-ffmpeg'
gem 'fides'
gem 'font-awesome-rails'
gem 'grape'
gem 'grape-entity'
gem 'grape-route-helpers'
gem 'haml-rails'
gem 'indefinite_article'
gem 'jwt'
gem 'json'
gem 'kaminari'
gem 'money'
gem 'mongoid'
gem 'nest'
gem 'orientdb4r'
gem 'paper_trail'
gem 'paper_trail-association_tracking'
gem 'pry-rails'
gem 'pundit'
gem 'rack-cors', :require => 'rack/cors'
gem 'rails-i18n'
gem 'recursive-open-struct'
gem 'redis-namespace'
gem 'redis-rack-cache'
gem 'redcarpet'
gem 'redis-rails'
gem 'resque'
gem 'resque_mailer'
gem 'rpush'
gem 'request_store'
gem 'sassc-rails'
gem 'settingslogic'
gem 'simple_command'
gem 'sneakers'
gem 'stripe', git: 'https://github.com/stripe/stripe-ruby'
gem 'therubyracer'
gem 'workless', '~> 2.2.0'
gem 'uglifier'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  gem 'capistrano', '~> 3.6'
  gem 'capistrano-rails', '~> 1.3'
  gem 'capybara'
  gem 'factory_bot_rails'
  gem 'grape-swagger'
  gem 'grape-swagger-rails'
  gem 'grape-swagger-ui'
  gem 'rails-controller-testing'
  gem 'rspec-rails', '~> 3.5'
  gem 'selenium-webdriver'
end

group :development do
  gem 'listen', '>= 3.0.5', '< 3.2'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

group :test do
  gem 'shoulda-matchers', '~> 3.1'
  gem 'database_cleaner'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
