group, subgroup = nil, nil
output = File.open('db/emoji-seed.rb', 'w')

File.readlines('emoji-test.txt').each do |line|
  group = line.split(':').last.strip if /^(# group)/ =~ line
  subgroup = line.split(':').last.strip if /^(# subgroup)/ =~ line

  if !((/^(# subgroup)/ =~ line) || (/^(# group)/ =~ line))
    # if subgroup == 'keycap' && /^[0-9A-F].+;.+#/ =~ line
    if /^[0-9A-F].+;.+#/ =~ line
      code, status, emoji_name = line.split(/[;#]/)
      emoji, name = emoji_name.split(' ', 2)
      output.puts "Emoji.create!(group: '#{group}', subgroup: '#{subgroup}', code: '#{code.strip}', status: '#{status.strip}', emoji: '#{emoji}', name: '#{name&.strip}')"
      # Emoji.create!(
      #   group: group,
      #   subgroup: subgroup,
      #   code: code.strip,
      #   status: status.strip,
      #   emoji: emoji,
      #   name: name
      # )
    end
  end
end

output.close
