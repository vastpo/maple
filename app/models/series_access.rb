class SeriesAccess < ApplicationRecord
  belongs_to :partner
  belongs_to :series


  # CRUD manipulation
  include SeriesAccessConcerns::CRUDConcern

end
