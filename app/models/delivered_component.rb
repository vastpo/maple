class DeliveredComponent < ApplicationRecord
  belongs_to :page_session


  # CRUD manipulation
  include DeliveredComponentConcerns::CRUDConcern

end
