class Series < ApplicationRecord

  attr_accessor :partner_id

  belongs_to :created_by, polymorphic: true
  belongs_to :parent, class_name: 'Series', optional: true

  has_many :attached_media, as: :attachable, dependent: :destroy
  has_many :engagements, as: :component, dependent: :destroy
  has_many :posts_series, dependent: :destroy
  has_many :series_access, dependent: :destroy
  has_many :sub_series, class_name: 'Series', foreign_key: :parent_id, dependent: :nullify

  has_and_belongs_to_many :posts, dependent: :destroy

  JSON_ATTRS = [:title, :short_note, :long_note, :color_scheme, :media_details, :interaction_details, :score_details]
  LOCALE_ATTRS = [:title, :short_note, :long_note]
  include JSONConcern

  # def get_hierarchy(init=[self.id])
  #   begin
  #     _parent = Series.find(init.last).parent
  #     if _parent.present?
  #       return get_hierarchy(init << _parent.id)
  #     else
  #       return init
  #     end
  #   rescue StandardError => e
  #     return e.message
  #   end
  # end

  # states manipulation
  include SeriesConcerns::AASMConcern

  # CRUD manipulation
  include SeriesConcerns::CRUDConcern

  def root
    Series.find(self.hierarchy.last)
  end

  # Relation chain methods
  class ActiveRecord_Relation
    def root
      Series.collection_root(self)
    end
  end
  class ActiveRecord_AssociationRelation
    def root
      Series.collection_root(self)
    end
  end
  class ActiveRecord_Associations_CollectionProxy
    def have_posts
      Series.filter_have_posts(self)
    end
  end

  def self.collection_root(series, state: STATE_PUBLISHED)
    Series.where(id: series.map { |s| s.state == state ? s.hierarchy.last : nil }.flatten.compact.uniq, state: state)
  end

  def self.filter_have_posts(series, state: STATE_PUBLISHED)
    Series.where(id: series.map{ |s| (s.state == state && s.posts.where(state: Post::STATE_PUBLISHED).present?) ? s.id : nil }.flatten.compact.uniq, state: state)
  end

end
