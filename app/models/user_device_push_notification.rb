class UserDevicePushNotification < ApplicationRecord
  self.primary_keys = :user_id, :device_id, :service
  belongs_to :user, optional: true
  belongs_to :device
  belongs_to :user_device, class_name: 'UsersDevices', foreign_key: UsersDevices.primary_keys
end
