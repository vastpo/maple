class Post < ApplicationRecord
  belongs_to :created_by, polymorphic: true
  belongs_to :managed_by, class_name: 'Partner'

  has_many :active_cards_posts, ->{ where(is_active: true) }, class_name: 'CardsPosts', dependent: :destroy
  has_many :active_cards, ->{ where(state: Card::STATE_PUBLISHED) }, class_name: 'Card', through: :active_cards_posts, source: :card
  has_many :active_posts_series, ->{ where(is_active: true) }, class_name: 'PostsSeries', dependent: :destroy
  has_many :active_series, ->{ where(state: Series::STATE_PUBLISHED) }, class_name: 'Series', through: :active_posts_series, source: :series do def root; Series.collection_root(self) end end
  has_many :active_thunder_offers, ->{ where(is_active: true) }, class_name: 'ThunderOffer', source: :thunder_offers
  has_many :attached_emojis, dependent: :destroy
  has_many :attached_media, as: :attachable, dependent: :destroy
  has_many :attached_widgets, dependent: :destroy
  has_many :cards_posts, class_name: 'CardsPosts', dependent: :destroy
  has_many :columns_posts, class_name: 'ColumnsPosts', dependent: :destroy
  has_many :delivery_locations, as: :deliverable, dependent: :destroy
  has_many :engagements, as: :component, dependent: :destroy
  has_many :posts_series, class_name: 'PostsSeries', dependent: :destroy
  has_many :posts_tags, class_name: 'PostsTags', dependent: :destroy
  has_many :thunder_offers, dependent: :nullify

  has_and_belongs_to_many :cards, dependent: :destroy
  has_and_belongs_to_many :columns, dependent: :destroy
  has_and_belongs_to_many :tags, dependent: :destroy
  has_and_belongs_to_many :series do def root; Series.collection_root(self) end end

  JSON_ATTRS = [:title, :note, :widget_details, :media_details, :interaction_details, :score_details]
  LOCALE_ATTRS = [:title, :note]
  include JSONConcern


  # states manipulation
  include PostConcerns::AASMConcern

  # CRUD manipulation
  include PostConcerns::CRUDConcern

end
