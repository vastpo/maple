# (1..1500).to_a.each { post_id = Post.where(managed_by_id: 4).pluck(:id).sample; Comment.create!(created_by: User.find(1), post_id: post_id, parent_id: (rand > 0.25 ? Comment.where(post_id: post_id).pluck(:id).sample : nil), external_id: Digest::SHA256.hexdigest(Time.now.to_f.to_s), content: Faker::Lorem.paragraph(5)) }

class Comment < ApplicationRecord
  belongs_to :created_by, polymorphic: true
  belongs_to :post
  belongs_to :parent, class_name: 'Comment', optional: true

  has_many :sub_comments, as: :parent, class_name: 'Comment'

  JSON_ATTRS = [:score_details]
  LOCALE_ATTRS = []
  include JSONConcern


  # CRUD manipulation
  include CommentConcerns::CRUDConcern

end
