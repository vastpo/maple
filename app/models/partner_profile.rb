class PartnerProfile < ApplicationRecord
  belongs_to :partner

  has_many :attached_media, as: :attachable, dependent: :destroy
  has_many :engagements, as: :component, dependent: :destroy

  JSON_ATTRS = [:display_name, :short_note, :long_note, :links, :media_details, :interaction_details, :score_details]
  LOCALE_ATTRS = [:display_name, :short_note, :long_note]
  include JSONConcern

  # states manipulation
  include PartnerProfileConcerns::AASMConcern

  # CRUD manipulation
  include PartnerProfileConcerns::CRUDConcern

end
