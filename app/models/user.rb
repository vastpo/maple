class User < ApplicationRecord
  has_secure_password

  has_one :api_key, as: :owner, dependent: :destroy
  has_one :partner_admin, dependent: :destroy

  has_many :active_users_devices, ->{ where(is_active: true) }, class_name: 'UsersDevices', dependent: :destroy
  has_many :active_devices, class_name: 'Device', through: :active_users_devices, source: :device
  has_many :attached_emojis, dependent: :destroy
  has_many :delivery_inventories, dependent: :destroy
  has_many :engagements, dependent: :destroy
  has_many :message_deliveries, foreign_key: :recipient_id, dependent: :destroy
  has_many :page_sessions, dependent: :destroy
  has_many :received_messages, class_name: 'Message', dependent: :destroy, through: :message_deliveries, source: :message
  has_many :sent_messages, class_name: 'Message', dependent: :destroy, foreign_key: :sent_by_id
  has_many :thunder_transactions, dependent: :destroy

  has_and_belongs_to_many :devices, join_table: :users_devices, dependent: :destroy, optional: true

  validates_uniqueness_of :login

  JSON_ATTRS = [:emails, :connected_profiles, :partner_admin_details]
  LOCALE_ATTRS = []
  include JSONConcern

  # states manipulation
  include UserConcerns::AASMConcern

  # CRUD manipulation
  include UserConcerns::CRUDConcern

  # trend = :in  -> Friends from other users to current user
  # trend = :out -> Friends from current user to other users
  def friends(trend, state)
    trends = {
      in:   { fnE: :in,   fnV: :out  },
      out:  { fnE: :out,  fnV: :in   },
      both: { fnE: :both, fnV: :both }
    }
    sql = "SELECT * FROM (SELECT expand(#{trends[trend][:fnE]}E('Friendship')[state = '#{state}'].#{trends[trend][:fnV]}V()) FROM User WHERE id = #{self.id}) WHERE id <> #{self.id}"
    friend_user_ids = OrientDB
      .query(sql)
      .deep_symbolize_keys
      .pluck(:id)
    return [] if friend_user_ids.empty?
    return User.where(id: friend_user_ids)
  end

  def mutual_friends(user_id)
    sql = "SELECT * FROM (SELECT expand(friends) FROM (MATCH {class: User, where: (id=#{self.id})}.both('Friendship'){as: friends}.(bothE('Friendship'){where: (state='accepted')}.bothV()){class: User, where: (id=#{user_id})} RETURN friends)) WHERE id <> #{user_id}"
    mutual_friend_user_ids = OrientDB
      .query(sql)
      .deep_symbolize_keys
      .pluck(:id)
    return [] if mutual_friend_user_ids.empty?
    return User.where(id: mutual_friend_user_ids)
  end

  def is_friend?(user_id, state)
    return false if self.id == user_id
    get_friendship(user_id, state: state).present?
  end

  def get_friendship(user_id, state: nil)
    filters = ["((in.id = #{self.id} AND out.id = #{user_id}) OR (in.id = #{user_id} AND out.id = #{self.id}))"]
    filters << "state = '#{state}'" if state.present?
    sql = "SELECT FROM Friendship WHERE #{filters.join(' AND ')}"
    OrientDB.query(sql).first
  end

end
