class PageSession < ApplicationRecord
  belongs_to :device, optional: true
  belongs_to :user, optional: true

  has_many :delivered_components, dependent: :destroy
  has_many :engagements, dependent: :destroy

  JSON_ATTRS = [:component_details]
  LOCALE_ATTRS = []
  include JSONConcern

  # CRUD manipulation
  include PageSessionConcerns::CRUDConcern

end
