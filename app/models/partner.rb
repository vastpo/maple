class Partner < ApplicationRecord
  has_secure_password

  has_one :active_payout_policy, -> { where(is_active: true) }, class_name: 'PartnerPayoutPolicy', dependent: :destroy
  has_one :active_content_policy, -> { where(is_active: true) }, class_name: 'PartnerContentPolicy', dependent: :destroy
  has_one :api_key, as: :owner, dependent: :destroy
  has_one :profile, class_name: 'PartnerProfile', dependent: :destroy

  has_many :access_invitations, class_name: 'PartnerAccessInvitation', dependent: :destroy
  has_many :partner_access, dependent: :destroy
  has_many :partner_managements, dependent: :destroy
  has_many :payout_policies, class_name: 'PartnerPayoutPolicy', dependent: :destroy
  has_many :content_policies, class_name: 'PartnerContentPolicy', dependent: :destroy
  has_many :posts, foreign_key: :managed_by_id, dependent: :destroy
  has_many :widgets, foreign_key: :managed_by_id, dependent: :destroy
  has_many :cards, foreign_key: :managed_by_id, dependent: :destroy
  has_many :series_access, dependent: :destroy
  has_many :thunder_offers, dependent: :destroy

  has_and_belongs_to_many :managing_series, class_name: 'Series', join_table: :series_access
  # has_many :media, as: :managed_by, dependent: :destroy
  # has_many :images, as: :managed_by, dependent: :destroy
  # has_many :videos, as: :managed_by, dependent: :destroy

  validates_uniqueness_of :login

  JSON_ATTRS = [:display_name]
  LOCALE_ATTRS = [:display_name]
  include JSONConcern

  # states manipulation
  include PartnerConcerns::AASMConcern

  # CRUD manipulation
  include PartnerConcerns::CRUDConcern
end
