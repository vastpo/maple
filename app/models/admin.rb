class Admin < ApplicationRecord
  has_secure_password

  has_one :api_key, as: :owner, dependent: :destroy
  has_one :profile, class_name: 'AdminProfile', dependent: :destroy

  has_many :partner_access_invitations, as: :invited_by, dependent: :destroy
  has_many :partner_managements, dependent: :destroy
  has_many :managing_partners, through: :partner_managements, source: :partner, dependent: :destroy

  validates_uniqueness_of :login

  JSON_ATTRS = [:roles]
  LOCALE_ATTRS = []
  include JSONConcern

  # states manipulation
  include AdminConcerns::AASMConcern

  # CRUD manipulation
  include AdminConcerns::CRUDConcern

  ROLES = [
    :super_admin,
    :account_manager,
    :account_executive,
    :viewer
  ]
  ROLES.each do |role|
    define_method "is_#{role}?" do
      roles.include? role.to_s
    end
  end
end
