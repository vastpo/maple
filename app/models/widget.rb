class Widget < ApplicationRecord
  belongs_to :created_by, polymorphic: true
  belongs_to :managed_by, class_name: 'Partner'

  has_many :attached_media, as: :attachable, dependent: :destroy
  has_many :attached_widgets, dependent: :destroy
  has_many :delivery_locations, as: :deliverable, dependent: :destroy
  has_many :engagements, as: :component, dependent: :destroy

  JSON_ATTRS = [:title, :headline, :conversion_text, :color_scheme, :media_details, :interaction_details, :score_details]
  LOCALE_ATTRS = [:title, :headline, :conversion_text]
  include JSONConcern

  # states manipulation
  include WidgetConcerns::AASMConcern

  # CRUD manipulation
  include WidgetConcerns::CRUDConcern

end
