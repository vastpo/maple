class Friendship
  include AASM

  aasm do
    state :requested, initial: true
    state :accepted
  end

  def initialize(json)
    @state
  end

  def self.find(user1_id, user2_id)
    $orientdb.query("SELECT FROM Friendship WHERE (in.id = #{user1_id} AND out.id = #{user2_id}) OR (in.id = #{user2_id} AND out.id = #{user1_id})")
  end
end
