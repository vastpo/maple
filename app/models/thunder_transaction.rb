class ThunderTransaction < ApplicationRecord
  belongs_to :thunder_offer
  belongs_to :user, optional: true


  # CRUD manipulation
  include ThunderTransactionConcerns::CRUDConcern

end
