class CardQuestionResponse < ApplicationRecord

  belongs_to :question, class_name: 'CardQuestion'
  belongs_to :user, optional: true
  belongs_to :device

  JSON_ATTRS = [:response]
  LOCALE_ATTRS = []
  include JSONConcern

  # CRUD manipulation
  include CardQuestionResponseConcerns::CRUDConcern

end
