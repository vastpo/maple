class Tag < ApplicationRecord
  has_many :engagements, as: :component, dependent: :destroy
  has_many :posts_tags, class_name: 'PostsTags', dependent: :destroy

  has_and_belongs_to_many :posts, dependent: :destroy

  JSON_ATTRS = [:color_scheme, :interaction_details, :score_details]
  LOCALE_ATTRS = []


  # CRUD manipulation
  include TagConcerns::CRUDConcern

end
