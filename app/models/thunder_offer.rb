class ThunderOffer < ApplicationRecord
  belongs_to :created_by, polymorphic: true
  belongs_to :partner
  belongs_to :post, optional: true

  has_many :thunder_transactions, dependent: :destroy

  JSON_ATTRS = [:amount, :fee, :note]
  LOCALE_ATTRS = [:note]
  include JSONConcern


  # CRUD manipulation
  include ThunderOfferConcerns::CRUDConcern

end
