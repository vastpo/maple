class Capture < ApplicationRecord
  belongs_to :message, optional: true
  belongs_to :video


  # CRUD manipulation
  include CaptureConcerns::CRUDConcern

end
