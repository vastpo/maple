class PartnerManagement < ApplicationRecord
  belongs_to :admin
  belongs_to :partner

  # CRUD manipulation
  include PartnerManagementConcerns::CRUDConcern

end
