class DeliveryLocation < ApplicationRecord
  belongs_to :deliverable, polymorphic: true


  # CRUD manipulation
  include DeliveryLocationConcerns::CRUDConcern

end
