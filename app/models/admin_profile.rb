class AdminProfile < ApplicationRecord
  belongs_to :admin

  JSON_ATTRS = [:phones, :emails, :role_details]
  LOCALE_ATTRS = []
  include JSONConcern


  # CRUD manipulation
  include AdminProfileConcerns::CRUDConcern

end
