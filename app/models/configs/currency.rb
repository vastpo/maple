class Configs::Currency < ApplicationRecord
  def method_missing(method_name, *args, &block)
    Money::Currency.new(self.iso_code).public_send(method_name, *args)
  end
end
