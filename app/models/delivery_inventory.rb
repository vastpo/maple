class DeliveryInventory < ApplicationRecord
  belongs_to :device, optional: true
  belongs_to :user, optional: true

  JSON_ATTRS = [:component_details]
  LOCALE_ATTRS = []
  include JSONConcern

  # CRUD manipulation
  include DeliveryInventoryConcerns::CRUDConcern

end
