class Engagement < ApplicationRecord
  belongs_to :component, polymorphic: true
  belongs_to :device, optional: true
  belongs_to :media, optional: true
  belongs_to :page_session
  belongs_to :user, optional: true

  JSON_ATTRS = [:media_behaviors, :comment_behaviors, :emoji_behaviors, :thunder_behaviors, :rpe_claim_details]
  LOCALE_ATTRS = []
  include JSONConcern


  # CRUD manipulation
  include EngagementConcerns::CRUDConcern

end
