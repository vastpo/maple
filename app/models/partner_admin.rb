class PartnerAdmin < ApplicationRecord
  has_secure_password

  belongs_to :user, optional: true

  has_one :api_key, as: :owner, dependent: :destroy

  has_many :partner_access, dependent: :destroy
  has_and_belongs_to_many :managing_partners, class_name: 'Partner', join_table: :partner_access
  has_many :managing_series, class_name: 'Series', through: :managing_partners

  validates_uniqueness_of :login

  JSON_ATTRS = [:access_details]
  LOCALE_ATTRS = []
  include JSONConcern

  # states manipulation
  include PartnerAdminConcerns::AASMConcern

  # CRUD manipulation
  include PartnerAdminConcerns::CRUDConcern

end
