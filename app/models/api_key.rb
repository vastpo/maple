class APIKey < ApplicationRecord
  self.table_name = 'api_rsa_keys'
  belongs_to :owner, polymorphic: true

  validates_uniqueness_of :access_key, allow_blank: true, allow_nil: true
  # validates_uniqueness_of :secret_key, allow_blank: true, allow_nil: true
  validates_uniqueness_of :rsa_public_key, allow_blank: true, allow_nil: true
  validates_uniqueness_of :rsa_private_key, allow_blank: true, allow_nil: true

  # CRUD manipulation
  include APIKeyConcerns::CRUDConcern

end
