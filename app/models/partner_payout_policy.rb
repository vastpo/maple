class PartnerPayoutPolicy < ApplicationRecord
  belongs_to :partner


  JSON_ATTRS = [:fee_rate, :fee_fix]
  LOCALE_ATTRS = []
  include JSONConcern


  # CRUD manipulation
  include PartnerPayoutPolicyConcerns::CRUDConcern

end
