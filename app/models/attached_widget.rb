class AttachedWidget < ApplicationRecord
  belongs_to :post
  belongs_to :widget


  # CRUD manipulation
  include AttachedWidgetConcerns::CRUDConcern

end
