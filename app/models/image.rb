class Image < ApplicationRecord
  acts_as :media, as: :media

  # CRUD manipulation
  include ImageConcerns::CRUDConcern

end
