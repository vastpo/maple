class MessageEntry
  include MongoidConcern

  field :sender_id, type: Integer
  field :recipient_type, type: String
  field :recipient_id, type: Integer
  field :group_id, type: Integer
  field :metadata, type: Hash
  field :body, type: Hash
  field :sent_at, type: DateTime
  field :read_at, type: DateTime
  field :deleted_at, type: DateTime

  belongs_to :quote_entry, class_name: 'MessageEntry', optional: true

  def sender
    User.find(self.sender_id)
  end

  def recipient
    self.recipient_type.constantize.find(self.recipient_id)
  end

end

# (1..10000).each do
#   m = MessageEntry.new(
#     sender_id: rand(1..20),
#     recipient_type: 'User',
#     recipient_id: rand(1..20),
#     quote_entry_id: nil,
#     group_id: nil,
#     metadata: {},
#     body: { text: Faker::Lorem.sentence(rand(5..8)) },
#     sent_at: Time.current,
#     read_at: Time.current,
#     deleted_at: nil
#   )
#   if rand(1..10) > 6
#     prev_m = MessageEntry.and(
#       recipient_type: m.recipient_type,
#       '$or' => [
#         {sender_id: m.sender_id, recipient_id: m.recipient_id},
#         {sender_id: m.recipient_id, recipient_id: m.sender_id}
#       ]
#     )
#     m.assign_attributes(quote_entry: prev_m.sample)
#   end
#   m.save!
# end
