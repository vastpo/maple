class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true
  after_destroy_commit { |record| ActiveRecord::Base.connection.reset_pk_sequence!(record.class.table_name) }

  EMPTY_LOCALE_OBJECT = I18n.available_locales.inject({}) { |obj, locale| obj[locale] = nil; obj }

  def self._index(params)
    result = self.all

    if params[:where].present?
      result = result.where(params[:where])
    end

    if params[:order].present?
      result = result.order(params[:order])
    end

    total_items = result.length
    page = 1
    items_per_page = total_items

    if params[:pagination].present? && params[:pagination][:per]&.zero?
      return {
        total_items: total_items
      }
    end

    if params[:pagination].present?
      items_per_page = params[:pagination][:per] || Kaminari.config.default_per_page
      # page = [params[:pagination][:page].to_i || 1, (total_items * 1.0 / items_per_page).ceil].min
      page = params[:pagination][:page].to_i || 1
      result = result.page(page).per(items_per_page)
      total_pages = result.total_pages
    else
      items_per_page = Kaminari.config.default_per_page
      page = 1
      result = result.page(page).per(items_per_page)
      total_pages = result.total_pages
    end

    if params[:as_json].present?
      result = result.as_json(params[:as_json]).deep_symbolize_keys
    end

    return {
      result: result,
      items_per_page: items_per_page,
      page: page,
      total_items: total_items,
      total_pages: total_pages
    }
  end

  def serializable_hash(options = nil)
    options ||= {}

    attribute_names = attributes.keys
    if only = options[:only]
      attribute_names &= Array(only).map(&:to_s)
    elsif except = options[:except]
      attribute_names -= Array(except).map(&:to_s)
    end

    hash = {}
    attribute_names.each { |n| hash[n] = read_attribute_for_serialization(n) }

    Array(options[:methods]).each do |m|
      hash[m.to_s] = send(m)
    end

    serializable_add_includes(options) do |association, records, opts|
      # ****************** Additional Options ******************
      if opts[:chain].present? # when collection, class ActiveRecord_AssociationRelation
        opts[:chain].each { |method| records = records.public_send(method) }
      end
      if opts[:where].present?
        records = records.where(opts[:where])
      end
      if opts[:order].present?
        records = records.order(opts[:order])
      end
      if opts[:limit].present?
        records = records.limit(opts[:limit])
      end
      # ------------------ Additional Options ------------------
      hash[association.to_s] = if records.respond_to?(:to_ary)
        records.to_ary.map { |a| a.serializable_hash(opts) }
      else
        records.serializable_hash(opts)
      end
    end

    hash.deep_symbolize_keys
  end

  def self.__reflections(kls:)
    return self._reflections.select{ |k, r| (r.try(:delegate_reflection) || r).is_a?("ActiveRecord::Reflection::#{kls}".constantize) }.keys.map(&:to_sym)
  end

  def self._attrs
    return (self.attribute_names + self._reflections.keys).map(&:to_sym)
  end

  def self._belongs_to
    return self.__reflections(kls: :BelongsToReflection)
  end

  def self._has_many
    return self.__reflections(kls: :HasManyReflection)
  end

  def sync_media_details
    return nil if !self.class._has_many.include?(:attached_media)
    result = attached_media.as_json(include: { media: { include: :specific }}).deep_symbolize_keys.each { |am|
      am[:media].except!(:created_at, :updated_at).merge!(am[:media][:specific]).except!(:specific, :id)
      am.merge!(am[:media]).except!(*Media::LEGAL_ATTRS).except!(:media, :created_at, :updated_at)
    }.inject({}) { |hash, am|
      hash[am[:attached_for]] = am.extract!(:external_id, :urls, :width, :height, :length)
      hash
    }
    assign_attributes(media_details: result)
    return self
  end

  def sync_media_details!
    sync_media_details&.save!
  end
end
