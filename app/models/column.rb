class Column < ApplicationRecord
  belongs_to :created_by, polymorphic: true

  has_many :attached_media, as: :attachable, dependent: :destroy
  has_many :columns_posts, class_name: 'ColumnsPosts', dependent: :destroy
  has_many :delivery_locations, as: :deliverable, dependent: :destroy
  has_many :engagements, as: :component, dependent: :destroy

  has_and_belongs_to_many :posts, dependent: :destroy
  
  JSON_ATTRS = [:title, :short_note, :long_note, :color_scheme, :media_details, :interaction_details, :score_details]
  LOCALE_ATTRS = [:title, :short_note, :long_note]
  include JSONConcern

  # states manipulation
  include ColumnConcerns::AASMConcern

  # CRUD manipulation
  include ColumnConcerns::CRUDConcern

end
