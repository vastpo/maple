module PartnerAccessInvitationConcerns
  module AASMConcern
    extend ActiveSupport::Concern

    included do
      include AASM

      def state
        self[:state]&.to_sym
      end

      aasm column: :state, enum: false do
        state :invited, initial: true
        state :accepted, :expired

        event :accept do
          transitions from: PartnerAccessInvitation::STATE_INVITED, to: PartnerAccessInvitation::STATE_ACCEPTED
        end

        event :expire do
          transitions from: PartnerAccessInvitation::STATE_INVITED, to: PartnerAccessInvitation::STATE_EXPIRED
        end
      end
    end
  end
end
