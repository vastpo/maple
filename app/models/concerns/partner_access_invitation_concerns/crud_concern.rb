module PartnerAccessInvitationConcerns
  module CRUDConcern
    extend ActiveSupport::Concern

    included do
      # ================ index ================
      def self._index(params)
        super(params)
      end

      # ================ show ================
      def self._show(params)
      end

      # ================ create ================
      def self._create!(params)
        params.merge!(
          partner: Partner.find(params[:partner_id]), 
          expired_at: 72.hours.from_now
        )
        token = Digest::SHA512.hexdigest(JSON.generate(params))
        params.merge!(token: token)

        accepted_invitations = self._index(partner_id: params[:partner_id], email: params[:email], state: :accepted)
        raise Exception.new "Invitation accepted" if (!accepted_invitations.empty?)

        ActiveRecord::Base.transaction do
          self._index(partner_id: params[:partner_id], email: params[:email], state: :invited).each{ |i| i.expire! }
          self.create!(params)
        end
      end

      # ================ update ================
      def _update!(params)
      end

      # ================ destroy ===============
      def _destroy(params)
      end
    end
  end
end
