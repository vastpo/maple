module DeviceConcerns
  module CRUDConcern
    extend ActiveSupport::Concern

    included do
      # ================ index ================
      def self._index(params)
      end

      # ================ show ================
      def self._show(params)
        self.find_by(device_id: params[:device_id])
      end

      # ================ create ================
      def self._create!(params)
        device = self.create!(params.slice(*Device._attrs))
        device
      end

      # ================ update ================
      def _update!(params)
      end

      # ================ destroy ===============
      def _destroy(params)
      end
    end
  end
end
