module PartnerAccessConcerns
  module CRUDConcern
    extend ActiveSupport::Concern

    included do
      # ================ index ================
      def self._index(params)
      end

      # ================ show ================
      def self._show(params)
      end

      # ================ create ================
      def self._create!(params)
      end

      # ================ update ================
      def _update!(params)
        ActiveRecord::Base.transaction do
          if params[:is_default] == true
            self.class.where(partner_admin_id: self.partner_admin_id).update_all(is_default: false)
          end
          self.update!(params)
          if Rails.env.production?
            { status: :ok }
          else
            self
          end
        end
      end

      # ================ destroy ===============
      def _destroy(params)
      end
    end
  end
end
