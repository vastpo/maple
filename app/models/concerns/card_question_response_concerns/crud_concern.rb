module CardQuestionResponseConcerns
  module CRUDConcern
    extend ActiveSupport::Concern

    included do
      # ================ index ================
      def self._index(params)
        super(params)
      end

      # ================ show ================
      def self._show(params)
        self.find_by(id: params[:id])
      end

      # ================ create ================
      def self._create!(params)
        ActiveRecord::Base.transaction do
          response = self.create!(params.slice(*CardQuestionResponse._attrs))
        end
      end

      # ================ update ================
      def _update!(params)
        self.update!(params)
      end

      # ================ destroy ===============
      def _destroy(params)
      end
    end
  end
end
