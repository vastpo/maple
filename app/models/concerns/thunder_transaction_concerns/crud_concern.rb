module ThunderTransactionConcerns
  module CRUDConcern
    extend ActiveSupport::Concern

    included do
      # ================ index ================
      def self._index(params)
      end

      # ================ show ================
      def self._show(params)
      end

      # ================ create ================
      def self._create!(params)
        ActiveRecord::Base.transaction do
          _self = self.create!(params.slice(*ThunderTransaction._attrs))
          _self
        end
      end

      # ================ update ================
      def _update!(params)
      end

      # ================ destroy ===============
      def _destroy(params)
      end
    end
  end
end
