module CardConcerns
  module CRUDConcern
    extend ActiveSupport::Concern

    included do
      # ================ index ================
      def self._index(params)
        super(params)
      end

      # ================ show ================
      def self._show(params)
        self.find_by(external_id: params[:external_id])
      end

      # ================ create ================
      def self._create!(params)
        params = params.merge({
          external_id: Digest::SHA1.hexdigest(sprintf('%.22f', Time.now)),
        })

        if params[:post_external_ids].present?
          params[:posts] = Post.where(external_id: params[:post_external_ids])
        end

        params[:questions] = params[:questions].map do |question|
          CardQuestion.new(question)
        end

        ActiveRecord::Base.transaction do
          card = self.create!(params.slice(*Card._attrs))

          if params[:is_publish]
            card.publish!
          end

          card
        end
      end

      # ================ update ================
      def _update!(params)
        self.update!(params)

        if params[:post_external_ids].present?
          params[:post] = Post.where(external_id: params[:post_external_ids])
        end

      end

      # ================ destroy ===============
      def _destroy(params)
      end
    end
  end
end
