module CardConcerns
  module AASMConcern
    extend ActiveSupport::Concern

    included do
      include AASM

      def state
        self[:state]&.to_sym
      end

      aasm column: :state, enum: false do
        state :draft, initial: true
        state :reviewing, :published, :withheld, :rejected

        event :review do
          transitions from: Card::STATE_DRAFT, to: Card::STATE_REVIEWING
          transitions from: Card::STATE_PUBLISHED, to: Card::STATE_REVIEWING
        end

        event :publish do
          transitions from: Card::STATE_DRAFT, to: Card::STATE_PUBLISHED
          transitions from: Card::STATE_REVIEWING, to: Card::STATE_PUBLISHED
          transitions from: Card::STATE_REJECTED, to: Card::STATE_PUBLISHED
        end

        event :withhold do
          transitions from: Card::STATE_PUBLISHED, to: Card::STATE_WITHHELD
        end

        event :reject do
          transitions from: Card::STATE_REVIEWING, to: Card::STATE_REJECTED
        end
      end
    end
  end
end
