module ImageConcerns
  module CRUDConcern
    extend ActiveSupport::Concern

    included do
      # ================ index ================
      def self._index(params)
        super(params)
      end

      # ================ show ================
      def self._show(params)
        self.find_by(external_id: params[:external_id])
      end

      # ================ create ================
      def self._create!(params)
        params = params.merge({
          external_id: Digest::SHA1.hexdigest(sprintf('%.22f', Time.now)),
        })
        ActiveRecord::Base.transaction do
          _self = self.create!(params)			
          _self.update!(id: _self.acting_as.id)
          ActiveRecord::Base.connection.reset_pk_sequence!(self.table_name)
          return _self
        end
      end

      # ================ update ================
      def _update!(params)
      end

      # ================ destroy ===============
      def _destroy(params)
      end
    end
  end
end
