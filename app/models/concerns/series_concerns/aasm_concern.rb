module SeriesConcerns
  module AASMConcern
    extend ActiveSupport::Concern

    included do
      include AASM

      def state
        self[:state]&.to_sym
      end

      aasm column: :state, enum: false do
        state :draft, initial: true
        state :reviewing, :published, :withheld, :rejected

        event :review do
          transitions from: Series::STATE_DRAFT, to: Series::STATE_REVIEWING
          transitions from: Series::STATE_PUBLISHED, to: Series::STATE_REVIEWING
        end

        event :publish do
          transitions from: Series::STATE_DRAFT, to: Series::STATE_PUBLISHED
          transitions from: Series::STATE_REVIEWING, to: Series::STATE_PUBLISHED
          transitions from: Series::STATE_REJECTED, to: Series::STATE_PUBLISHED
        end
        event :withhold do
          transitions from: Series::STATE_PUBLISHED, to: Series::STATE_WITHHELD
        end

        event :reject do
          transitions from: Series::STATE_REVIEWING, to: Series::STATE_REJECTED
        end
      end
    end
  end
end
