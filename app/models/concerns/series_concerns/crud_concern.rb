module SeriesConcerns
  module CRUDConcern
    extend ActiveSupport::Concern

    included do
      # ================ index ================
      def self._index(params)
        super(params)
      end

      # ================ show ================
      def self._show(params)
        self.find_by(external_id: params[:external_id])
      end

      # ================ create ================
      def self._create!(params)
        params = params.merge({
          external_id: Digest::SHA1.hexdigest(sprintf('%.22f', Time.now)),
          is_ad_disabled: false,
          is_running_ad: false,
          base_url: ''
        })

        if params[:post_external_ids].present?
          params[:posts] = Post.where(external_id: params[:post_external_ids])
        end

        if params[:series_access].present?
          params[:series_access] = [ SeriesAccess.new(params[:series_access]) ]
        end

        ActiveRecord::Base.transaction do
          _self = self.create!(params.slice(*Series._attrs))
          _self.update!(hierarchy: [_self.id, _self.parent&.hierarchy].compact.flatten)
          _self
        end
      end

      # ================ update ================
      def _update!(params)
        if params[:post_external_ids].present?
          params[:posts] = Post.where(external_id: params[:post_external_ids])
        end

        ActiveRecord::Base.transaction do
          self.assign_attributes(params)
          parent_changed = self.parent_id_changed?
          self.save!
          if parent_changed
            self.update!(hierarchy: [self.id, self.parent&.hierarchy].compact.flatten)
            Series.where('id <> ?', self.id).where("hierarchy @> '?'::jsonb", self.id).order(id: :asc).each do |s|
              s.update!(hierarchy: [s.id, s.parent&.hierarchy].compact.flatten)
            end
          end
          return self.reload
        end
      end

      # ================ destroy ===============
      def _destroy(params)
      end
    end
  end
end
