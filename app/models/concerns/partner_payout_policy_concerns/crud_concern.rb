module PartnerPayoutPolicyConcerns
  module CRUDConcern
    extend ActiveSupport::Concern

    included do
      # ================ index ================
      def self._index(params)
        super(params)
      end

      # ================ show ================
      def self._show(params)
      end

      # ================ create ================
      def self._create!(params)
        if params[:is_active]
          other_policies = self.where(partner_id: params[:partner_id])
          other_policies.update_all(is_active: false)
        end
        self.create!(params)
      end

      # ================ update ================
      def _update!(params)
        if params[:is_active] == true
          other_policies = PartnerPayoutPolicy.where(partner_id: self.partner_id).where.not(id: self.id)
          other_policies.update_all(is_active: false)
        end
        self.update!(params)
      end

      # ================ destroy ===============
      def _destroy(params)
      end
    end
  end
end
