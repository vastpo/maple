module TagConcerns
  module CRUDConcern
    extend ActiveSupport::Concern

    included do
      # ================ index ================
      def self._index(params)
        super(params)
      end

      # ================ show ================
      def self._show(params)
        self.find_by(external_id: params[:external_id])
      end

      # ================ create ================
      def self._create!(params)
        params = params.merge({
          external_id: Digest::SHA1.hexdigest(sprintf('%.22f', Time.now)),
          base_url: ''
        })

        tag_params = [ :external_id, :title, :color_scheme, :base_url, :is_active ]

        ActiveRecord::Base.transaction do
          tag = self.create!(params.slice(*tag_params))
          tag
        end
      end

      # ================ update ================
      def _update!(params)
      end

      # ================ destroy ===============
      def _destroy(params)
      end
    end
  end
end
