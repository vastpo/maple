module UserConcerns
  module AASMConcern
    extend ActiveSupport::Concern

    included do
      include AASM

      def state
        self[:state]&.to_sym
      end

      aasm column: :state, enum: false do
        state :pending, initial: true
        state :active, :inactive, :suspended

        event :active do
          transitions from: Admin::STATE_PENDING, to: Admin::STATE_ACTIVE
          transitions from: Admin::STATE_INACTIVE, to: Admin::STATE_ACTIVE
          transitions from: Admin::STATE_SUSPENDED, to: Admin::STATE_ACTIVE
        end

        event :inactive do
          transitions from: Admin::STATE_ACTIVE, to: Admin::STATE_INACTIVE
          transitions from: Admin::STATE_SUSPENDED, to: Admin::STATE_INACTIVE
        end

        event :suspend do
          transitions from: Admin::STATE_ACTIVE, to: Admin::STATE_SUSPENDED
          transitions from: Admin::STATE_INACTIVE, to: Admin::STATE_SUSPENDED
        end
      end
    end
  end
end
