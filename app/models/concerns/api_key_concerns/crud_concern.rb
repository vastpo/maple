module APIKeyConcerns
  module CRUDConcern
    extend ActiveSupport::Concern

    included do
      # ================ index ================
      def self._index(params)
      end

      # ================ show ================
      def self._show(params)
      end

      # ================ create ================
      def self._create!(params)
        self.create!(
          owner: params[:owner],
          access_key: Digest::SHA256.hexdigest(JSON.generate({
            owner_type: params[:owner].class.name,
            owner_id: params[:owner].id,
            time: Time.now.to_f,
            key_type: :access_key
          })),
          secret_key: Digest::SHA256.hexdigest(JSON.generate({
            owner_type: params[:owner].class.name,
            owner_id: params[:owner].id,
            time: Time.now.to_f,
            key_type: :secret_key
          }))
        )
      end

      # ================ update ================
      def _update!(params)
      end

      # ================ destroy ===============
      def _destroy(params)
      end
    end
  end
end
