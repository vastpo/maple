module PartnerProfileConcerns
  module AASMConcern
    extend ActiveSupport::Concern

    included do
      include AASM

      def state
        self[:state]&.to_sym
      end

      aasm column: :state, enum: false do
        state :draft, initial: true
        state :reviewing, :published, :inactive, :rejected

        event :review do
          transitions from: PartnerProfile::STATE_DRAFT, to: PartnerProfile::STATE_REVIEWING
        end

        event :publish do
          transitions from: PartnerProfile::STATE_REVIEWING, to: PartnerProfile::STATE_PUBLISHED
          transitions from: PartnerProfile::STATE_INACTIVE, to: PartnerProfile::STATE_PUBLISHED
          transitions from: PartnerProfile::STATE_REJECTED, to: PartnerProfile::STATE_PUBLISHED
        end

        event :inactive do
          transitions from: PartnerProfile::STATE_PUBLISHED, to: PartnerProfile::STATE_INACTIVE
          transitions from: PartnerProfile::STATE_REJECTED, to: PartnerProfile::STATE_INACTIVE
        end

        event :reject do
          transitions from: PartnerProfile::STATE_REVIEWING, to: PartnerProfile::STATE_REJECTED
          transitions from: PartnerProfile::STATE_PUBLISHED, to: PartnerProfile::STATE_REJECTED
        end
      end
    end
  end
end
