module ThunderOfferConcerns
  module CRUDConcern
    extend ActiveSupport::Concern

    included do
      # ================ index ================
      def self._index(params)
        super(params)
      end

      # ================ show ================
      def self._show(params)
        self.find_by(external_id: params[:external_id])
      end

      # ================ create ================
      def self._create!(params)

        params = params.merge({
          external_id: Digest::SHA1.hexdigest(sprintf('%.22f', Time.now))
        })

        if params[:post_external_id].present?
          params[:post] = Post.find_by(external_id: params[:post_external_id])
        end

        ActiveRecord::Base.transaction do
          thunder_offer = self.create!(params.slice(*ThunderOffer._attrs))
          thunder_offer
        end
      end

      # ================ update ================
      def _update!(params)
        self.update!(params)
      end

      # ================ destroy ===============
      def _destroy(params)
      end
    end
  end
end
