module JSONConcern
  extend ActiveSupport::Concern

  included do

    self::JSON_ATTRS.each do |attr|
      locale_attrs = self::LOCALE_ATTRS
      empty_locale_object = self::EMPTY_LOCALE_OBJECT

      define_method attr do
        self[attr] = empty_locale_object if ((self[attr] || {}).empty? && attr.in?(locale_attrs))
        if self[attr].is_a?(Array) || self[attr].is_a?(Hash)
          self[attr].deep_symbolize_keys
        else
          self[attr]
        end
      end
    end

    self::LOCALE_ATTRS.each do |attr|
      empty_locale_object = self::EMPTY_LOCALE_OBJECT

      define_method "#{attr}=" do |value|
        value ||= empty_locale_object
        if value.is_a?(Hash) && !value.empty?
          I18n.available_locales.each do |locale|
            value[locale] ||= nil
          end
        else
          value = empty_locale_object
        end
        write_attribute(attr, value)
      end
    end

  end
end
