module AttachedMediaConcerns
  module CRUDConcern
    extend ActiveSupport::Concern

    included do
      # ================ index ================
      def self._index(params)
        super(params)
      end

      # ================ show ================
      def self._show(params)
      end

      # ================ create ================
      def self._create!(params)
        ActiveRecord::Base.transaction do
          attached_media = self.create!(params)
          attached_media
        end
      end

      # ================ update ================
      def _update!(params)
      end

      # ================ destroy ===============
      def _destroy(params)
      end
    end
  end
end
