module PostConcerns
  module AASMConcern
    extend ActiveSupport::Concern

    included do
      include AASM

      def state
        self[:state]&.to_sym
      end

      aasm column: :state, enum: false do
        state :draft, initial: true
        state :reviewing, :published, :withheld, :rejected

        event :review do
          transitions from: Post::STATE_DRAFT, to: Post::STATE_REVIEWING
          transitions from: Post::STATE_PUBLISHED, to: Post::STATE_REVIEWING
        end

        event :publish do
          transitions from: Post::STATE_DRAFT, to: Post::STATE_PUBLISHED
          transitions from: Post::STATE_REVIEWING, to: Post::STATE_PUBLISHED
          transitions from: Post::STATE_REJECTED, to: Post::STATE_PUBLISHED
        end

        event :withhold do
          transitions from: Post::STATE_PUBLISHED, to: Post::STATE_WITHHELD
        end

        event :reject do
          transitions from: Post::STATE_REVIEWING, to: Post::STATE_REJECTED
        end
      end
    end
  end
end
