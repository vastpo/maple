module PostConcerns
  module CRUDConcern
    extend ActiveSupport::Concern

    included do
      # ================ index ================
      def self._index(params)
        super(params)
      end

      # ================ show ================
      def self._show(params)
        self.find_by(external_id: params[:external_id])
      end

      # ================ create ================
      def self._create!(params)
        params = params.merge({
          external_id: Digest::SHA1.hexdigest(sprintf('%.22f', Time.now)),
          is_ad_disabled: false,
          is_running_ad: false,
          is_counting_rpe: false,
          base_url: ''
        })

        if params[:permitted_emojis].any?{ |e| !e.emoji? }
          raise 'permited_emojis invalid'
        end

        if params[:series_external_ids].present?
          params[:series] = Series.where(external_id: params[:series_external_ids])
        end

        if params[:column_external_ids].present?
          params[:columns] = Column.where(external_id: params[:column_external_ids])
        end

        if params[:tag_external_ids].present?
          params[:tags] = Tag.where(external_id: params[:tag_external_ids])
        end

        if params[:card_external_ids].present?
          params[:cards] = Card.where(external_id: params[:card_external_ids])
        end

        ActiveRecord::Base.transaction do
          post = self.create!(params.slice(*Post._attrs))
          (params[:attached_media_details] || []).each do |am|
            media = Media.find_by(external_id: am[:external_id])
            if media.present?
              AttachedMedia.create!(
                media: Media.find_by(external_id: am[:external_id]),
                attachable: post,
                attached_for: am[:attached_for]
              )
            end
          end

          post.sync_media_details

          if params[:is_publish]
            post.publish
          end

          post.save!
          return post
        end
      end

      # ================ update ================
      def _update!(params)
        if params[:series_external_ids].present?
          params[:series] = Series.where(external_id: params[:series_external_ids])
        end

        if params[:column_external_ids].present?
          params[:columns] = Column.where(external_id: params[:column_external_ids])
        end

        if params[:tag_external_ids].present?
          params[:tags] = Tag.where(external_id: params[:tag_external_ids])
        end

        if params[:card_external_ids].present?
          params[:cards] = Card.where(external_id: params[:card_external_ids])
        end

        ActiveRecord::Base.transaction do
          params[:attached_media] = params[:attached_media_details].map do |am|
            media = Media.find_by(external_id: am[:external_id])
            if media.present?
              AttachedMedia.new(
                media: Media.find_by(external_id: am[:external_id]),
                attachable: self,
                attached_for: am[:attached_for]
              )
            end
          end.compact

          self.assign_attributes(params.slice(*Post._attrs))
          self.sync_media_details

          self.save!
          return self
        end
      end

      # ================ destroy ===============
      def _destroy(params)
      end
    end
  end
end
