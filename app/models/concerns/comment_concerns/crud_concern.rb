module CommentConcerns
  module CRUDConcern
    extend ActiveSupport::Concern

    included do
      # ================ index ================
      def self._index(params)
        super(params)
      end

      # ================ show ================
      def self._show(params)
        self.find_by(external_id: params[:external_id])
      end

      # ================ create ================
      def self._create!(params)
        params = params.merge({
          external_id: Digest::SHA1.hexdigest(sprintf('%.22f', Time.now)),
          is_masked: false,
          is_hidden: false,
        })

        comment_params = [ :created_by, :external_id, :post_id, :parent_id, :content, :is_masked, :is_hidden ] 

        ActiveRecord::Base.transaction do
          comment = self.create!(params.slice(*comment_params))
          comment
        end

      end

      # ================ update ================
      def _update!(params)
        self.update!(params)
      end

      # ================ destroy ===============
      def _destroy(params)
      end
    end
  end
end
