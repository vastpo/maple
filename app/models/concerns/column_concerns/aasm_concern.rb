module ColumnConcerns
  module AASMConcern
    extend ActiveSupport::Concern

    included do
      include AASM

      def state
        self[:state]&.to_sym
      end

      aasm column: :state, enum: false do
        state :draft, initial: true
        state :reviewing, :published, :withheld, :rejected

        event :review do
          transitions from: Column::STATE_DRAFT, to: Column::STATE_REVIEWING
          transitions from: Column::STATE_PUBLISHED, to: Column::STATE_REVIEWING
        end

        event :publish do
          transitions from: Column::STATE_DRAFT, to: Column::STATE_PUBLISHED
          transitions from: Column::STATE_REVIEWING, to: Column::STATE_PUBLISHED
          transitions from: Column::STATE_REJECTED, to: Column::STATE_PUBLISHED
        end

        event :withhold do
          transitions from: Column::STATE_PUBLISHED, to: Column::STATE_WITHHELD
        end

        event :reject do
          transitions from: Column::STATE_REVIEWING, to: Column::STATE_REJECTED
        end
      end
    end
  end
end
