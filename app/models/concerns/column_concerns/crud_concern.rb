module ColumnConcerns
  module CRUDConcern
    extend ActiveSupport::Concern

    included do
      # ================ index ================
      def self._index(params)
        super(params)
      end

      # ================ show ================
      def self._show(params)
        self.find_by(external_id: params[:external_id])
      end

      # ================ create ================
      def self._create!(params) 
        params = params.merge({
          external_id: Digest::SHA1.hexdigest(sprintf('%.22f', Time.now)),
          base_url: ''
        })

        column_params = [ :created_by, :external_id, :title, :short_note, :long_note, :color_scheme, :base_url, :posts ]

        if params[:post_external_ids].present?
          params[:posts] = Post.where(external_id: params[:post_external_ids])
        end

        ActiveRecord::Base.transaction do
          column = self.create!(params.slice(*column_params))

          if params[:is_publish]
            column.publish!
          end

          column
        end
      end
    end

    # ================ update ================
    def _update!(params)
      if params[:post_external_ids].present?
        params[:posts] = Post.where(external_id: params[:post_external_ids])
      end

      self.update!(params)
    end

    # ================ destroy ===============
    def _destroy(params)
    end
  end
end
