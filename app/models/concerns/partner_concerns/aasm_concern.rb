module PartnerConcerns
  module AASMConcern
    extend ActiveSupport::Concern

    included do
      include AASM

      def state
        self[:state]&.to_sym
      end

      aasm column: :state, enum: false do
        state :pending, initial: true
        state :active, :inactive, :suspended, :rejected

        event :active do
          transitions from: Partner::STATE_PENDING, to: Partner::STATE_ACTIVE
          transitions from: Partner::STATE_INACTIVE, to: Partner::STATE_ACTIVE
          transitions from: Partner::STATE_SUSPENDED, to: Partner::STATE_ACTIVE
          transitions from: Partner::STATE_REJECTED, to: Partner::STATE_ACTIVE
        end

        event :inactive do
          transitions from: Partner::STATE_ACTIVE, to: Partner::STATE_INACTIVE
          transitions from: Partner::STATE_SUSPENDED, to: Partner::STATE_INACTIVE
        end

        event :suspend do
          transitions from: Partner::STATE_ACTIVE, to: Partner::STATE_SUSPENDED
          transitions from: Partner::STATE_INACTIVE, to: Partner::STATE_SUSPENDED
        end

        event :reject do
          transitions from: Partner::STATE_PENDING, to: Partner::STATE_REJECTED
        end
      end
    end
  end
end
