module PartnerConcerns
  module CRUDConcern
    extend ActiveSupport::Concern

    included do
      # ================ index ================
      def self._index(params)
        super(params)
      end

      # ================ show ================
      def self._show(params)
        self.find(params[:id])
      end

      # ================ create ================
      def self._create!(params)
        partner_params = params.slice(:login, :password, :display_name, :preferred_language, :preferred_currency, :stripe_account_id, :state)
        profile_params = params.slice(:short_note, :long_note, :links, :is_badged, :media_details, :interaction_details, :score_details, :is_ad_disabled, :state)
        payout_policy_params = params[:payout_policy].slice(:fee_rate, :fee_fix, :rpe, :is_active, :remarks)
        content_policy_params = params[:content_policy]

        partner_params.merge!({
          external_id: Digest::SHA1.hexdigest(sprintf('%.22f', Time.now))
        })

        profile_params.merge!({
          external_id: Digest::SHA1.hexdigest(sprintf('%.22f', Time.now)),
          display_name: partner_params[:display_name],
          is_running_ad: false,
          base_url: 'http://google.com.hk'
        })

        payout_policy_params.merge!({
          currency: partner_params[:preferred_currency],
        })

        ActiveRecord::Base.transaction do
          _self = self.create!(partner_params)
          profile = PartnerProfile._create!({ partner: _self }.merge(profile_params))
          PartnerPayoutPolicy._create!({ partner: _self }.merge(payout_policy_params))
          PartnerContentPolicy._create!({ partner: _self }.merge(content_policy_params))
          APIKey._create!(owner: _self)
          profile.review! if (params[:is_review_profile])
          _self
        end
      end

      # ================ update ================
      def _update!(params)
        partner_params = params.slice(:login, :password, :external_id, :display_name, :preferred_language, :preferred_currency, :stripe_account_id)
        profile_params = params.slice(:short_note, :long_note, :links, :is_badged, :media_details, :is_ad_disabled)

        profile_params.merge!({ display_name: partner_params[:display_name] })

        ActiveRecord::Base.transaction do
          update!(partner_params)
          profile.update!(profile_params) if profile.present?
          self
        end
      end

      # ================ destroy ===============
      def _destroy(params)
      end
    end
  end
end
