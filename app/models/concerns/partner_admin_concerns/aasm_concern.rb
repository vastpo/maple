module PartnerAdminConcerns
  module AASMConcern
    extend ActiveSupport::Concern

    included do
      include AASM

      def state
        self[:state]&.to_sym
      end

      aasm column: :state, enum: false do
        state :active, initial: true
        state :inactive, :suspended

        event :active do
          transitions from: PartnerAdmin::STATE_INACTIVE, to: PartnerAdmin::STATE_ACTIVE
          transitions from: PartnerAdmin::STATE_SUSPENDED, to: PartnerAdmin::STATE_ACTIVE
        end

        event :inactive do
          transitions from: PartnerAdmin::STATE_ACTIVE, to: PartnerAdmin::STATE_INACTIVE
          transitions from: PartnerAdmin::STATE_SUSPENDED, to: PartnerAdmin::STATE_INACTIVE
        end

        event :suspend do
          transitions from: PartnerAdmin::STATE_ACTIVE, to: PartnerAdmin::STATE_SUSPENDED
          transitions from: PartnerAdmin::STATE_INACTIVE, to: PartnerAdmin::STATE_SUSPENDED
        end
      end
    end
  end
end
