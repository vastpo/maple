module AdminConcerns
  module CRUDConcern
    extend ActiveSupport::Concern

    included do
      # ================ index ================
      def self._index(params)
        super(params)
      end

      # ================ show ================
      def self._show(params)
        self.find(params[:id])
      end

      # ================ create ================
      def self._create!(params)
        ActiveRecord::Base.transaction do
          _self = self.create!(params.slice(*Admin._attrs))
          AdminProfile._create!({ admin: _self }.merge(params.slice(*AdminProfile._attrs)))
          APIKey._create!(owner: _self)
          _self
        end
      end

      # ================ update ================
      def _update!(params)
        admin_params = params.slice(:login, :password, :display_name, :roles)
        profile_params = params.slice(:first_name, :last_name, :nickname, :gender, :phones, :emails, :role_details)
        ActiveRecord::Base.transaction do
          update!(admin_params)
          profile.update!(profile_params) if profile.present?
          self
        end
      end

      # ================ destroy ===============
      def _destroy(params)
      end
    end
  end
end
