module WidgetConcerns
  module CRUDConcern
    extend ActiveSupport::Concern

    included do
      # ================ index ================
      def self._index(params)
        super(params)
      end

      # ================ show ================
      def self._show(params)
        self.find_by(external_id: params[:external_id])
      end

      # ================ create ================
      def self._create!(params)
        params = params.merge({
          external_id: Digest::SHA1.hexdigest(sprintf('%.22f', Time.now)),
          is_ad_disabled: false,
          is_running_ad: false,
        })
        ActiveRecord::Base.transaction do
          widget = self.create!(params.except(:is_publish))

          if params[:is_publish]
            widget.publish!
          end

          widget
        end
      end

      # ================ update ================
      def _update!(params)
        self.update!(params)
      end

      # ================ destroy ===============
      def _destroy(params)
      end
    end
  end
end
