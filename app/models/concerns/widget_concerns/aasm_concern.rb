module WidgetConcerns
  module AASMConcern
    extend ActiveSupport::Concern

    included do
      include AASM

      def state
        self[:state]&.to_sym
      end

      aasm column: :state, enum: false do
        state :draft, initial: true
        state :reviewing, :published, :withheld, :rejected

        event :review do
          transitions from: Widget::STATE_DRAFT, to: Widget::STATE_REVIEWING
          transitions from: Widget::STATE_PUBLISHED, to: Widget::STATE_REVIEWING
        end

        event :publish do
          transitions from: Widget::STATE_DRAFT, to: Widget::STATE_PUBLISHED
          transitions from: Widget::STATE_REVIEWING, to: Widget::STATE_PUBLISHED
          transitions from: Widget::STATE_REJECTED, to: Widget::STATE_PUBLISHED
        end

        event :withhold do
          transitions from: Widget::STATE_PUBLISHED, to: Widget::STATE_WITHHELD
        end

        event :reject do
          transitions from: Widget::STATE_REVIEWING, to: Widget::STATE_REJECTED
        end
      end
    end
  end
end
