class Message < ApplicationRecord
  belongs_to :sent_by, class_name: 'User'

  has_one :capture, dependent: :destroy

  has_many :deliveries, class_name: 'MessageDelivery', dependent: :destroy


  # CRUD manipulation
  include MessageConcerns::CRUDConcern

end
