class Media < ApplicationRecord
  actable as: :media, inverse_of: :media

  belongs_to :managed_by, class_name: 'Partner', optional: true

  has_many :attached_media, dependent: :destroy
  has_many :engagements, dependent: :destroy

  before_destroy do
    puts "Fuck"
  end

  LEGAL_ATTRS = [:exclusivity, :transferability, :sub_licensibility, :royalty_terms, :territorialities]

  JSON_ATTRS = [:urls] + LEGAL_ATTRS
  LOCALE_ATTRS = []
  include JSONConcern

  # CRUD manipulation
  include MediaConcerns::CRUDConcern

  def before_media_destroy
    binding.pry
  end

end
