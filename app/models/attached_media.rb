class AttachedMedia < ApplicationRecord
  self.primary_keys = :media_id, :attachable_type, :attachable_id
  belongs_to :attachable, polymorphic: true
  belongs_to :media

  # CRUD manipulation
  include AttachedMediaConcerns::CRUDConcern

end
