class PartnerContentPolicy < ApplicationRecord
  belongs_to :partner


  JSON_ATTRS = [:default_delivery_locations]
  LOCALE_ATTRS = []
  include JSONConcern


  # CRUD manipulation
  include PartnerContentPolicyConcerns::CRUDConcern

end
