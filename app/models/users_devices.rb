class UsersDevices < ApplicationRecord
  self.primary_keys = :user_id, :device_id
  belongs_to :user, optional: true
  belongs_to :device

  has_many :push_notifications, class_name: 'UserDevicePushNotification', foreign_key: self.primary_keys, dependent: :destroy
  has_many :sessions, class_name: 'UserDeviceSession', foreign_key: self.primary_keys, dependent: :destroy
end
