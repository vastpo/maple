class PartnerAccessInvitation < ApplicationRecord
  belongs_to :invited_by, polymorphic: true
  belongs_to :partner

  has_one :partner_access, foreign_key: :invitation_id, dependent: :nullify

  JSON_ATTRS = [:roles]
  LOCALE_ATTRS = []
  include JSONConcern

  # states manipulation
  include PartnerAccessInvitationConcerns::AASMConcern

  # CRUD manipulation
  include PartnerAccessInvitationConcerns::CRUDConcern

end
