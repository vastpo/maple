class CardQuestion < ApplicationRecord

  belongs_to :card

  has_many :responses, class_name: 'CardQuestionResponse'

  RESPONSE_TYPE = [
    :boolean,
    :checkbox,
    :date,
    :email,
    :number,
    :phone,
    :radio,
    :text,
  ]

end
