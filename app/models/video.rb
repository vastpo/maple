class Video < ApplicationRecord
  acts_as :media, as: :media

  has_many :captures, dependent: :destroy

  # before_destroy do
  #   before_media_destroy
  # end

  # CRUD manipulation
  include VideoConcerns::CRUDConcern

end
