class CardsPosts < ApplicationRecord
  self.primary_keys = :card_id, :post_id
  belongs_to :card
  belongs_to :post
end
