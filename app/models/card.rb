class Card < ApplicationRecord
  belongs_to :created_by, polymorphic: true
  belongs_to :managed_by, class_name: 'Partner'

  has_many :active_cards_posts, ->{ where(is_active: true) }, class_name: 'CardsPosts', dependent: :destroy
  has_many :active_posts, ->{ where(state: Post::STATE_PUBLISHED) }, class_name: 'Post', through: :active_cards_posts, source: :posts
  has_many :questions, class_name: 'CardQuestion', dependent: :destroy

  has_and_belongs_to_many :posts, dependent: :destroy

  JSON_ATTRS = [:accessible_locations, :tag_text, :conversion_text, :start_note, :end_note, :color_scheme, :interaction_details]
  LOCALE_ATTRS = [:tag_text, :conversion_text, :start_note, :end_note]
  include JSONConcern

  # states manipulation
  include CardConcerns::AASMConcern

  # CRUD manipulation
  include CardConcerns::CRUDConcern

end
