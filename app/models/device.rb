class Device < ApplicationRecord

  SUPPORTED_SERVICES = [ 'APN', 'FCM' ]

  has_many :delivery_inventories, dependent: :destroy
  has_many :engagements, dependent: :destroy
  has_many :page_sessions, dependent: :destroy

  has_and_belongs_to_many :users, join_table: :users_devices, dependent: :destroy, optional: true

  # CRUD manipulation
  include DeviceConcerns::CRUDConcern

end
