class AttachedEmoji < ApplicationRecord
  belongs_to :post
  belongs_to :user

  # CRUD manipulation
  include AttachedEmojiConcerns::CRUDConcern

end
