class PartnerAccess < ApplicationRecord
  belongs_to :invitation, class_name: 'PartnerAccessInvitation', optional: true
  belongs_to :partner_admin
  belongs_to :partner

  JSON_ATTRS = [:roles, :location_accesses, :phones, :emails]
  LOCALE_ATTRS = []
  include JSONConcern

  # CRUD manipulation
  include PartnerAccessConcerns::CRUDConcern

end
