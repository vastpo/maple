class PartnerAccessInvitationPolicy < ApplicationPolicy
  def create?
    is_authorized_admin = record.invited_by.managing_partners.pluck(:id).include?(record.partner_id)
    return (
      (member.is_a?(Admin) && member.is_account_executive? && is_authorized_admin) ||
      super
    )
  end
end
