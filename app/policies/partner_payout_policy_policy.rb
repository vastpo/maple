class PartnerPayoutPolicyPolicy < ApplicationPolicy
  def show?
    return (
      super
    )
  end

  def create?
    return (
      super
    )
  end

  def update?
    return (
      super
    )
  end
end
