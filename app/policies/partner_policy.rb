class PartnerPolicy < ApplicationPolicy
  def create?
    return (
      (member.is_a?(Admin) && member.is_account_manager?) ||
      super
    )
  end

  def show?
    return (
      (member.is_a?(Partner) && (member.id == record.id)) ||
      member.is_a?(Admin) ||
      super
    )
  end

  def update?
    return (
      (member.is_a?(Partner) && member.id == record.id && !record.login_changed? && !record.external_id_changed?) ||
      (member.is_a?(Admin) && member.is_account_executive?) ||
      super
    )
  end
end
