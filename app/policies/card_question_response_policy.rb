class CardQuestionResponsePolicy < ApplicationPolicy
  def index?
    return (
      (member.is_a?(PartnerAdmin)) || super
    )
  end

  def show?
    return (
      (member.is_a?(PartnerAdmin)) || super
    )
  end

  def create?
    return (
      member.is_a?(User)
    )
  end
end
