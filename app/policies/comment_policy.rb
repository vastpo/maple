class CommentPolicy < ApplicationPolicy
  def index?
    return (
      (member.is_a?(PartnerAdmin)) || (member.is_a?(User)) || (member.is_a?(Admin)) ||
      super
    )
  end

  def show?
    return (
      (member.is_a?(PartnerAdmin)) || (member.is_a?(User)) || (member.is_a?(Admin)) ||
      super
    )
  end

  def create?
    return (
      (member.is_a?(PartnerAdmin)) || (member.is_a?(User)) || (member.is_a?(Admin)) ||
      super
    )
  end

  def update?
    return (
      (member.is_a?(PartnerAdmin) && member == record.created_by) || (member.is_a?(User) && member == record.created_by) ||
      super
    )
  end
end
