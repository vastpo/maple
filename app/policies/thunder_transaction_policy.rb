class ThunderTransactionPolicy < ApplicationPolicy
  def show?
    return (
      (member.is_a?(PartnerAdmin) && member.partner_access.pluck(:partner_id).include?(record.managed_by_id)) ||
      super
    )
  end

  def create?
    return (
      (member.is_a?(PartnerAdmin) && member.partner_access.pluck(:partner_id).include?(record.managed_by_id)) ||
      super
    )
  end
end

