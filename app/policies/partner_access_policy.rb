class PartnerAccessPolicy < ApplicationPolicy
  def update?
    return (
      (member.is_a?(PartnerAdmin) && member.id == record.partner_admin_id) ||
      super
    )
  end
end
