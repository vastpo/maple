class ApplicationPolicy
  attr_reader :member, :record

  def initialize(member, record)
    @member = member
    @record = record
  end

  def index?
    is_super_admin?
  end

  def show?
    scope.where(:id => record.id).exists? && is_super_admin?
  end

  def create?
    is_super_admin?
  end

  def new?
    is_super_admin?
  end

  def update?
    is_super_admin?
  end

  def edit?
    is_super_admin?
  end

  def destroy?
    is_super_admin?
  end

  private def is_super_admin?
    member.is_a?(Admin) && member.is_super_admin?
  end

  def scope
    Pundit.policy_scope!(member, record.class)
  end

  class Scope
    attr_reader :member, :scope

    def initialize(member, scope)
      @member = member
      @scope = scope
    end

    def resolve
      scope
    end
  end
end
