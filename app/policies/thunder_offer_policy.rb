class ThunderOfferPolicy < ApplicationPolicy
  def index?
    return (
      (member.is_a?(PartnerAdmin) && member.partner_access.pluck(:partner_id).include?(record.partner_id)) || (member.is_a?(Admin)) || (member.is_a?(User)) ||
      super
    )
  end

  def show?
    return (
      (member.is_a?(PartnerAdmin) && member.partner_access.pluck(:partner_id).include?(record.partner_id)) || (member.is_a?(Admin)) || (member.is_a?(User)) ||
      super
    )
  end

  def create?
    return (
      (member.is_a?(PartnerAdmin) && member.partner_access.pluck(:partner_id).include?(record.partner_id)) || (member.is_a?(Admin)) ||
      super
    )
  end

  def update?
    return (
      (member.is_a?(PartnerAdmin) && member.partner_access.pluck(:partner_id).include?(record.partner_id)) || (member.is_a?(Admin)) ||
      super
    )
  end
end
