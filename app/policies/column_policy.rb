class ColumnPolicy < ApplicationPolicy
  def index?
    return (
      (member.is_a?(PartnerAdmin)) || (member.is_a?(Admin)) || (member.is_a?(User)) || (member.is_a?(General)) ||
      super
    )
  end

  def show?
    return (
      (member.is_a?(PartnerAdmin)) || (member.is_a?(Admin)) || (member.is_a?(User)) || (member.is_a?(General)) ||
      super
    )
  end

  def create?
    return (
      (member.is_a?(Admin)) || (member.is_a?(User)) ||
      super
    )
  end

  def update?
    return (
      (member.is_a?(Admin)) ||
      super
    )
  end

end
