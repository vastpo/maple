class PartnerProfilePolicy < ApplicationPolicy
  def index?
    return (
      (member.is_a?(Admin)) || (member.is_a?(User)) ||
      super
    )
  end

  def show?
    return (
      (member.is_a?(PartnerAdmin) && member.partner_access.pluck(:partner_id).include?(record.partner_id)) || (member.is_a?(Admin)) || (member.is_a?(User)) ||
      super
    )
  end
end
