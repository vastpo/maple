class AdminPolicy < ApplicationPolicy
  def show?
    return (
      (member.is_a?(Admin) && member.id == record.id) ||
      super
    )
  end

  def create?
    member.is_a?(Admin) && member.is_super_admin?
  end

  def update?
    return (
      (
        member.is_a?(Admin) && member.id == record.id &&
        !record.login_changed? && !record.roles_changed?
      ) ||
      super
    )
  end
end
