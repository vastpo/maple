class SeriesPolicy < ApplicationPolicy
  def index?
    return (
      (member.is_a?(PartnerAdmin) && member.partner_access.pluck(:partner_id).include?(record.partner_id)) ||
      (member.is_a?(Admin)) ||
      (member.is_a?(User)) ||
      super
    )
  end

  def show?
    return (
      (member.is_a?(PartnerAdmin) && (member.partner_access.pluck(:partner_id) & record.series_access.pluck(:partner_id)).present?) ||
      (member.is_a?(Admin)) ||
      (member.is_a?(User)) ||
      super
    )
  end

  def create?
    return (
      super
    )
  end

  def update?
    return (
      (member.is_a?(PartnerAdmin) && member.managing_series.pluck(:external_id).include?(record.external_id)) ||
      super
    )
  end
end
