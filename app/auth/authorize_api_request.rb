class AuthorizeAPIRequest
  prepend SimpleCommand

  def initialize(_classes, path, headers = {}, params = {})
    @_classes = _classes
    @path = path
    @headers = headers
    @params = params
    @member = nil
    # @redis_signature = Nest.new("Signature")
  end

  def call
    check_member_exist!
    @payload = decoded_auth_token(secret_key: @member.api_key.secret_key)
    check_time_frame!
    check_path!
    # check_params!
    member
  end

  private

  attr_reader :headers

  def member
    @member if @payload
  rescue Exception
    raise
  end

  def check_member_exist!
    raise ExceptionHandler::DecodeError, 'Missing Access-Key' if !headers['Access-Key'].present?
    @member ||= APIKey.find_by(owner_type: @_classes.map(&:to_s).map(&:classify), access_key: headers['Access-Key'])&.owner
    raise Exception.new "Invalid Access-Key" if @member.nil?
  end

  def check_time_frame!
    if Time.current.to_i < @payload[:time]
      raise Exception.new "Invalid Signature" if Rails.env.production?
      raise Exception.new "Invalid Signature for Time Frame"
    end
  end

  def check_path!
    @payload[:path] = "/#{@payload[:path]}" if @payload[:path].first != '/'
    if @path != @payload[:path]
      raise Exception.new "Invalid Signature" if Rails.env.production?
      raise Exception.new "Invalid Signature for Request Path"
    end
  end

  def check_params!
    if @params != @payload[:params]
      raise Exception.new "Invalid Signature" if Rails.env.production?
      raise Exception.new "Invalid Signature for Request Params"
    end
  end

  def decoded_auth_token(secret_key:)
    @decoded_auth_token ||= JSONWebToken.decode(http_auth_header, secret_key)
  rescue ExceptionHandler::ExpiredSignature => e
    raise Exception.new e.message
  rescue ExceptionHandler::DecodeError => e
    raise Exception.new e.message
  end

  def http_auth_header
    raise ExceptionHandler::DecodeError, 'Missing Signature' if !headers['Signature'].present?
    # raise ExceptionHandler::DecodeError, "Signature has expired" if $redis.get(@redis_signature[headers['Signature']]).present?
    # $redis.set(@redis_signature[headers['Signature']], true, ex: ENV['JWT_EXPIRE_AFTER'].to_i)
    return headers['Signature'].split(' ').last
  end
end
