class Authenticator
  prepend SimpleCommand
  attr_accessor :login, :password

  #this is where parameters are taken when the command is called
  def initialize(_class, login, password)
    @class = _class
    @login = login
    @password = password
  end
  
  #this is where the result gets returned
  def call
    _member = member
    if _member
      payload = {
        id: _member.id,
        type: @class.name,
        time: Time.current.to_i
      }
      return { status: :ok, type: @class.name, }.merge(_member.as_json({
        except: [:login, :password, :password_digest, :created_at, :updated_at],
        include: {
          api_key: {
            only: [:access_key, :rsa_private_key]
          }
        }
      }))
    end
  end

  private

  def member
    member = @class.find_by_login(login)
    return member if member && member.authenticate(password)

    errors.add :authentication, 'invalid_credentials'
    nil
  end
end
