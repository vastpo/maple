module API
  class PunditExceptionHandler
    def initialize(member, exception)
      @member = member
      @exception = exception
    end

    def perform!
      handled_result = handle!
      if @exception.is_a?(Pundit::NotAuthorizedError)
        return { status: 'access_denied' } if (Rails.env.production? || Rails.env.staging?)
        { status: 'access_denied', message: handled_result }
      else
        handled_result
      end
    end

    private def handle!
      if (Rails.env.production? || Rails.env.staging?)
        Bugsnag.notify(@exception) do |report|
          report.add_tab(:error_details, {
            member_type: @member.class.name,
            member_object: @member.as_json,
            request_headers: RequestStore.store[:request].headers.as_json,
            request_params: RequestStore.store[:request].params.as_json
          })
        end
      end
      @exception.message
    end
  end
end
