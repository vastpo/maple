module API::V2::General
  class Base < Grape::API
    namespace '/general' do
      get '/home' do
        { home: :OK, version: :v2 }
      end
    end
  end
end
