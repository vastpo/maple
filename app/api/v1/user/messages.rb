module API::V1::User
  class Messages < Grape::API
    namespace '/messages' do

      params do
        requires :counterpart_id, type: Integer
        requires :page, type: Integer
      end
      get '/personal_history' do
        authenticate! :user

        MessageEntry
          .and(
            recipient_type: 'User',
            '$or' => [
              { sender_id: 1, recipient_id: params[:counterpart_id] },
              { sender_id: params[:counterpart_id], recipient_id: 1 }
            ]
          )
          .order(_id: :desc)
          .offset(Kaminari.config.default_per_page * (params[:page].to_i - 1))
          .limit(Kaminari.config.default_per_page)
          .as_json(include: :quote_entry)
      end

      params do
        requires :sender_id, type: Integer
      end
      put '/personal_read' do
        authenticate! :user

        msgs = MessageEntry.where(sender_id: params[:sender_id], recipient_id: @current_member.id, read_at: nil)
        msg_count = msgs.count
        msgs.update_all(read_at: Time.current)

        return {
          status: :ok,
          affected: msg_count
        }
      end

      params do
        requires :recipient_id, type: Integer
        optional :quote_entry_id, type: String
        requires :body, type: Hash do
          optional :audio, type: File
          optional :capture_id, type: Integer
          optional :image, type: File
          optional :text, type: String
          optional :url, type: String
          optional :video, type: String
        end
        requires :sent_at, type: DateTime
      end
      post '/personal_send' do
        authenticate! :user

        recipient = ::User.find(params[:recipient_id])
        devices = Device::SUPPORTED_SERVICES.inject({}) { |h,k| h[k] = recipient.active_devices.where(service: k); h }

        begin
          quote_entry = (params[:quote_entry_id].present? ? MessageEntry.find(params[:quote_entry_id]) : nil)
          t = Thread.new do
            m = MessageEntry.new(
              sender_id: 1,
              recipient_type: 'User',
              recipient_id: params[:recipient_id],
              quote_entry: quote_entry,
              group_id: nil,
              metadata: {},
              body: params[:body],
              sent_at: params[:sent_at],
              read_at: nil,
              deleted_at: nil
            )
            m.save!
            devices['APN'].each do |device|
              n = Rpush::Apns::Notification.new
              n.app = Rpush::Apnsp8::App.find_by_name(ENV['APP_NAME'])
              n.device_token = device.token
              n.alert = { body: params[:body][:text], title: 'Stephy Cat' }
              n.data = { foo: :bar }
              n.save!
            end
            devices['FCM'].each do |device|
              n = Rpush::Gcm::Notification.new
              n.app = Rpush::Gcm::App.find_by_name(ENV['APP_NAME'])
              n.registration_ids = [ device.token ]
              n.data = { message: "hi mom!" }
              n.priority = 'high'
              n.content_available = true
              n.notification = { body: params[:body][:text], title: 'Stephy Cat', icon: 'myicon' }
              n.save!
            end
          end
        rescue Exception => e
          return {
            status: :error,
            message: e.message
          }
        end
        t.abort_on_exception = false

        return {
          status: :ok
        }
      end

    end
  end
end
