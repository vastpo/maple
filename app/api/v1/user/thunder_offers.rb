module API::V1::User
  class ThunderOffers < Grape::API
    namespace '/thunder_offers' do
     
      params do
        requires :partner_id, type: Integer
        optional :post_id, type: Integer
        optional :order, type: String, desc: 'Ordering, format: [key1:asc|desc[;key2:asc|desc[;...]]]'
        optional :opt, type: String, desc: 'Options, format: [key1:value1[;key2:value2[;...]]]'
        optional :pagination, type: String, desc: 'Pagination, format: [page:Integer[;per:Integer]]'
      end
      get '/index' do
        authenticate! :user

        # authorize ::ThunderOffer.new(partner_id: params[:partner_id]), :index?
      
        parsed_params = parse_qs(params)
        parsed_params[:where] = { partner_id: params[:partner_id] }
        if params[:post_id]
          parsed_params[:where].merge!(post_id: params[:post_id].presence)
        end

        thunder_offers = ::ThunderOffer._index(parsed_params)
        return thunder_offers
      end

      # params do
      #   optional :where, type: Hash
      #   optional :as_json, type: Hash
      # end
      # post '/index' do
      #   status 200
      #   thunder_offers = ::ThunderOffer._index(params || {})
      #   thunder_offers.as_json(params[:as_json])
      # end

      params do
        requires :external_id, type: String
        optional :opt, type: String, desc: 'Options, format: [key1:value1[;key2:value2[;...]]]'
      end
      get '/show/:external_id' do
        authenticate! :user
        thunder_offer = ::ThunderOffer._show(params.slice(:external_id))

        raise ActiveRecord::RecordNotFound if !thunder_offer

        authorize thunder_offer, :show?
        status 200
        return thunder_offer
      end

      # params do
      #   requires :external_id, type: String
      #   optional :as_json, type: Hash
      # end
      # post '/show' do
      #   authenticate! :user
      #   thunder_offer = ::ThunderOffer._show(params.slice(:external_id))
      #
      #   raise ActiveRecord::RecordNotFound if !thunder_offer
      #
      #   authorize thunder_offer, :show?
      #   status 200
      #   thunder_offer.as_json(params[:as_json])
      # end
    
    end
  end
end
