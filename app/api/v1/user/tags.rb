module API::V1::User
  class Tags < Grape::API
    namespace '/tags' do
 
      params do
        optional :order, type: String, desc: 'Ordering, format: [key1:asc|desc[;key2:asc|desc[;...]]]'
        optional :opt, type: String, desc: 'Options, format: [key1:value1[;key2:value2[;...]]]'
        optional :pagination, type: String, desc: 'Pagination, format: [page:Integer[;per:Integer]]'
      end
      get '/index' do
        authenticate! :user

        # authorize ::Tag.new(partner_id: params[:partner_id]), :index?

        parsed_params = parse_qs(params)

        tags = ::Tag._index(parsed_params)
        return tags
      end

     # params do
     #    optional :where, type: Hash
     #    optional :as_json, type: Hash
     #  end
     #  post '/index' do
     #    authenticate! :user
     #    status 200
     #    tags = Tag._index(params || {})
     #    tags.as_json(params[:as_json])
     #  end

      params do
        requires :external_id, type: String
        optional :opt, type: String, desc: 'Options, format: [key1:value1[;key2:value2[;...]]]'
      end
      get '/show/:external_id' do
        authenticate! :user
        tag = ::Tag._show(params.slice(:external_id))

        raise ActiveRecord::RecordNotFound if !tag

        authorize tag, :show?

        parsed_params = parse_qs(params)

        _include = []
        if parsed_params[:opt] && parsed_params[:opt][:posts]&.to_bool == true
          _include << :posts
        end

        status 200
        return tag.as_json(include: _include)
      end

      # params do
      #   requires :external_id, type: String
      #   optional :as_json, type: Hash
      # end
      # post '/show' do
      #   authenticate! :user
      #   post = ::Post._show(params.slice(:external_id))
      #
      #   raise ActiveRecord::RecordNotFound if !post
      #
      #   authorize post, :show?
      #   status 200
      #   post.as_json(params[:as_json])
      # end
    
    end
  end
end
