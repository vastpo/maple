module API::V1::User
  class Widgets < Grape::API
    namespace '/widgets' do
 
      params do
        requires :managed_by_id, type: Integer
        optional :order, type: String, desc: 'Ordering, format: [key1:asc|desc[;key2:asc|desc[;...]]]'
        optional :opt, type: String, desc: 'Options, format: [key1:value1[;key2:value2[;...]]]'
        optional :pagination, type: String, desc: 'Pagination, format: [page:Integer[;per:Integer]]'
      end
      get '/index' do
        authenticate! :user

        # authorize ::Widget.new(managed_by_id: params[:managed_by_id]), :index?

        parsed_params = parse_qs(params).merge(where: { managed_by_id: params[:managed_by_id] })

        widgets = ::Widget._index(parsed_params)
        return widgets
      end

     # params do
     #    optional :where, type: Hash
     #    optional :as_json, type: Hash
     #  end
     #  post '/index' do
     #    status 200
     #    widgets = ::Widget._index(params || {})
     #    widgets.as_json(params[:as_json])
     #  end

      params do
        requires :external_id, type: String
        optional :opt, type: String, desc: 'Options, format: [key1:value1[;key2:value2[;...]]]'
      end
      get '/show/:external_id' do
        authenticate! :user
        widget = ::Widget._show(params.slice(:external_id))

        raise ActiveRecord::RecordNotFound if !widget

        authorize widget, :show?
        status 200
        return widget
      end

      # params do
      #   requires :external_id, type: String
      #   optional :as_json, type: Hash
      # end
      # post '/show' do
      #   widget = ::Widget._show(params.slice(:external_id))
      #
      #   raise ActiveRecord::RecordNotFound if !widget
      #
      #   status 200
      #   widget.as_json(params[:as_json])
      # end

    end
  end
end
