module API::V1::User
  class Comments < Grape::API
    namespace '/comments' do
      
      params do
        requires :post_id, type: Integer
        optional :order, type: String, desc: 'Ordering, format: [key1:asc|desc[;key2:asc|desc[;...]]]'
        optional :opt, type: String, desc: 'Options, format: [key1:value1[;key2:value2[;...]]]'
        optional :pagination, type: String, desc: 'Pagination, format: [page:Integer[;per:Integer]]'
      end
      get '/index' do
        authenticate! :user

        # authorize ::Comment.new(post_id: params[:post_id]), :index?

        parsed_params = parse_qs(params).merge(where: { post_id: params[:post_id] })

        comments = ::Comment._index(parsed_params)
        return comments
      end

      # params do
      #   optional :where, type: Hash
      #   optional :as_json, type: Hash
      #   optional :pagination, type: Hash
      # end
      # post '/index' do
      #   authenticate! :user 
      #   status 200
      #   comments = ::Comment._index(params || {} )
      #   comments.as_json(params[:as_json])
      # end

      params do
        requires :external_id, type: String
        optional :opt, type: String, desc: 'Options, format: [key1:value1[;key2:value2[;...]]]'
      end
      get '/show/:external_id' do
        authenticate! :user
        comment = ::Comment._show(params.slice(:external_id))

        raise ActiveRecord::RecordNotFound if !comment

        authorize comment, :show?
        status 200
        return comment
      end

      # params do
      #   requires :external_id, type: String
      #   optional :as_json, type: Hash
      # end
      # post '/show' do
      #   authenticate! :user
      #   comment = ::Comment._show(params.slice(:external_id))
      #
      #   raise ActiveRecord::RecordNotFound if !comment
      #
      #   authorize comment, :show?
      #   status 200
      #   comment.as_json(params[:as_json])
      # end

      params do
        requires :post_id, type: Integer
        optional :parent_id, type: Integer
        requires :content, type: String
      end
      post '/create' do
        authenticate! :user
        authorize Comment.new, :create?
        comment = Comment._create!({ created_by: @current_member }.merge(params))
        present comment, with: API::V1::Entities::Comment
      end
    
    end
  end
end
