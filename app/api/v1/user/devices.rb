module API::V1::User
  class Devices < Grape::API
    namespace '/devices' do

      params do
        requires :device_id, type: String
        requires :operating_system, type: String
        requires :token, type: String
        requires :country, type: String
        requires :service, type: String
      end
      post '/create' do
        authenticate! :user

        device = ::Device._show(device_id: params[:device_id])
        user_device = ::UsersDevices.find_by(user_id: @current_member.id, device_id: params[:device_id])

        if !device.present?
          device = ::Device._create!(params)
        else
          device.update!(params.slice(*Device._attrs))
        end

        if !user_device.present?
          user_device = ::UsersDevices.create!(user_id: @current_member.id, device_id: params[:device_id], is_active: true)
        else
          user_device.update!(is_active: true)
        end
        ::UsersDevices.where('user_id <> ?', @current_member.id).where(device_id: params[:device_id]).update_all(is_active: false)

        present device, with: API::V1::Entities::Device
      end

    end
  end
end

