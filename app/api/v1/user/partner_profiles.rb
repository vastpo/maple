module API::V1::User
  class PartnerProfiles < Grape::API
    namespace '/partner_profiles' do

      params do
        optional :order, type: String, desc: 'Ordering, format: [key1:asc|desc[;key2:asc|desc[;...]]]'
        optional :opt, type: String, desc: 'Options, format: [key1:value1[;key2:value2[;...]]]'
        optional :pagination, type: String, desc: 'Pagination, format: [page:Integer[;per:Integer]]'
      end
      get '/index' do
        authenticate! :user

        # authorize ::PartnerProfile.new(partner_id: params[:partner_id]), :index?

        parsed_params = parse_qs(params)

        partner_profiles = ::PartnerProfile._index(parsed_params)
        return partner_profiles
      end

      # params do
      #   optional :where, type: Hash
      #   optional :as_json, type: Hash
      # end
      # post '/index' do
      #   status 200
      #   partner_profiles = ::PartnerProfile._index(params || {})
      #   partner_profiles.as_json(params[:as_json])
      # end

      params do
        requires :external_id, type: String
        optional :opt, type: String, desc: 'Options, format: [key1:value1[;key2:value2[;...]]]'
      end
      get '/show/:external_id' do
        authenticate! :user
        partner_profile = ::PartnerProfile._show(params.slice(:external_id))

        raise ActiveRecord::RecordNotFound if !partner_profile

        authorize partner_profile, :show?
        status 200
        return partner_profile.as_json
      end

      # params do
      #   requires :external_id, type: String
      #   optional :as_json, type: Hash
      # end
      # post '/show' do
      #   authenticate! :user
      #   partner_profile = ::PartnerProfile._show(params.slice(:external_id))
      #
      #   raise ActiveRecord::RecordNotFound if !partner_profile
      #
      #   authorize partner_profile, :show?
      #   status 200
      #   partner_profile.as_json(params[:as_json])
      # end

    end
  end
end
