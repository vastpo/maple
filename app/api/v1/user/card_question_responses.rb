module API::V1::User
  class CardQuestionResponses < Grape::API
    namespace '/card_question_responses' do

      params do
        requires :question_id, type: Integer
        requires :device_id, type: String
        requires :response, type: Array
      end
      post '/create' do
        authenticate! :user

        params[:user_id] = @current_member.id

        begin
          raise 'ALREADY_RESPONSED' if CardQuestionResponse.exists?(question_id: params[:question_id], user_id: params[:user_id], device_id: params[:device_id])
          card_question_responses = CardQuestionResponse._create!(params)
          return {
            status: :success,
            result: card_question_responses
          }
        rescue StandardError => e
          return {
            status: :error,
            message: e.message
          }
        end
      end

    end
  end
end

