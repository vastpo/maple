module API::V1::Admin
  class Base < Grape::API
    namespace '/admin' do
      params do
        requires :login, type: String
        requires :password, type: String
      end
      post '/signin' do
        signin ::Admin, params[:login], params[:password]
      end

      params do
        requires :login, type: String, regexp: Regexp.new(Settings.credentials.login)
        requires :password, type: String
        requires :display_name, type: String
        optional :first_name, type: String
        optional :last_name, type: String
        requires :nickname, type: String
        optional :gender, type: String
        optional :phones, type: Array
        optional :emails, type: Array
        optional :roles, type: Array
        optional :role_details, type: Hash
      end
      post '/signup' do
        authenticate! :admin
        authorize Admin.new, :create?
        admin = ::Admin._create! params
        present admin, with: API::V1::Entities::Admin
      end

      params do
        optional :where, type: Hash
        optional :as_json, type: Hash
        optional :pagination, type: Hash
      end
      post '/index' do
        authenticate! :admin
        status 200
        admins= ::Admin._index(params || {})
        admins.as_json(params[:as_json])
      end

      params do
        requires :id, type: Integer
        optional :as_json, type: Hash
      end
      post '/show' do
        authenticate! :admin
        authorize Admin.new(id: params[:id]), :show?
        status 200
        ::Admin._show(params).as_json(params[:as_json])
      end

      params do
        requires :id, type: Integer
        optional :login, type: String
        optional :password, type: String
        optional :display_name, type: String
        optional :first_name, type: String
        optional :last_name, type: String
        optional :nickname, type: String
        optional :gender, type: String
        optional :phones, type: Array
        optional :emails, type: Array
        optional :roles, type: Array
      end
      put '/modify' do
        authenticate! :admin

        target_admin = ::Admin.find(params[:id])
        target_admin.assign_attributes(params.slice(*Admin.attribute_names.map(&:to_sym)))
        authorize target_admin, :update?

        status 202
        present target_admin._update!(params), with: API::V1::Entities::Admin
      end

      mount API::V1::Admin::PartnerAccessInvitations
      mount API::V1::Admin::Posts
      mount API::V1::Admin::Series
      mount API::V1::Admin::Columns
      mount API::V1::Admin::Widgets
      mount API::V1::Admin::Cards
      mount API::V1::Admin::Tags
      mount API::V1::Admin::ThunderOffers
      mount API::V1::Admin::PartnerPayoutPolicies
      mount API::V1::Admin::Comments
      mount API::V1::Admin::Images
      mount API::V1::Admin::Videos
      mount API::V1::Admin::AttachedMedia
    end
  end
end
