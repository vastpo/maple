module API::V1::PartnerAdmin
  class Base < Grape::API
    namespace '/partner_admin' do
      params do
        requires :login, type: String
        requires :password, type: String
      end
      post '/signin' do
        signin ::PartnerAdmin, params[:login], params[:password]
      end

      params do
        requires :login, type: String, regexp: Regexp.new(Settings.credentials.login)
        requires :password, type: String
        requires :timezone, type: Integer
        optional :access_details, type: Hash
      end
      post '/signup' do
        authenticate! :admin
        partner_admin = ::PartnerAdmin._create! params
        present partner_admin, with: API::V1::Entities::PartnerAdmin
      end

      post '/show' do
        authenticate! :partner_admin
        present @current_member, with: API::V1::Entities::PartnerAdmin
      end

      mount API::V1::PartnerAdmin::Posts
      mount API::V1::PartnerAdmin::Series
      mount API::V1::PartnerAdmin::Columns
      mount API::V1::PartnerAdmin::Widgets
      mount API::V1::PartnerAdmin::Cards
      mount API::V1::PartnerAdmin::Tags
      mount API::V1::PartnerAdmin::ThunderOffers
      mount API::V1::PartnerAdmin::Comments
      mount API::V1::PartnerAdmin::Images
      mount API::V1::PartnerAdmin::Videos
      mount API::V1::PartnerAdmin::AttachedMedia
      mount API::V1::PartnerAdmin::PartnerProfiles
      mount API::V1::PartnerAdmin::PartnerAccess
      mount API::V1::PartnerAdmin::Emojis
    end
  end
end
