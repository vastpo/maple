module API::V1::General
  class Series < Grape::API
    namespace '/series' do
 
      params do
        requires :partner_id, type: Integer
        optional :order, type: String, desc: 'Ordering, format: [key1:asc|desc[;key2:asc|desc[;...]]]'
        optional :opt, type: String, desc: 'Options, format: [key1:value1[;key2:value2[;...]]]'
        optional :pagination, type: String, desc: 'Pagination, format: [page:Integer[;per:Integer]]'
      end
      get '/index' do

        parsed_params = parse_qs(params).merge(where: { id: ::Partner.find(params[:partner_id]).series_access.pluck(:series_id) })

        series = ::Series._index(parsed_params)
        return series
      end

     # params do
     #    optional :where, type: Hash
     #    optional :as_json, type: Hash
     #  end
     #  post '/index' do
     #    status 200
     #    series = ::Series._index(params || {})
     #    series.as_json(params[:as_json])
     #  end

      params do
        requires :external_id, type: String
        optional :opt, type: String, desc: 'Options, format: [key1:value1[;key2:value2[;...]]]'
      end
      get '/show/:external_id' do
        series = ::Series._show(params.slice(:external_id))

        raise ActiveRecord::RecordNotFound if !series

        status 200
        series = series.as_json(
          include: {
            series_access: {
              include: { partner: { include: { profile: { only: [ :external_id, :display_name, :media_details ] } } } }
            },
            sub_series: {
              chain: [ :have_posts ],
              only: [ :external_id, :title ]
            }
          },
          only: [ :title, :long_note, :media_details ]
        )
        series = { cover: series[:media_details][:cover] }.merge(series)
        series[:media_details].delete(:cover)
        series[:series_access] = series[:series_access].map{ |sa| sa = sa[:partner][:profile] }
        return series
      end

      # params do
      #   requires :external_id, type: String
      #   optional :as_json, type: Hash
      # end
      # post '/show' do
      #   authenticate! :general
      #   series = ::Series._show(params.slice(:external_id))
      #
      #   raise ActiveRecord::RecordNotFound if !series
      #
      #   authorize series, :show?
      #   status 200
      #   series.as_json(params[:as_json])
      # end
    
    end
  end
end
