module API::V1::General
  class Videos < Grape::API
    namespace '/videos' do

      params do
        optional :order, type: String, desc: 'Ordering, format: [key1:asc|desc[;key2:asc|desc[;...]]]'
        optional :opt, type: String, desc: 'Options, format: [key1:value1[;key2:value2[;...]]]'
        optional :pagination, type: String, desc: 'Pagination, format: [page:Integer[;per:Integer]]'
      end
      get '/index' do

        parsed_params = parse_qs(params)

        videos = ::Video._index(parsed_params)
        return videos
      end

      # params do
      #   optional :where, type: Hash
      #   optional :as_json, type: Hash
      # end
      # post '/index' do
      #   authenticate! :general
      #   status 200
      #   videos = ::Video._index(params || {})
      #   videos.as_json(params[:as_json])
      # end

      params do
        requires :external_id, type: String
        optional :opt, type: String, desc: 'Options, format: [key1:value1[;key2:value2[;...]]]'
      end
      get '/show/:external_id' do
        video = ::Video._show(params.slice(:external_id))

        raise ActiveRecord::RecordNotFound if !video

        status 200
        return video.as_json
      end

      # params do
      #   requires :external_id, type: String
      #   optional :as_json, type: Hash
      # end
      # post '/show' do
      #   authenticate! :general
      #   video = ::Video._show(params.slice(:external_id))
      #
      #   raise ActiveRecord::RecordNotFound if !video
      #
      #   status 200
      #   video.as_json(params[:as_json])
      # end

    end
  end
end
