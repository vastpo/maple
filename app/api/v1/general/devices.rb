module API::V1::General
  class Devices < Grape::API
    namespace '/devices' do

      params do
        requires :device_id, type: String
        requires :operating_system, type: String
        requires :token, type: String
        requires :country, type: String
        requires :service, type: String
      end
      post '/create' do
        device = ::Devices._show(device_id: params[:device_id])
        if device.present?
          new_device = device.update!(params)
        else
          new_device = ::Device.create!(params)
        end
        present new_device, with: API::V1::Entities::Device
      end

    end
  end
end
