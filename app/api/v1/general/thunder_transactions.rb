module API::V1::General
  class ThunderTransactions < Grape::API
    namespace '/thunder_transactions' do

      params do
        requires :thunder_offer_id, type: Integer
        requires :currency, type: String
        requires :stripe_charge, type: Hash do
          # requires :amount, type: Float
          requires :source, type: String
          optional :description, type: String
          # optional :destination, type: Hash do
          #   requires :amount, type: Float
          #   requires :account, type: String
          # end
        end
        optional :stripe_customer_id, type: String
        optional :user_id, type: Integer
      end
      post '/create' do
        begin
          thunder_offer = ThunderOffer.find(params[:thunder_offer_id])
          params[:amount] = thunder_offer.amount[:pricing_scheme][params[:currency].to_sym]
          params[:fee] = thunder_offer.fee[params[:currency].to_sym]
          raise 'Currency is not supported' if (!params[:amount].present? || !params[:fee].present?)
          params[:destination_account] = thunder_offer.destination_account
          params[:is_stripe_payout] = (params[:destination_account].present? ? thunder_offer.is_stripe_payout : false)

          multiplier = 10 ** Configs::Currency.find(params[:currency]).exponent
          params[:stripe_charge].merge!({
            amount: (params[:amount] * multiplier).humanize,
            currency: params[:currency],
            destination: {
              amount: (params[:amount].minus(params[:fee]) * multiplier).humanize,
              account: params[:destination_account]
            },
            metadata: {
              partner_id: thunder_offer.partner_id,
              post_id: thunder_offer.post_id,
              thunder_offer_id: params[:thunder_offer_id]
            }
          })

          charge = Stripe::Charge.create(params[:stripe_charge])

          params[:stripe_charge_id] = charge.id
          params[:stripe_transfer_id] = charge.transfer
          params[:is_captured] = charge.captured

          thunder_transaction = ThunderTransaction._create!(params)
          present thunder_transaction, with: API::V1::Entities::ThunderTransaction
        rescue Exception => e
          status 500
          return {
            error: e.message
          }
        end
      end

    end
  end
end
