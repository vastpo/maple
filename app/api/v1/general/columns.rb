module API::V1::General
  class Columns < Grape::API
    namespace '/columns' do

      params do
        optional :order, type: String, desc: 'Ordering, format: [key1:asc|desc[;key2:asc|desc[;...]]]'
        optional :opt, type: String, desc: 'Options, format: [key1:value1[;key2:value2[;...]]]'
        optional :pagination, type: String, desc: 'Pagination, format: [page:Integer[;per:Integer]]'
      end
      get '/index' do

        parsed_params = parse_qs(params)

        columns = ::Column._index(parsed_params)
        return columns
      end

      # params do
      #   optional :where, type: Hash
      #   optional :as_json, type: Hash
      # end
      # post '/index' do
      #   status 200
      #   columns = ::Column._index(params || {})
      #   columns.as_json(params[:as_json])
      # end

      params do
        requires :external_id, type: String
        optional :opt, type: String, desc: 'Options, format: [key1:value1[;key2:value2[;...]]]'
      end
      get '/show/:external_id' do
        column = ::Column._show(params.slice(:external_id))

        raise ActiveRecord::RecordNotFound if !column

        status 200
        return column.as_json(include: [:attached_media, :posts])
      end

      # params do
      #   requires :external_id, type: String
      #   optional :as_json, type: Hash
      # end
      # post '/show' do
      #   column = ::Column._show(params.slice(:external_id))
      #
      #   raise ActiveRecord::RecordNotFound if !column
      #
      #   status 200
      #   column.as_json(params[:as_json])
      # end

    end
  end
end
