module API::V1::General
  class Posts < Grape::API
    namespace '/posts' do

      x = {
        order: { updated_at: :desc },
        # pagination: params[:pagination],
        as_json: {
          only: [:external_id, :post_type, :title, :note, :media_details, :interaction_detail, :base_url, :is_masked, :is_emoji_blocked, :is_comment_blocked, :is_capture_blocked ],
          include: {
            managed_by: {
              include: {
                profile: {
                  only: [ :external_id, :display_name, :media_details ]
                }
              },
              only: []
            },
            active_series: {
              chain: [ :root ],
              only: [ :external_id, :title, :short_note, :media_details ]
            },
            active_cards: {
              only: [ :external_id, :color_scheme, :conversion_text ]
            },
            active_thunder_offers: {
            }
          }
        }
      }

      post_opts = {
        only: [
          :external_id,
          :post_type,
          :title,
          :note,
          :media_details,
          :interaction_details,
          :is_masked,
          :is_emoji_blocked,
          :is_comment_blocked,
          :is_capture_blocked,
          :base_url
        ],
        include: {
          managed_by: {
            only: [  ],
            include: {
              profile: {
                only: [ :external_id, :display_name, :media_details ]
              }
            }
          },
          active_series: {
            chain: [ :root ],
            only: [ :external_id, :title, :media_details ]
          },
          active_cards: {
            only: [ :external_id, :conversion_text, :color_scheme ]
          },
          thunder_offers: {
            only: [ :external_id, :note, :amount ],
            where: { is_active: true }
          }
        }
      }

      params do
        optional :managed_by_id, type: Integer
        optional :order, type: String, desc: 'Ordering, format: [key1:asc|desc[;key2:asc|desc[;...]]]'
        optional :opt, type: String, desc: 'Options, format: [key1:value1[;key2:value2[;...]]]'
        optional :pagination, type: String, desc: 'Pagination, format: [page:Integer[;per:Integer]]'
      end
      get '/index' do
        parsed_params = parse_qs(params)
        
        if params[:managed_by_id].present?
          parsed_params.merge!(where: { managed_by_id: params[:managed_by_id] })
        end

        posts = ::Post._index(parsed_params)
        return posts
      end

      # params do
      #   optional :where, type: Hash
      #   optional :as_json, type: Hash
      # end
      # post '/index' do
      #   status 200
      #   posts = ::Post._index(params || {})
      #   posts.as_json(params[:as_json])
      # end

      params do
        requires :external_id, type: String
        optional :opt, type: String, desc: 'Options, format: [key1:value1[;key2:value2[;...]]]'
      end
      get '/show/:external_id' do
        post = ::Post._show(params.slice(:external_id))

        raise ActiveRecord::RecordNotFound if !post

        status 200
        post =  post.as_json(post_opts)

        post[:managed_by] = post[:managed_by][:profile]
        post = { _class: 'Post', icon: post[:managed_by][:media_details][:icon] }.merge(post)
        post[:managed_by].delete(:media_details)

        # clone_post = post.clone
        # post[:suggested_posts] = Array.new(10, clone_post)

        return post
      end

      params do
        requires :external_id, type: String
      end
      get '/suggested' do
        ref_post = ::Post._show(params.slice(:external_id))
        related_post_ids = Post.where('id <> ?', ref_post.id).where(post_type: ref_post.post_type).pluck(:id)
        # suggested_posts = Post.where(id: related_post_ids.sample(10))._index({ as_json: post_opts })
        suggested_posts = Post.where(id: [ 70, 235, 271, 275, 427, 493, 540, 550, 702, 766 ])._index({ as_json: post_opts }) # hardcoded
        suggested_posts = suggested_posts[:result].map do |e|
          e[:managed_by] = e[:managed_by][:profile]
          e = { icon: e[:managed_by][:media_details][:icon] }.merge(e)
          e[:managed_by].delete(:media_details)
          e
        end
      end

      params do
        requires :external_id, type: String
      end
      get '/associated' do
        post = ::Post._show(params.slice(:external_id))

        raise ActiveRecord::RecordNotFound if !post

        status 200
        post =  post.as_json(post_opts)

        post[:managed_by] = post[:managed_by][:profile]
        post = { _class: 'Post', icon: post[:managed_by][:media_details][:icon] }.merge(post)
        post[:managed_by].delete(:media_details)

        ref_post = ::Post._show(params.slice(:external_id))
        related_post_ids = Post.where('id <> ?', ref_post.id).where(post_type: ref_post.post_type).pluck(:id)
        # suggested_posts = Post.where(id: related_post_ids.sample(10))._index({ as_json: post_opts })
        suggested_posts = Post.where(id: [ 70, 235, 271, 275, 427, 493, 540, 550, 702, 766 ])._index({ as_json: post_opts }) # hardcoded
        suggested_posts = suggested_posts[:result].map do |e|
          e[:managed_by] = e[:managed_by][:profile]
          e = { icon: e[:managed_by][:media_details][:icon] }.merge(e)
          e[:managed_by].delete(:media_details)
          e
        end

        return [post] + suggested_posts
      end

      # params do
      #   requires :external_id, type: String
      #   optional :as_json, type: Hash
      # end
      # post '/show' do
      #   post = ::Post._show(params.slice(:external_id))
      #
      #   raise ActiveRecord::RecordNotFound if !post
      #
      #   authorize post, :show?
      #   status 200
      #   post.as_json(params[:as_json])
      # end

      params do
        requires :external_id, type: String
      end
      get '/list/:external_id/series_media' do
        post = ::Post._show(params.slice(:external_id))

        raise ActiveRecord::RecordNotFound if !post

        status 200
        return post.series.map(&:attached_media).flatten.map(&:media)
      end

      params do
        requires :series_external_id, type: String
        optional :order, type: Hash
        optional :opt, type: Hash
        requires :pagination, type: Hash
      end
      post '/list/by_series' do
        series = ::Series._show(external_id: params[:series_external_id])

        raise ActiveRecord::RecordNotFound if !series

        status 200

        sub_series = series.sub_series.where(state: ::Series::STATE_PUBLISHED)
        x[:pagination] = params[:pagination]
        posts = if sub_series.present?
                  Post.joins(:series).where('series.id IN (?)', sub_series.pluck(:id)).where(state: Post::STATE_PUBLISHED)._index(x)
                else
                  Post.joins(:series).where('series.id = ?', series.id).where(state: Post::STATE_PUBLISHED)._index(x)
                end
        posts[:result] = posts[:result].map do |e|
          e[:managed_by] = e[:managed_by][:profile]
          e = { _class: 'Post', icon: e[:managed_by][:media_details][:icon] }.merge(e)
          e[:managed_by].delete(:media_details)
          e
        end
        return posts
      end

    end
  end
end
