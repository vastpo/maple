module API::V1::General
  class Partners < Grape::API
    namespace '/partners' do
      params do
        optional :partner_external_id, type: String
      end
      get '/series' do
        ::Partner.find_by(external_id: params[:partner_external_id]).managing_series.order(id: :desc).limit(100)
      end
    end
  end
end
