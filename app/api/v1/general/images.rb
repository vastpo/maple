module API::V1::General
  class Images < Grape::API
    namespace '/images' do

      params do
        optional :order, type: String, desc: 'Ordering, format: [key1:asc|desc[;key2:asc|desc[;...]]]'
        optional :opt, type: String, desc: 'Options, format: [key1:value1[;key2:value2[;...]]]'
        optional :pagination, type: String, desc: 'Pagination, format: [page:Integer[;per:Integer]]'
      end
      get '/index' do

        parsed_params = parse_qs(params)

        images = ::Image._index(parsed_params)
        return images
      end

      # params do
      #   optional :where, type: Hash
      #   optional :as_json, type: Hash
      # end
      # post '/index' do
      #   authenticate! :general
      #   status 200
      #   images = ::Image._index(params || {})
      #   images.as_json(params[:as_json])
      # end

      params do
        requires :external_id, type: String
        optional :opt, type: String, desc: 'Options, format: [key1:value1[;key2:value2[;...]]]'
      end
      get '/show/:external_id' do
        image = ::Image._show(params.slice(:external_id))

        raise ActiveRecord::RecordNotFound if !image

        status 200
        return image.as_json
      end

      # params do
      #   requires :external_id, type: String
      #   optional :as_json, type: Hash
      # end
      # post '/show' do
      #   authenticate! :general
      #   image = ::Image._show(params.slice(:external_id))
      #
      #   raise ActiveRecord::RecordNotFound if !image
      #
      #   status 200
      #   image.as_json(params[:as_json])
      # end

    end
  end
end
