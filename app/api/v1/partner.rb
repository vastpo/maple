module API::V1::Partner
  class Base < Grape::API
    namespace '/partner' do
      params do
        requires :login, type: String
        requires :password, type: String
      end
      post '/signin' do
        signin ::Partner, params[:login], params[:password]
      end

      params do
        requires :login, type: String, regexp: Regexp.new(Settings.credentials.login)
        requires :password, type: String
        requires :display_name, type: Hash do I18n.available_locales.each { |l| requires l, type: String } end
        requires :preferred_language, type: String
        requires :preferred_currency, type: String
        optional :short_note, type: Hash do I18n.available_locales.each { |l| requires l, type: String } end
        optional :long_note, type: Hash do I18n.available_locales.each { |l| requires l, type: String } end
        optional :media_details, type: Hash
        optional :interaction_details, type: Hash
        optional :score_details, type: Hash
        optional :links, type: Array
        requires :is_ad_disabled, type: Boolean
        optional :is_review_profile, type: Boolean
        requires :payout_policy, type: Hash do
          requires :fee_rate, type: Hash do Configs::Currency.pluck(:iso_code).map(&:to_sym).each { |c| optional c, type: Float } end
          requires :fee_fix, type: Hash do Configs::Currency.pluck(:iso_code).map(&:to_sym).each { |c| optional c, type: Float } end
          optional :rpe, type: Float
          requires :is_active, type: Boolean
          optional :remarks, type: String
        end
        requires :content_policy, type: Hash do
          requires :is_allowing_post, type: Boolean
          requires :is_allowing_series, type: Boolean
          requires :is_allowing_widget, type: Boolean
          requires :is_allowing_live, type: Boolean
          requires :is_allowing_ad, type: Boolean
          requires :is_post_ad_compulsory, type: Boolean
          requires :is_series_ad_compulsory, type: Boolean
          requires :is_widget_ad_compulsory, type: Boolean
          requires :is_live_ad_compulsory, type: Boolean
          requires :is_review_required, type: Boolean
          requires :default_delivery_locations, type: Hash
          requires :post_limit_per_day, type: Integer
          requires :post_limit_per_month, type: Integer
          requires :series_limit_per_day, type: Integer
          requires :series_limit_per_month, type: Integer
          requires :widget_limit_per_day, type: Integer
          requires :widget_limit_per_month, type: Integer
          requires :video_max_sec, type: Integer
          requires :video_max_kb, type: Integer
          requires :image_max_kb, type: Integer
          requires :is_active, type: Boolean
          optional :remarks, type: String
        end
        optional :stripe_account, type: Hash, default: false do
          requires :type, type: String, values: [ 'standard', 'custom' ]
          optional :country, type: String, values: ISO3166::Country.countries.map(&:alpha2).sort, desc: 'ISO 3166-1 alpha-2 country code'
          optional :email, type: String, desc: 'Required id type is "standard"'
          optional :account_token, type: String, desc: 'Used to securely provide details to the account'
          optional :business_logo, type: String, desc: '(ID of a file upload) A logo for this account (at least 128px x 128px)'
          optional :business_name, type: String, desc: 'The publicly sharable name for this account'
          optional :business_url, type: String, desc: 'The URL that best shows the service or product provided by this account.'
          optional :default_currency, type: String, values: ISO3166::Country.countries.map{ |c| c.currency_code&.downcase }.compact.sort.uniq, desc: 'Three-letter ISO currency code representing the default currency for the account. This must be a currency that Stripe supports in the account’s country.'
          optional :external_account, type: Hash do
            requires :object, type: String, values: [ 'bank_account' ]
            requires :country, type: String, values: ISO3166::Country.countries.map(&:alpha2).sort, desc: 'The country in which the bank account is located'
            requires :currency, type: String, values: ISO3166::Country.countries.map{ |c| c.currency_code&.downcase }.compact.sort.uniq, desc: 'The currency the bank account is in'
            optional :account_holder_name, type: String, desc: 'The name of the person or business that owns the bank account'
            optional :account_holder_type, type: String, values: [ 'individual', 'company' ], desc: 'The type of entity that holds the account'
            optional :routing_number, type: String, desc: 'The routing number, sort code, or other country-appropriate institution number for the bank account. For US bank accounts, this is required and should be the ACH routing number, not the wire routing number. If you are providing an IBAN for account_number, this field is not required.'
            requires :account_number, type: String, desc: 'The account number for the bank account, in string form. Must be a checking account'
          end
          optional :legal_entity, type: Hash
          optional :metadata, type: String, desc: 'Set of key-value pairs that you can attach to an object. This can be useful for storing additional information about the object in a structured format.'
          optional :payout_schedule, type: Hash do
            optional :delay_days, type: Integer, desc: 'The number of days charges for the account will be held before being paid out.'
            optional :interval, type: String, values: [ 'daily', 'weekly', 'monthly', 'manual' ], desc: 'How frequently funds will be paid out.'
            optional :monthly_anchor, type: Integer, desc: 'The day of the month funds will be paid out. Only shown if interval is monthly. Payouts scheduled between 29-31st of the month are sent on the last day of shorter months.'
            optional :weekly_anchor, type: String, values: [ 'sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'saturday' ], desc: 'The day of the week funds will be paid out, of the style ‘monday’, ‘tuesday’, etc. Only shown if interval is weekly.'
          end
					optional :payout_statement_descriptor, type: String, desc: 'The text that appears on the bank account statement for payouts. If not set, this defaults to the platform’s bank descriptor as set in the Dashboard.'
					optional :product_description, type: String, desc: 'Internal-only description of the product sold or service provided by the business. It’s used by Stripe for risk and underwriting purposes.'
					optional :statement_descriptor, type: String, desc: 'The default text that appears on credit card statements when a charge is made directly on the account'
          optional :support_email, type: String, desc: 'A publicly shareable support email address for the business'
          optional :support_phone, type: String, desc: 'A publicly shareable support phone number for the business'
          optional :support_url, type: String
        end
      end
      post '/signup' do
        authenticate! :admin
        authorize ::Partner.new, :create?
        begin
          ActiveRecord::Base.transaction do
            if params[:stripe_account]
              stripe_account = Stripe::Account.create(params[:stripe_account])
              params[:stripe_account_id] = stripe_account.id
            end
            partner = ::Partner._create! params
            present partner, with: API::V1::Entities::Partner
          end
        rescue Exception => e
          status 500
          return e.as_json
        end
      end

      params do
        optional :where, type: Hash
        optional :as_json, type: Hash
        optional :pagination, type: Hash
      end
      post '/index' do
        authenticate! :admin
        status 200
        partners = ::Partner._index(params || {})
        partners.as_json(params[:as_json])
      end

      params do
        requires :id, type: Integer
        optional :as_json, type: Hash
      end
      post '/show' do
        authenticate! :partner
        authorize Partner.new(id: params[:id]), :show?
        status 200
        ::Partner._show(params).as_json(params[:as_json])
      end

      params do
        requires :id, type: Integer
        optional :login, type: String, regexp: Regexp.new(Settings.credentials.login)
        optional :password, type: String
        optional :external_id, type: String
        optional :display_name, type: Hash do I18n.available_locales.each { |l| optional l, type: String } end
        optional :preferred_language, type: String
        optional :preferred_currency, type: String
        optional :short_note, type: Hash do I18n.available_locales.each { |l| requires l, type: String } end
        optional :long_note, type: Hash do I18n.available_locales.each { |l| requires l, type: String } end
        optional :media_details, type: Hash
        optional :links, type: Array
        optional :is_ad_disabled, type: Boolean
        optional :is_review_profile, type: Boolean
        optional :payout_policy, type: Hash
        optional :content_policy, type: Hash
      end
      put '/modify' do
        authenticate! :partner

        target_partner = ::Partner.find(params[:id])
        target_partner.assign_attributes(params.slice(*Partner.attribute_names.map(&:to_sym)))
        authorize target_partner, :update?

        status 202
        present target_partner._update!(params), with: API::V1::Entities::Partner
      end

      mount API::V1::Partner::AJAX
    end
  end
end
