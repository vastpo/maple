module API::V1
  class ExceptionHandler
    def initialize(member, exception)
      @member = member
      @exception = exception
    end

    def perform!
      handled_result = handle!
      if @exception.is_a?(Pundit::NotAuthorizedError)
        return 'Access denied' if (Rails.env.production? || Rails.env.staging?)
        handled_result
      else
        handled_result
      end
    end

    private def handle!
      if (Rails.env.production? || Rails.env.staging?)
        # do something for bugsnag
      end
      @exception.message
    end
  end
end
