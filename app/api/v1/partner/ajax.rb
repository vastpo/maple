module API::V1::Partner
  class AJAX < Grape::API
    namespace '/ajax' do

      namespace '/search' do

        params do
          requires :partner_external_id, type: String
          requires :term, type: String
        end
        get '/managing_series' do
          if params[:term].length < Settings.ajax.search.minimum_input_length
            return {
              status: :error,
              message: "Search term should be at least #{Settings.ajax.search.minimum_input_length} characters"
            }
          end
          term_criteria = I18n.available_locales.map do |locale|
            "LOWER(series.title->>'#{locale}') LIKE '%#{params[:term].downcase}%'"
          end
          ::Partner.find_by(external_id: params[:partner_external_id])
            .managing_series
            .where(parent_id: nil)
            .where(term_criteria.join(' OR '))
            .order(id: :desc)
            .limit(100)
            .as_json({
              include: :sub_series
            })
        end

      end

    end
  end
end
