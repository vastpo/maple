module API::V1::General
  class Base < Grape::API
    namespace '/general' do

      params do
        requires :posts, type: Hash, default: {}
        requires :series, type: Hash, default: {}
        requires :columns, type: Hash, default: {}
        requires :widgets, type: Hash, default: {}
        requires :tags, type: Hash, default: {}
        requires :partner_profiles, type: Hash, default: {}
      end
      post '/homes' do

        params[:posts][:where] ||= {}
        params[:posts][:where].merge!(state: :published)
        params[:posts][:as_json] ||= {}
        params[:posts][:as_json][:include] ||= []
        posts = ::Post.preload([{ managed_by: :profile }, :series])._index(params[:posts])[:result]
        posts.each do |post|
          post[:series] = post[:series]&.map do |series|
            ::Series.find(series[:hierarchy].last)
          end
        end
        posts = posts.map{ |e| { _class: 'Post' }.merge(e) }

        params[:series][:where] ||= {}
        params[:series][:as_json] ||= {}
        params[:series][:as_json][:include] ||= []
        params[:series][:where].merge!(parent: nil, state: :published)
        params[:series][:as_json][:include] << :sub_series
        # params[:series][:as_json][:include] << { posts: { order: { updated_at: :desc }, limit: 18 } }
        series = ::Series.preload(:sub_series, :posts)._index(params[:series])[:result]
        series = series.map{ |e| { _class: 'Series' }.merge(e) }

        params[:columns][:where] ||= {}
        params[:columns][:where].merge!(state: :published)
        columns = ::Column._index(params[:columns])[:result]
        columns = columns.map{ |e| { _class: 'Column' }.merge(e.as_json) }

        params[:widgets][:where] ||= {}
        params[:widgets][:where].merge!(state: :published)
        widgets = Widget._index(params[:widgets])[:result]
        widgets = widgets.map{ |e| { _class: 'Widget' }.merge(e.as_json) }

        params[:tags][:where] ||= {}
        params[:tags][:where].merge!(is_active: :true)
        tags = ::Tag._index(params[:tags])[:result]
        tags = tags.map{ |e| { _class: 'Tag' }.merge(e.as_json) }

        params[:partner_profiles][:where] ||= {}
        params[:partner_profiles][:as_json] ||= {}
        params[:partner_profiles][:as_json][:include] ||= []
        params[:partner_profiles][:where].merge!(state: :published)
        params[:partner_profiles][:as_json][:include] << { partner: { include: :managing_series } }
        partner_profiles = ::PartnerProfile.preload(partner: :managing_series)._index(params[:partner_profiles])[:result]
        partner_profiles = partner_profiles.map{ |e| { _class: 'PartnerProfile' }.merge(e) }

        results = [posts, series, columns, widgets, tags, partner_profiles].flatten.shuffle
        return results
        # return {
        #   posts: posts,
        #   series: series,
        #   columns: columns,
        #   widgets: widgets,
        #   tags: tags,
        #   partner_profiles: partner_profiles
        # }
      end

      params do
        requires :language, type: String, default: I18n.default_locale
        requires :page, type: Integer, default: 1
      end
      get '/mobile_home' do
        return { status: :failed, message: 'Fuck You!' } if !params[:page].present?
        posts = ::Post.preload({ managed_by: :profile }, :series, :cards).where(id: [1000, 999], state: :published).distinct._index({ # hardcoded
        # posts = ::Post.preload({ managed_by: :profile }, :series, :cards).where(state: :published).distinct._index({
          order: { updated_at: :desc },
          pagination: { page: params[:page] },
          as_json: {
            only: [:id, :external_id, :post_type, :title, :note, :media_details, :interaction_detail, :base_url],
            include: {
              managed_by: {
                include: {
                  profile: {
                    only: [ :external_id, :display_name, :media_details ]
                  }
                },
                only: []
              },
              active_series: {
                chain: [ :root ],
                only: [ :external_id, :title, :short_note, :media_details ]
              },
              active_cards: {
                only: [ :external_id, :tag_text, :color_scheme ]
              },
              active_thunder_offers: {
              }
            }
          }
        })
        posts = posts[:result].map do |e|
          e[:managed_by] = e[:managed_by][:profile]
          e = { _class: 'Post', icon: e[:managed_by][:media_details][:icon] }.merge(e)
          e[:managed_by].delete(:media_details)
          e[:has_active_thunder_offers] = e[:active_thunder_offers].count > 0
          e.delete(:active_thunder_offers)
          e
        end

        series = ::Series.preload({ series_access: { partner: :profile } }, :sub_series).joins(:posts).where('posts_series.is_active' => true, 'posts.state' => :published).where(parent: nil, state: :published).distinct._index({
          pagination: { page: params[:page] },
          as_json: {
            only: [ :external_id, :title, :short_note, :media_details, :interaction_details ],
            include: {
              series_access: {
                where: { is_active: true },
                include: {
                  partner: {
                    include: {
                      profile: {
                        only: [ :external_id, :display_name, :media_details ]
                      }
                    },
                    only: []
                  }
                },
                only: []
              },
              sub_series: {
                where: { state: :published },
                only: [ :external_id, :title ]
              }
            }
          }
        })
        series = series[:result].map do |e|
          e[:series_access] = e[:series_access].map { |sa| sa = sa[:partner][:profile] }
          e = { _class: 'Series', cover: e[:media_details][:cover] }.merge(e)
          e[:media_details].delete(:cover)
          e
        end

        banner_widgets = ::Widget.preload(managed_by: :profile).joins(:managed_by).where('partners.state = ? AND widgets.state = ? AND widgets.widget_type = ?', :active, :published, :banner).distinct._index({
          pagination: { page: params[:page] },
          as_json: {
            only: [ :id, :external_id, :title, :headline, :conversion_text, :directory_type, :directory, :color_scheme, :media_details ],
            include: {
              managed_by: {
                include: {
                  profile: {
                    only: [ :external_id, :display_name, :media_details ]
                  }
                },
                only: []
              }
            }
          }
        })

        banner_widgets = banner_widgets[:result].map do |e|
          e[:managed_by] = e[:managed_by][:profile]
          { _class: 'Widget', _subclass: 'BannerWidget' }.merge(e)
        end

        listed_widgets = ::Partner.preload(:profile, :widgets).joins(:widgets).where('partners.state = ? AND widgets.state = ? AND widgets.widget_type = ?', :active, :published, :list).distinct._index({
          pagination: { page: params[:page] },
          as_json: {
            include: {
              profile: {
                only: [ :external_id, :display_name, :media_details ]
              },
              widgets: {
                where: { state: :published, widget_type: :list },
                only: [ :id, :external_id, :title, :headline, :conversion_text, :directory_type, :directory, :color_scheme, :media_details ],
                order: { created_at: :desc }
              }
            },
            only: []
          }
        })

        listed_widgets = listed_widgets[:result].map { |e| { _class: 'Widget', _subclass: 'ListedWidget', managed_by: e[:profile], content: e[:widgets] } }

        loop do
          # results = [ posts, series, banner_widgets, listed_widgets ].flatten.shuffle
          results = [ posts, series, banner_widgets, listed_widgets ].flatten
          return results if (posts.count + series.count == 0) || !results.first[:_class].in?(['Series', 'Widget'])
        end
      end

      params do
        requires :model, type: String
      end
      get '/state' do
        begin
          params[:model].camelize.constantize.aasm.states.map(&:name)
        rescue StandardError
          status 500
          { status: :error, message: 'NO_STATE_DEFINITIONS' }
        end
      end

      mount API::V1::General::Posts
      mount API::V1::General::Series
      mount API::V1::General::Columns
      mount API::V1::General::Widgets
      mount API::V1::General::Tags
      mount API::V1::General::ThunderOffers
      mount API::V1::General::ThunderTransactions
      mount API::V1::General::Comments
      mount API::V1::General::Images
      mount API::V1::General::Videos
      mount API::V1::General::Partners
      mount API::V1::General::PartnerProfiles
      mount API::V1::General::Devices
      mount API::V1::General::CardQuestionResponses
    end
  end
end
