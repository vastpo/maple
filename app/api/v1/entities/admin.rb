module API::V1::Entities
  class Admin < Grape::Entity
    expose :id
    expose :login
    expose :password_digest
    expose :display_name
    expose :roles
    expose :state
    expose :profile, with: API::V1::Entities::AdminProfile
    expose :created_at
    expose :updated_at
  end
end
