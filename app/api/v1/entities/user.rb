module API::V1::Entities
  class User < Grape::Entity
    expose :id
    expose :login
    expose :password_digest
    expose :display_name
    expose :gender
    expose :dob_year
    expose :dob_month
    expose :dob_day
    expose :timezone
    expose :location
    expose :phone
    expose :phone_verified_at
    expose :email
    expose :connected_profiles
    expose :preferred_currency
    expose :is_hd_preferred
    expose :is_video_autoplay
    expose :partner_admin_details
    expose :state
    expose :created_at
    expose :updated_at
  end
end
