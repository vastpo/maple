module API::V1::Entities
  class Card < Grape::Entity
    expose :id
    expose :created_by
    expose :managed_by
    expose :external_id
    expose :accessible_locations
    expose :tag_text
    expose :conversion_text
    expose :start_note
    expose :end_note
    expose :questions
    expose :color_scheme
    expose :interaction_details
    expose :is_requiring_verification
    expose :is_displaying_result
    expose :state
    expose :created_at
    expose :updated_at
  end
end
