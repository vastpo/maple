module API::V1::Entities
  class PartnerPayoutPolicy < Grape::Entity
    expose :id
    expose :partner_id
    expose :fee_rate
    expose :fee_fix
    expose :currency
    expose :rpe
    expose :is_active
    expose :remarks
    expose :created_at
    expose :updated_at
  end
end
