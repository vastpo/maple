module API::V1::Entities
  class PartnerProfile < Grape::Entity
    expose :id
    expose :partner_id
    expose :external_id
    expose :display_name
    expose :short_note
    expose :long_note
    expose :links
    expose :is_badged 
    expose :media_details
    expose :interaction_details
    expose :score_details
    expose :is_ad_disabled
    expose :is_running_ad
    expose :base_url
    expose :state
    expose :created_at
    expose :updated_at
  end
end
