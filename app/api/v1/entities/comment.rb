module API::V1::Entities
  class Comment < Grape::Entity
    expose :id
    expose :created_by
    expose :post_id
    expose :parent_id
    expose :external_id
    expose :content
    expose :score_details
    expose :is_masked
    expose :is_hidden
    expose :created_at
    expose :updated_at
  end
end
