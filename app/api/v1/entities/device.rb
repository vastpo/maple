module API::V1::Entities
  class Device < Grape::Entity
    expose :device_id
    expose :operating_system
    expose :token
    expose :country
    expose :service
  end
end
