module API::V1::Entities
  class Image < Grape::Entity
    expose :id
    expose :managed_by
    expose :external_id
    expose :media_format
    expose :width
    expose :height
    expose :urls
    expose :exclusivity
    expose :transferability
    expose :sub_licensibility
    expose :royalty_terms
    expose :territorialities
    expose :remarks
    expose :is_grayscale
    expose :is_line_drawing
    expose :created_at
    expose :updated_at
  end
end
