module API::V1::Entities
  class Widget < Grape::Entity
    expose :id
    expose :created_by
    expose :managed_by
    expose :external_id
    expose :widget_type
    expose :title
    expose :headline
    expose :conversion_text
    expose :directory_type
    expose :directory
    expose :media_details
    expose :interaction_details
    expose :score_details
    expose :is_ad_disabled
    expose :is_running_ad
    expose :state
    expose :created_at
    expose :updated_at
  end
end
