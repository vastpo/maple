module API::V1::Entities
  class Partner < Grape::Entity
    expose :id
    expose :external_id
    expose :login
    expose :password_digest
    expose :display_name
    expose :preferred_language
    expose :preferred_currency
    expose :profile, with: API::V1::Entities::PartnerProfile
    expose :active_payout_policy, with: API::V1::Entities::PartnerPayoutPolicy
    expose :active_content_policy, with: API::V1::Entities::PartnerContentPolicy
    expose :stripe_account_id
    expose :created_at
    expose :updated_at
  end
end
