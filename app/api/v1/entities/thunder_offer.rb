module API::V1::Entities
  class ThunderOffer < Grape::Entity
    expose :id
    expose :created_by_id
    expose :partner_id
    expose :post_id 
    expose :external_id
    expose :note
    expose :is_stripe_payout
    expose :destination_account
    expose :amount
    expose :fee
    expose :is_active
    expose :created_at
    expose :updated_at
  end
end
