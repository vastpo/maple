module API::V1::Entities
  class Series < Grape::Entity
    expose :id
    expose :parent 
    expose :hierarchy
    expose :external_id
    expose :title
    expose :short_note
    expose :long_note
    expose :color_scheme
    expose :media_details
    expose :interaction_details
    expose :score_details
    expose :is_ad_disabled
    expose :is_running_ad
    expose :base_url
    expose :state
    expose :series_access
    expose :created_at
    expose :updated_at
  end
end
