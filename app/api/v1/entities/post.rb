module API::V1::Entities
  class Post < Grape::Entity
    expose :id
    expose :created_by
    expose :managed_by
    expose :external_id
    expose :post_type
    expose :title
    expose :note
    expose :is_masked
    expose :is_emoji_blocked
    expose :is_comment_blocked
    expose :is_capture_blocked
    expose :permitted_emojis
    expose :widget_details
    expose :media_details
    expose :interaction_details
    expose :score_details
    expose :is_ad_disabled
    expose :is_running_ad
    expose :is_counting_rpe
    expose :base_url
    expose :state
    expose :series
    expose :columns
    expose :tags
    expose :created_at
    expose :updated_at
  end
end
