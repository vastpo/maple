module API::V1::Entities
  class AttachedMedia < Grape::Entity
    expose :media_id
    expose :attachable
    expose :attached_for
    expose :created_at
    expose :updated_at
  end
end
