module API::V1::Entities
  class AdminProfile < Grape::Entity
    expose :id
    expose :admin_id
    expose :first_name
    expose :last_name
    expose :nickname
    expose :gender
    expose :phones
    expose :emails
    expose :role_details
    expose :created_at
    expose :updated_at
  end
end
