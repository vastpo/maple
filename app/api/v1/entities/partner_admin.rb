module API::V1::Entities
  class PartnerAdmin < Grape::Entity
    expose :id
    expose :user
    expose :login
    expose :timezone
    expose :access_details
    expose :state
    expose :managing_partners do |pa|
      pa.partner_access.where(is_active: true).order(is_default: :desc, sort_order: :asc, id: :desc).as_json(include: :partner).reject{ |mp| mp[:partner][:state] == :rejected }.map do |mp|
        _mp = mp.deep_symbolize_keys
        _mp[:partner_id] = _mp[:partner][:id]
        _mp[:partner_access_id] = _mp[:id]
        _mp.merge!(_mp[:partner]).except!(:id, :partner)
      end
    end
    expose :created_at
    expose :updated_at
  end
end
