module API::V1::Entities
  class Column < Grape::Entity
    expose :id
    expose :created_by
    expose :external_id
    expose :title
    expose :short_note
    expose :long_note
    expose :color_scheme
    expose :media_details
    expose :interaction_details
    expose :score_details
    expose :base_url
    expose :state
    expose :created_at
    expose :updated_at
  end
end
