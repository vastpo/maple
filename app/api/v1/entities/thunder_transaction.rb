module API::V1::Entities
  class ThunderTransaction < Grape::Entity
    expose :id
    expose :thunder_offer_id
    expose :user_id
    expose :stripe_customer_id
    expose :stripe_charge_id
    expose :stripe_transfer_id
    expose :is_stripe_payout
    expose :destination_account
    expose :currency
    expose :amount
    expose :fee
    expose :is_captured
    expose :created_at
    expose :updated_at
  end
end
