module API::V1::Entities
  class Tag < Grape::Entity
    expose :id
    expose :external_id
    expose :title
    expose :color_scheme
    expose :interaction_details
    expose :score_details
    expose :base_url
    expose :is_active
    expose :created_at
    expose :updated_at
  end
end
