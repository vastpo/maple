module API::V1::User
  class Base < Grape::API
    namespace '/user' do
      params do
        requires :login, type: String
        requires :password, type: String
      end
      post '/signin' do
        signin ::User, params[:login], params[:password]
      end

      params do
        requires :login, type: String, regexp: Regexp.new(Settings.credentials.login)
        requires :password, type: String
        requires :display_name, type: String
        requires :gender, type: String
        optional :dob_year, type: Integer
        optional :dob_month, type: Integer
        optional :dob_day, type: Integer
        requires :timezone, type: Integer
        optional :location, type: String
        optional :phone, type: String
        optional :phone_verified_at, type: String 
        optional :email, type: Hash
        optional :email_verified_at, type: String
        optional :connected_profiles, type: Hash
        optional :preferred_currency, type: String
        optional :is_hd_preferred, type: Boolean
        optional :is_video_autoplay, type: Boolean
      end
      post '/signup' do
        user = ::User._create! params
        present user, with: API::V1::Entities::User
      end

      mount API::V1::User::Posts
      mount API::V1::User::Series
      mount API::V1::User::Columns
      mount API::V1::User::Widgets
      # mount API::V1::User::Cards
      mount API::V1::User::Tags
      mount API::V1::User::ThunderOffers
      mount API::V1::User::Comments
      mount API::V1::User::Images
      mount API::V1::User::Videos
      mount API::V1::User::PartnerProfiles
      mount API::V1::User::Messages
      mount API::V1::User::Devices
      mount API::V1::User::CardQuestionResponses
    end
  end
end
