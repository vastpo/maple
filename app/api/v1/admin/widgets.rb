module API::V1::Admin
  class Widgets < Grape::API
    namespace '/widgets' do
      params do
        optional :where, type: Hash
        optional :as_json, type: Hash
        optional :pagination, type: Hash
      end
      post '/index' do
        authenticate! :admin
        status 200

        params[:order] = { id: :desc }

        widgets = ::Widget._index(params || {})
        widgets.as_json(params[:as_json])
      end

      params do
        requires :external_id, type: String
        optional :as_json, type: Hash
      end
      post '/show' do
        authenticate! :admin
        widget = ::Widget._show(params.slice(:external_id))

        raise ActiveRecord::RecordNotFound if !widget

        authorize widget, :show?
        status 200
        widget.as_json(params[:as_json])
      end

      params do
        requires :managed_by_id, type: Integer
        requires :widget_type, type: String
        requires :title, type: Hash do I18n.available_locales.each { |l| requires l, type: String } end
        requires :headline, type: Hash do I18n.available_locales.each { |l| requires l, type: String } end
        requires :conversion_text, type: Hash do I18n.available_locales.each { |l| requires l, type: String } end
        optional :directory_type, type: String
        optional :directory, type: String
        requires :color_scheme, type: Hash do
          requires :button_color, type: String
          requires :text_color, type: String
        end
        optional :is_publish, type: Boolean
      end
      post '/create' do
        authenticate! :admin
        authorize Widget.new, :create?
        widget = ::Widget._create!({ created_by: @current_member }.merge(params))
        present widget, with: API::V1::Entities::Widget
      end
    end
  end
end
