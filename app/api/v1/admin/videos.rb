module API::V1::Admin
  class Videos < Grape::API
    namespace '/videos' do
      params do
        optional :where, type: Hash
        optional :as_json, type: Hash
        optional :pagination, type: Hash
      end
      post '/index' do
        authenticate! :admin 
        status 200
       
        params[:order] = { id: :desc }
       
        videos = ::Video._index(params || {} )
        videos.as_json(params[:as_json])
      end

      params do
        requires :external_id, type: String
        optional :as_json, type: Hash
      end
      post '/show' do
        authenticate! :admin
        video = ::Video._show(params.slice(:external_id))

        raise ActiveRecord::RecordNotFound if !video

        authorize video, :show?
        status 200
        video.as_json(params[:as_json])
      end

      params do
        optional :managed_by_id, type: Integer
        requires :media_format, type: String
        requires :width, type: Integer
        requires :height, type: Integer
        requires :urls, type: Hash do
          requires :original_url, type: String
          requires :resized_url, type: String
        end
        optional :exclusivity, type: Hash
        optional :transferability, type: Hash
        optional :sub_licensibility, type: Hash
        optional :royalty_terms, type: Hash
        optional :territorialities, type: Hash
        optional :remarks, type: String
        optional :length, type: Float
      end
      post '/create' do
        authenticate! :admin
        authorize Video.new(managed_by_id: params[:managed_by_id]), :create?
        video = Video._create!(params)
        present video, with: API::V1::Entities::Video
      end
    end
  end
end
