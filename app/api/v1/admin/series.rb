module API::V1::Admin
  class Series < Grape::API
    namespace '/series' do
      params do
        optional :where, type: Hash
        optional :as_json, type: Hash
        optional :pagination, type: Hash
      end
      post '/index' do
        authenticate! :admin
        authorize @current_member, :index?
        status 200
        
        params[:order] = { id: :desc }
        
        series = ::Series._index(params || {})
        series.as_json(params[:as_json])
      end

      params do
        requires :external_id, type: String
        optional :as_json, type: Hash
      end
      post '/show' do
        authenticate! :admin
        series = ::Series._show(params.slice(:external_id))

        raise ActiveRecord::RecordNotFound if !series

        authorize series, :show?
        status 200
        series.as_json(params[:as_json])
      end

      params do
        optional :parent_id, type: Integer
        requires :title, type: Hash do I18n.available_locales.each { |l| requires l, type: String } end
        optional :short_note, type: Hash do I18n.available_locales.each { |l| requires l, type: String } end
        optional :long_note, type: Hash do I18n.available_locales.each { |l| requires l, type: String } end
        requires :color_scheme, type: Hash do
          requires :top_color, type: String
          requires :bottom_color, type: String
          requires :text_color, type: String
        end
        optional :post_external_ids, type: Array[String]
        optional :series_access, type: Hash do
          requires :partner_id, type: Integer
          requires :is_editing_allowed, type: Boolean
          requires :is_contribution_allowed, type: Boolean
          requires :is_active, type: Boolean
          requires :is_owner, type: Boolean
        end
      end
      post '/create' do
        authenticate! :admin
        series = ::Series._create!({ created_by: @current_member }.merge(params))
        present series, with: API::V1::Entities::Series
      end
    end
  end
end
