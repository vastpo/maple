module API::V1::Admin
  class Tags < Grape::API
    namespace '/tags' do
      params do
        optional :where, type: Hash
        optional :as_json, type: Hash
        optional :pagination, type: Hash
      end
      post '/index' do
        authenticate! :admin
        status 200
        tags = ::Tag._index(params || {})
        tags.as_json(params[:as_json])
      end

      params do
        requires :external_id, type: String
        optional :as_json, type: Hash
      end
      post '/show' do
        authenticate! :admin
        tag = ::Tag._show(params.slice(:external_id))

        raise ActiveRecord::RecordNotFound if !tag

        authorize tag, :show?
        status 200
        tag.as_json(params[:as_json])
      end

      params do
        requires :title, type: String
        requires :color_scheme, type: Hash do
          requires :top_color, type: String
          requires :bottom_color, type: String
          requires :text_color, type: String
        end
        requires :is_active, type: Boolean

      end
      post '/create' do
        authenticate! :admin
        authorize Tag.new, :create?
        tag = ::Tag._create!(params)
        present tag, with: API::V1::Entities::Tag
      end
    end
  end
end
