module API::V1::Admin
  class Comments < Grape::API
    namespace '/comments' do
      params do
        optional :where, type: Hash
        optional :as_json, type: Hash
        optional :pagination, type: Hash
      end
      post '/index' do
        authenticate! :admin 
        status 200
       
        params[:order] = { id: :desc }
       
        comments = ::Comment._index(params || {} )
        comments.as_json(params[:as_json])
      end

      params do
        requires :external_id, type: String
        optional :as_json, type: Hash
      end
      post '/show' do
        authenticate! :admin
        comment = ::Comment._show(params.slice(:external_id))

        raise ActiveRecord::RecordNotFound if !comment

        authorize comment, :show?
        status 200
        comment.as_json(params[:as_json])
      end

      params do
        requires :post_id, type: Integer
        optional :parent_id, type: Integer
        requires :content, type: String
      end
      post '/create' do
        authenticate! :admin
        authorize Comment.new(post_id: params[:post_id]), :create?
        comment = Comment._create!({ created_by: @current_member }.merge(params))
        present comment, with: API::V1::Entities::Comment
      end
    end
  end
end
