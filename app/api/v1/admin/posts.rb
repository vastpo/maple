module API::V1::Admin
  class Posts < Grape::API
    namespace '/posts' do

      params do
        optional :where, type: String
        optional :order, type: String
        optional :opt, type: String
        optional :pagination, type: String
      end
      get '/index' do
        authenticate! :admin
        parsed_params = parse_qs(params)

        posts = ::Post._index(parsed_params)
        return posts
      end

      params do
        optional :where, type: Hash
        optional :as_json, type: Hash
        optional :pagination, type: Hash
      end
      post '/index' do
        authenticate! :admin
        status 200

        params[:order] = { id: :desc }

        posts = ::Post._index(params || {})
        posts.as_json(params[:as_json])
      end

      params do
        requires :external_id, type: String
        optional :as_json, type: Hash
      end
      post '/show' do
        authenticate! :admin
        post = ::Post._show(params.slice(:external_id))

        raise ActiveRecord::RecordNotFound if !post

        authorize post, :show?
        status 200
        post.as_json(params[:as_json])
      end

      params do
        requires :managed_by_id, type: Integer
        requires :post_type, type: String
        requires :title, type: Hash do I18n.available_locales.each { |l| requires l, type: String } end
        optional :note, type: Hash do I18n.available_locales.each { |l| requires l, type: String } end
        requires :is_masked, type: Boolean
        requires :is_emoji_blocked, type: Boolean
        requires :is_comment_blocked, type: Boolean
        requires :is_capture_blocked, type: Boolean
        requires :permitted_emojis, type: Array[String]
        optional :series_external_ids, type: Array[String]
        optional :column_external_ids, type: Array[String]
        optional :tag_external_ids, type: Array[String]
        optional :card_external_ids, type: Array[String]
        optional :is_publish, type: Boolean
      end
      post '/create' do
        authenticate! :admin
        authorize Post.new(managed_by_id: params[:managed_by_id]), :create?
        begin
          post = Post._create!({ created_by: @current_member }.merge(params))
          present post, with: API::V1::Entities::Post
        rescue StandardError => e
          status 500
          return {
            error: e.message
          }
        end
      end

      params do
        requires :external_id, type: String
        optional :title, type: Hash do I18n.available_locales.each { |l| requires l, type: String } end
        optional :note, type: Hash do I18n.available_locales.each { |l| optional l, type: String } end
        optional :is_masked, type: Boolean
        optional :is_emoji_blocked, type: Boolean
        optional :is_comment_blocked, type: Boolean
        optional :is_capture_blocked, type: Boolean
        optional :permitted_emojis, type: Array[String]
        optional :series_external_ids, type: Array[String]
        optional :column_external_ids, type: Array[String]
        optional :tag_external_ids, type: Array[String]
        optional :card_external_ids, type: Array[String]
      end
      put '/modify/:external_id' do
        authenticate! :admin
        post = ::Post._show(params.slice(:external_id))

        raise ActiveRecord::RecordNotFound if !post

        authorize post, :update?

        begin
          post._update!(params)
          present post, with: API::V1::Entities::Post
        rescue StandardError => e
          status 500
          return {
            error: e.message
          }
        end
      end
    end
  end
end
