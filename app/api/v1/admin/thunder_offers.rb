module API::V1::Admin
  class ThunderOffers < Grape::API
    namespace '/thunder_offers' do
      params do
        optional :where, type: Hash
        optional :as_json, type: Hash
        optional :pagination, type: Hash
      end
      post '/index' do
        authenticate! :admin
        status 200
       
        params[:order] = { id: :desc }
        
        thunder_offers = ::ThunderOffer._index(params || {})
        thunder_offers.as_json(params[:as_json])
      end

      params do
        requires :external_id, type: String
        optional :as_json, type: Hash
      end
      post '/show' do
        authenticate! :admin
        thunder_offer = ::ThunderOffer._show(params.slice(:external_id))

        raise ActiveRecord::RecordNotFound if !thunder_offer

        authorize thunder_offer, :show?
        status 200
        thunder_offer.as_json(params[:as_json])
      end

      params do
        requires :partner_id, type: Integer
        optional :post_external_id, type: String
        requires :note, type: Hash do I18n.available_locales.each { |l| requires l, type: String } end
        requires :is_stripe_payout, type: Boolean
        optional :destination_account, type: String 
        requires :amount, type: Hash do
          requires :default_currency, type: String, values: Configs::Currency.pluck(:iso_code)
          requires :pricing_scheme, type: Hash do Configs::Currency.pluck(:iso_code).map(&:to_sym).each { |c| optional c, type: Float } end
        end
        requires :fee, type: Hash do Configs::Currency.pluck(:iso_code).map(&:to_sym).each { |c| optional c, type: Float } end
        requires :is_active, type: Boolean
      end
      post '/create' do
        authenticate! :admin
        authorize ThunderOffer.new(partner_id: params[:partner_id]), :create?

        active_payout_policy = ::Partner.find(params[:partner_id]).active_payout_policy

        if params[:is_stripe_payout].present? && !params[:destination_account].present?
          status 500
          return {
            error: "Stripe Account is invalid"
          }
        end

        if active_payout_policy.nil?
          status 500
          return {
            error: "Partner ##{params[:partner_id]} has no active payout policy"
          }
        end

        if params[:amount][:pricing_scheme].any?{ |c, v| v.present? && (params[:fee][c].nil? || v <= params[:fee][c]) }
          status 500
          return {
            error: "pricing_scheme does not match all currencies defined in fee and not satisfying (pricing_scheme[:currency] > fee[:currency])"
          }
        end

        thunder_offer = ThunderOffer._create!({ created_by: @current_member }.merge(params))
        present thunder_offer, with: API::V1::Entities::ThunderOffer
      end
    end
  end
end
