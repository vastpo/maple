module API::V1::Admin
  class Cards < Grape::API
    namespace '/cards' do

      params do
        optional :where, type: String
        optional :order, type: String
        optional :opt, type: String
        optional :pagination, type: String
      end
      get '/index' do
        authenticate! :admin
        parsed_params = parse_qs(params)

        cards = ::Card._index(parsed_params)
        return cards
      end

      params do
        optional :where, type: Hash
        optional :as_json, type: Hash
        optional :pagination, type: Hash
      end
      post '/index' do
        authenticate! :admin
        status 200

        params[:order] = { id: :desc }

        cards = ::Card._index(params || {})
        cards.as_json(params[:as_json])
      end

      params do
        requires :external_id, type: String
        optional :as_json, type: Hash
      end
      post '/show' do
        authenticate! :admin
        card = ::Card._show(params.slice(:external_id))

        raise ActiveRecord::RecordNotFound if !card

        authorize card, :show?
        status 200
        card.as_json(params[:as_json])
      end

      params do
        requires :managed_by_id, type: Integer
        optional :post_external_ids, type: Array[String]
        requires :accessible_locations, type: Hash do
          requires :locations, type: String
        end
        requires :tag_text, type: Hash do I18n.available_locales.each { |l| requires l, type: String } end
        optional :conversion_text, type: Hash do I18n.available_locales.each { |l| requires l, type: String } end
        optional :start_note, type: Hash do I18n.available_locales.each { |l| requires l, type: String } end
        optional :end_note, type: Hash do I18n.available_locales.each { |l| requires l, type: String } end
        requires :color_scheme, type: Hash do
          requires :tag_background, type: String
          requires :tag_font, type: String
          requires :card_background, type: String
          requires :card_font, type: String
          requires :button_background, type: String
          requires :button_font, type: String
        end
        requires :questions, type: Array do
          requires :question, type: String
          requires :response_type, type: String, values: CardQuestion::RESPONSE_TYPE.map(&:to_s)
          requires :is_required, type: Boolean
        end
        requires :is_requiring_login, type: Boolean
        requires :is_requiring_verification, type: Boolean
        requires :is_displaying_result, type: Boolean
        requires :is_publish, type: Boolean
      end
      post '/create' do
        authenticate! :admin
        authorize Card.new(managed_by_id: params[:managed_by_id]), :create?
        card = Card._create!({ created_by: @current_member }.merge(params))
        present card, with: API::V1::Entities::Card
      end

      params do
        requires :external_id, type: String
        optional :accessible_locations, type: Array[String]
        optional :tag_text, type: Hash do I18n.available_locales.each { |l| requires l, type: String } end
        optional :conversion_text, type: Hash do I18n.available_locales.each { |l| requires l, type: String } end
        optional :start_note, type: Hash do I18n.available_locales.each { |l| requires l, type: String } end
        optional :end_note, type: Hash do I18n.available_locales.each { |l| requires l, type: String } end
        optional :color_scheme, type: Hash do
          optional :tag_background, type: String
          optional :tag_font, type: String
          optional :card_background, type: String
          optional :card_font, type: String
          optional :button_background, type: String
          optional :button_font, type: String
        end
        requires :questions, type: Array do
          requires :question, type: String
          requires :response_type, type: String, values: CardQuestion::RESPONSE_TYPE.map(&:to_s)
          requires :is_required, type: Boolean
        end
        optional :is_requiring_login, type: Boolean
        optional :is_requiring_verification, type: Boolean
        optional :is_displaying_result, type: Boolean
      end
      put '/modify/:external_id' do
        authenticate! :admin
        card = ::Card._show(params.slice(:external_id))

        raise ActiveRecord::RecordNotFound if !card

        authorize card, :update?

        card._update!(params)
        present card, with: API::V1::Entities::Card
      end
    end
  end
end
