module API::V1::Admin
  class Images < Grape::API
    namespace '/images' do
      params do
        optional :where, type: Hash
        optional :as_json, type: Hash
        optional :pagination, type: Hash
      end
      post '/index' do
        authenticate! :admin 
        status 200
       
        params[:order] = { id: :desc }
       
        images = ::Image._index(params || {} )
        images.as_json(params[:as_json])
      end

      params do
        requires :external_id, type: String
        optional :as_json, type: Hash
      end
      post '/show' do
        authenticate! :admin
        image = ::Image._show(params.slice(:external_id))

        raise ActiveRecord::RecordNotFound if !image

        authorize image, :show?
        status 200
        image.as_json(params[:as_json])
      end

      params do
        optional :managed_by_id, type: Integer
        requires :media_format, type: String
        requires :width, type: Integer
        requires :height, type: Integer
        requires :urls, type: Hash do
          requires :original_url, type: String
          requires :resized_url, type: String
        end
        optional :exclusivity, type: Hash
        optional :transferability, type: Hash
        optional :sub_licensibility, type: Hash
        optional :royalty_terms, type: Hash
        optional :territorialities, type: Hash
        optional :remarks, type: String
        optional :is_grayscale, type: Boolean
        optional :is_line_drawing, type: Boolean
      end
      post '/create' do
        authenticate! :admin
        authorize Image.new(managed_by_id: params[:managed_by_id]), :create?
        image = Image._create!(params)
        present image, with: API::V1::Entities::Image
      end
    end
  end
end
