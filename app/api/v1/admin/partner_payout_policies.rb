module API::V1::Admin
  class PartnerPayoutPolicies < Grape::API
    namespace '/partner_payout_policies' do
      params do
        optional :where, type: Hash
        optional :as_json, type: Hash
        optional :pagination, type: Hash
      end
      post '/index' do
        authenticate! :admin
        status 200
        partner_payout_policies = ::PartnerPayoutPolicy._index(params || {})
        partner_payout_policies.as_json(params[:as_json])
      end

      params do
        requires :id, type: Integer 
        optional :as_json, type: Hash
      end
      post '/show' do
        authenticate! :admin
        partner_payout_policy = ::PartnerPayoutPolicy._show(params.slice(:external_id))

        raise ActiveRecord::RecordNotFound if !partner_payout_policy

        authorize partner_payout_policy, :show?
        status 200
        partner_payout_policy.as_json(params[:as_json])
      end

      params do
        requires :partner_id, type: Integer
        requires :fee_rate, type: Hash do Configs::Currency.pluck(:iso_code).map(&:to_sym).each { |c| optional c, type: Float } end
        requires :fee_fix, type: Hash do Configs::Currency.pluck(:iso_code).map(&:to_sym).each { |c| optional c, type: Float } end
        optional :rpe, type: Float
        requires :is_active, type: Boolean
        optional :remarks, type: String
      end
      post '/create' do
        authenticate! :admin
        authorize PartnerPayoutPolicy.new, :create?

        partner = ::Partner.find(params[:partner_id])
        partner_payout_policy = ::PartnerPayoutPolicy._create!({ currency: partner.preferred_currency }.merge(params))
        present partner_payout_policy, with: API::V1::Entities::PartnerPayoutPolicy
      end

      params do
        requires :id, type: Integer
        optional :fee_rate, type: Hash do Configs::Currency.pluck(:iso_code).map(&:to_sym).each { |c| optional c, type: Float } end
        optional :fee_fix, type: Hash do Configs::Currency.pluck(:iso_code).map(&:to_sym).each { |c| optional c, type: Float } end
        optional :rpe, type: Float
        optional :is_active, type: Boolean
        optional :remarks, type: String
      end
      put '/modify/:id' do
        authenticate! :admin

        partner_payout_policy = ::PartnerPayoutPolicy.find(params[:id])
        authorize partner_payout_policy, :update?

        if (params[:is_active] == false && partner_payout_policy.is_active == true)
          status 500
          return { error: 'Fuck You!' }
        end

        partner_payout_policy._update!(params)
        present partner_payout_policy, with: API::V1::Entities::PartnerPayoutPolicy
      end
    end
  end
end
