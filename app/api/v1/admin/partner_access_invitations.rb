module API::V1::Admin
  class PartnerAccessInvitations < Grape::API
    namespace '/partner_access_invitations' do
      params do
        optional :where, type: Hash
        optional :as_json, type: Hash
        optional :pagination, type: Hash
      end
      post '/index' do
        authenticate! :admin
        status 200
        partner_access_invitations = ::PartnerAccessInvitation._index(params || {})
        partner_access_invitations.as_json(params[:as_json])
      end

      params do
        requires :id, type: Integer 
        optional :as_json, type: Hash
      end
      post '/show' do
        authenticate! :admin
        partner_admin_invitation = ::PartnerAdminInvitation._show(params.slice(:partner_id))

        raise ActiveRecord::RecordNotFound if !partner_access_invitation

        authorize partner_admin_invitation, :show?
        status 200
        partner_access_invitation.as_json(params[:as_json])
      end

      params do
        requires :partner_id, type: Integer
        requires :email, type: String
        requires :roles, type: Array[String]
      end
      post '/create' do
        authenticate! ::Admin
        authorize PartnerAccessInvitation.new(invited_by: @current_member, partner_id: params[:partner_id]), :create?
        PartnerAccessInvitation._create!({ invited_by: @current_member }.merge(params))
      end
    end
  end
end
