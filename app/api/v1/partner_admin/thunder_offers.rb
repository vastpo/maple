module API::V1::PartnerAdmin
  class ThunderOffers < Grape::API
    namespace '/thunder_offers' do

      params do
        requires :partner_id, type: Integer
        optional :post_id, type: Integer
        optional :order, type: String, desc: 'Ordering, format: [key1:asc|desc[;key2:asc|desc[;...]]]'
        optional :opt, type: String, desc: 'Options, format: [key1:value1[;key2:value2[;...]]]'
        optional :pagination, type: String, desc: 'Pagination, format: [page:Integer[;per:Integer]]'
      end
      get '/index' do
        authenticate! :partner_admin

        authorize ::ThunderOffer.new(partner_id: params[:partner_id]), :index?

        parsed_params = parse_qs(params)
        parsed_params[:where] = { partner_id: params[:partner_id] }
        if params[:post_id]
          parsed_params[:where].merge!(post_id: params[:post_id].presence)
        end
        
        thunder_offers = ::ThunderOffer._index(parsed_params)
        return thunder_offers
      end

      params do
        requires :partner_id, type: Integer
        optional :where, type: Hash
        optional :as_json, type: Hash
        optional :pagination, type: Hash
      end
      post '/index' do
        authenticate! :partner_admin

        authorize ::ThunderOffer.new(partner_id: params[:partner_id]), :index?

        status 200

        params[:where] = (params[:where] || {}).merge(partner_id: params[:partner_id])
        params[:order] = { id: :desc }

        thunder_offers = ::ThunderOffer._index(params)
        thunder_offers.as_json(params[:as_json])
      end

      params do
        requires :external_id, type: String
        optional :opt, type: String, desc: 'Options, format: [key1:value1[;key2:value2[;...]]]'
      end
      get '/show/:external_id' do
        authenticate! :partner_admin
        thunder_offer = ::ThunderOffer._show(params.slice(:external_id))

        raise ActiveRecord::RecordNotFound if !thunder_offer

        authorize thunder_offer, :show?
        status 200
        return thunder_offer
      end

      params do
        requires :external_id, type: String
        optional :as_json, type: Hash
      end
      post '/show' do
        authenticate! :partner_admin
        thunder_offer = ::ThunderOffer._show(params.slice(:external_id))

        raise ActiveRecord::RecordNotFound if !thunder_offer

        authorize thunder_offer, :show?
        status 200
        thunder_offer.as_json(params[:as_json])
      end

      params do
        requires :partner_id, type: Integer
        optional :post_external_id, type: String
        requires :note, type: Hash do I18n.available_locales.each { |l| requires l, type: String } end
        requires :is_stripe_payout, type: Boolean
        optional :destination_account, type: String 
        requires :amount, type: Hash do
          requires :default_currency, type: String, values: Configs::Currency.pluck(:iso_code)
          requires :pricing_scheme, type: Hash do Configs::Currency.pluck(:iso_code).map(&:to_sym).each { |c| optional c, type: Float } end
        end
        # requires :fee, type: Hash do Configs::Currency.pluck(:iso_code).map(&:to_sym).each { |c| optional c, type: Float } end
        requires :is_active, type: Boolean
      end
      post '/create' do
        authenticate! :partner_admin
        authorize ThunderOffer.new(partner_id: params[:partner_id]), :create?

        if ::Post._show(external_id: params[:post_external_id]).managed_by != ::Partner.find(params[:partner_id])
          status 500
          return {
            error: "Mismatching Partner and Post"
          }
        end

        if params[:is_stripe_payout].present? && !params[:destination_account].present?
          status 500
          return {
            error: "Stripe Account is invalid"
          }
        end

        active_payout_policy = ::Partner.find(params[:partner_id]).active_payout_policy

        if active_payout_policy.nil?
          status 500
          return {
            error: "Partner ##{params[:partner_id]} has no active payout policy"
          }
        end

        if params[:amount][:pricing_scheme].any?{ |c, v| v.present? && (active_payout_policy.fee_rate[c].nil? || active_payout_policy.fee_fix[c].nil?) }
          status 500
          return {
            error: "Partner ##{params[:partner_id]} does not support all currencies defined in pricing_scheme"
          }
        end

        params[:fee] = params[:amount][:pricing_scheme].inject({}) do |fee, (c, v)|
          if v.present?
            fee[c] = v * active_payout_policy.fee_rate[c] + active_payout_policy.fee_fix[c]
          else
            fee[c] = nil
          end
          fee
        end

        thunder_offer = ThunderOffer._create!({ created_by: @current_member }.merge(params))
        present thunder_offer, with: API::V1::Entities::ThunderOffer
      end

      params do
        requires :external_id, type: String
        optional :note, type: Hash do I18n.available_locales.each { |l| requires l, type: String } end
        optional :is_active, type: Boolean
      end
      put '/modify/:external_id' do
        authenticate! :partner_admin
        thunder_offer = ::ThunderOffer._show(params.slice(:external_id))

        raise ActiveRecord::RecordNotFound if !thunder_offer

        authorize thunder_offer, :update?

        thunder_offer._update!(params)
        present thunder_offer, with: API::V1::Entities::ThunderOffer
      end
    end
  end
end
