module API::V1::PartnerAdmin
  class Columns < Grape::API
    namespace '/columns' do
      params do
        optional :where, type: Hash
        optional :as_json, type: Hash
        optional :pagination, type: Hash
      end
      post '/index' do
        authenticate! :partner_admin
        status 200
        columns = ::Column._index(params || {})
        columns.as_json(params[:as_json])
      end

      params do
        requires :external_id, type: String
        optional :as_json, type: Hash
      end
      post '/show' do
        authenticate! :partner_admin
        column = ::Column._show(params.slice(:external_id))

        raise ActiveRecord::RecordNotFound if !column

        authorize column, :show?
        status 200
        column.as_json(params[:as_json])
      end

      # params do
      #   requires :title, type: Hash do I18n.available_locales.each { |l| requires l, type: String } end
      #   optional :short_note, type: Hash do I18n.available_locales.each { |l| requires l, type: String } end
      #   optional :long_note, type: Hash do I18n.available_locales.each { |l| requires l, type: String } end
      #   requires :color_scheme, type: Hash do
      #     requires :top_color, type: String
      #     requires :bottom_color, type: String
      #     requires :text_color, type: String
      #   end
      # end
      # post '/create' do
      #   authenticate! :partner_admin
      #   authorize Column.new, :create?
      #   column = ::Column._create!({ created_by: @current_member }.merge(params))
      #   present column, with: API::V1::Entities::Column
      # end
    end
  end
end
