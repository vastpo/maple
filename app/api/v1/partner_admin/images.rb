module API::V1::PartnerAdmin
  class Images < Grape::API
    namespace '/images' do
     
      params do
        requires :managed_by_id, type: Integer
        optional :order, type: String, desc: 'Ordering, format: [key1:asc|desc[;key2:asc|desc[;...]]]'
        optional :opt, type: String, desc: 'Options, format: [key1:value1[;key2:value2[;...]]]'
        optional :pagination, type: String, desc: 'Pagination, format: [page:Integer[;per:Integer]]'
      end
      get '/index' do
        authenticate! :partner_admin

        authorize ::Image.new(managed_by_id: params[:managed_by_id]), :index?

        parsed_params = parse_qs(params).merge(where: { managed_by_id: params[:managed_by_id] })

        images = ::Image._index(parsed_params)
        return images
      end

      params do
        requires :managed_by_id, type: Integer
        optional :where, type: Hash
        optional :as_json, type: Hash
        optional :pagination, type: Hash
      end
      post '/index' do
        authenticate! :partner_admin 
        
        authorize ::Image.new(managed_by_id: params[:managed_by_id]), :index?
        
        status 200

        params[:where] = (params[:where] || {}).merge(managed_by_id: params[:managed_by_id])
        params[:order] = { id: :desc }

        images = ::Image._index(params || {} )
        images.as_json(params[:as_json])
      end

      params do
        requires :external_id, type: String
        optional :opt, type: String, desc: 'Options, format: [key1:value1[;key2:value2[;...]]]'
      end
      get '/show/:external_id' do
        authenticate! :partner_admin
        image = ::Image._show(params.slice(:external_id))

        raise ActiveRecord::RecordNotFound if !image

        authorize image, :show?
        status 200
        return image
      end

      params do
        requires :external_id, type: String
        optional :as_json, type: Hash
      end
      post '/show' do
        authenticate! :partner_admin
        image = ::Image._show(params.slice(:external_id))

        raise ActiveRecord::RecordNotFound if !image

        authorize image, :show?
        status 200
        image.as_json(params[:as_json])
      end

      params do
        requires :managed_by_id, type: Integer
        requires :media_format, type: String
        requires :width, type: Integer
        requires :height, type: Integer
        requires :urls, type: Hash do
          requires :original_url, type: String
          requires :resized_url, type: String
        end
        optional :exclusivity, type: Hash
        optional :transferability, type: Hash
        optional :sub_licensibility, type: Hash
        optional :royalty_terms, type: Hash
        optional :territorialities, type: Hash
        optional :remarks, type: String
        optional :is_grayscale, type: Boolean
        optional :is_line_drawing, type: Boolean
      end
      post '/create' do
        authenticate! :partner_admin
        authorize Image.new(managed_by_id: params[:managed_by_id]), :create?
        image = Image._create!(params)
        present image, with: API::V1::Entities::Image
      end
    end
  end
end
