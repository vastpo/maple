module API::V1::PartnerAdmin
  class Emojis < Grape::API
    namespace '/emojis' do

      get '/index' do
        items_per_page = 4000
        params[:where] = { is_active: true }
        params[:pagination] = (params[:pagination] || {}).merge(per: items_per_page)
        Emoji._index(params)
      end

    end
  end
end
