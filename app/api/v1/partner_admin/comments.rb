module API::V1::PartnerAdmin
  class Comments < Grape::API
    namespace '/comments' do
      params do
        optional :where, type: Hash
        optional :as_json, type: Hash
        optional :pagination, type: Hash
      end
      post '/index' do
        authenticate! :partner_admin 
        status 200
        comments = ::Comment._index(params || {} )
        comments.as_json(params[:as_json])
      end

      params do
        requires :external_id, type: String
        optional :as_json, type: Hash
      end
      post '/show' do
        authenticate! :partner_admin
        comment = ::Comment._show(params.slice(:external_id))

        raise ActiveRecord::RecordNotFound if !comment

        authorize comment, :show?
        status 200
        comment.as_json(params[:as_json])
      end

      params do
        requires :post_id, type: Integer
        optional :parent_id, type: Integer
        requires :content, type: String
      end
      post '/create' do
        authenticate! :partner_admin
        authorize Comment.new, :create?
        comment = Comment._create!({ created_by: @current_member }.merge(params))
        present comment, with: API::V1::Entities::Comment
      end

      params do
        requires :external_id, type: String
        optional:content, type: String
      end
      put '/modify/:external_id' do
        authenticate! :partner_admin
        comment = ::Comment._show(params.slice(:external_id))

        raise ActiveRecord::RecordNotFound if !comment

        authorize comment, :update?

        comment._update!(params)
        present comment, with: API::V1::Entities::Comment
      end
    end
  end
end
