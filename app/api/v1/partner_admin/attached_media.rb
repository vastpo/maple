module API::V1::PartnerAdmin
  class AttachedMedia < Grape::API
    namespace '/attached_media' do
      
      params do
        requires :attachable_type, type: String
        requires :attachable_id, type: Integer
        optional :order, type: String, desc: 'Ordering, format: [key1:asc|desc[;key2:asc|desc[;...]]]'
        optional :opt, type: String, desc: 'Options, format: [key1:value1[;key2:value2[;...]]]'
        optional :pagination, type: String, desc: 'Pagination, format: [page:Integer[;per:Integer]]'
      end
      get '/index' do
        authenticate! :partner_admin

        parsed_params = parse_qs(params)

        attached_media = ::AttachedMedia._index(parsed_params)
        return attached_media
      end

      params do
        requires :media_id, type: Integer
        requires :attachable_type, type: String
        requires :attachable_id, type: Integer
        requires :attached_for, type: String
      end
      post '/create' do
        authenticate! :partner_admin
        attached_media = ::AttachedMedia._create!(params)
        present attached_media, with: API::V1::Entities::AttachedMedia
      end
    end
  end
end
