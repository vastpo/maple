module API::V1::PartnerAdmin
  class PartnerAccess < Grape::API
    namespace '/partner_access' do

      params do
        requires :id, type: Integer
        optional :sort_order, type: Integer
        optional :is_default, type: Boolean
      end
      put '/modify/:id' do
        authenticate! :partner_admin

        partner_access = ::PartnerAccess.find(params[:id])
        raise ActiveRecord::RecordNotFound if !partner_access

        authorize partner_access, :update?

        partner_access._update!(params)
      end

    end
  end
end
