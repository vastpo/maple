module API::V1::PartnerAdmin
  class Series < Grape::API
    namespace '/series' do

      params do
        requires :partner_id, type: Integer
        optional :order, type: String, desc: 'Ordering, format: [key1:asc|desc[;key2:asc|desc[;...]]]'
        optional :opt, type: String, desc: 'Options, format: [key1:value1[;key2:value2[;...]]]'
        optional :pagination, type: String, desc: 'Pagination, format: [page:Integer[;per:Integer]]'
      end
      get '/index' do
        authenticate! :partner_admin

        authorize SeriesAccess.new(partner_id: params[:partner_id]), :index?

        parsed_params = parse_qs(params).merge(where: { id: ::Partner.find(params[:partner_id]).series_access.pluck(:series_id) })

        series = ::Series._index(parsed_params)
        return series
      end

      # params do
      #   requires :partner_id, type: Integer
      #   optional :where, type: Hash
      #   optional :as_json, type: Hash
      #   optional :pagination, type: Hash
      # end
      # post '/index' do
      #   authenticate! :partner_admin
      #
      #   # authorize ::Series.new(partner_id: params[:partner_id]), :index?
      #   authorize SeriesAccess.new(partner_id: params[:partner_id]), :index?
      #
      #   status 200
      #
      #   params[:where] = (params[:where] || {}).merge(id: ::Partner.find(params[:partner_id]).series_access.pluck(:series_id))
      #   params[:order] = { id: :desc }
      #   
      #   series = ::Series._index(params)
      #   series.as_json(params[:as_json])
      # end

      params do
        requires :external_id, type: String
        optional :opt, type: String, desc: 'Options, format: [key1:value1[;key2:value2[;...]]]'
      end
      get '/show/:external_id' do
        authenticate! :partner_admin
        series = ::Series._show(params.slice(:external_id))

        raise ActiveRecord::RecordNotFound if !series

        authorize series, :show?
        status 200
        return series.as_json(include: [:series_access, :attached_media, :posts])
      end

      # params do
      #   requires :external_id, type: String
      #   optional :as_json, type: Hash
      # end
      # post '/show' do
      #   authenticate! :partner_admin
      #   series = ::Series._show(params.slice(:external_id))
      #
      #   raise ActiveRecord::RecordNotFound if !series
      #
      #   # authorize series, :show?
      #   # authorize SeriesAccess.new(partner_id: params[:partner_id]), :show?
      #   status 200
      #   series.as_json(params[:as_json])
      # end

      params do
        requires :partner_id, type: Integer
        optional :parent_id, type: Integer
        requires :title, type: Hash do I18n.available_locales.each { |l| requires l, type: String } end
        optional :short_note, type: Hash do I18n.available_locales.each { |l| requires l, type: String } end
        optional :long_note, type: Hash do I18n.available_locales.each { |l| requires l, type: String } end
        requires :color_scheme, type: Hash do
          requires :top_color, type: String
          requires :bottom_color, type: String
          requires :text_color, type: String
        end
        optional :post_external_ids, type: Array[String]
      end
      post '/create' do
        authenticate! :partner_admin
        authorize SeriesAccess.new(partner_id: params[:partner_id]), :create?

        params[:series_access] = {
          partner_id: params[:partner_id],
          # series_id: _self.id,
          is_owner: true,
          is_editing_allowed: true,
          is_contribution_allowed: true,
          is_active: true
        }
        series = ::Series._create!({ created_by: @current_member }.merge(params))
        present series, with: API::V1::Entities::Series
      end

      params do
        requires :external_id, type: String
        optional :parent_id, type: Integer
        optional :title, type: Hash do I18n.available_locales.each { |l| requires l, type: String } end
        optional :short_note, type: Hash do I18n.available_locales.each { |l| requires l, type: String } end
        optional :long_note, type: Hash do I18n.available_locales.each { |l| requires l, type: String } end
        optional :color_scheme, type: Hash do
          requires :top_color, type: String
          requires :bottom_color, type: String
          requires :text_color, type: String
        end
        optional :post_external_ids, type: Array[String]
      end
      put '/modify/:external_id' do
        authenticate! :partner_admin
        series = ::Series._show(params.slice(:external_id))

        raise ActiveRecord::RecordNotFound if !series

        authorize series, :update?

        series._update!(params)
        present series, with: API::V1::Entities::Series
      end

    end
  end
end
