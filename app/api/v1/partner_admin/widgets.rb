module API::V1::PartnerAdmin
  class Widgets < Grape::API
    namespace '/widgets' do

      params do
        requires :managed_by_id, type: Integer
        optional :order, type: String, desc: 'Ordering, format: [key1:asc|desc[;key2:asc|desc[;...]]]'
        optional :opt, type: String, desc: 'Options, format: [key1:value1[;key2:value2[;...]]]'
        optional :pagination, type: String, desc: 'Pagination, format: [page:Integer[;per:Integer]]'
      end
      get '/index' do
        authenticate! :partner_admin

        authorize ::Widget.new(managed_by_id: params[:managed_by_id]), :index?

        parsed_params = parse_qs(params).merge(where: { managed_by_id: params[:managed_by_id] })

        widgets = ::Widget._index(parsed_params)
        return widgets
      end

      # params do
      #   requires :managed_by_id, type: Integer
      #   optional :where, type: Hash
      #   optional :as_json, type: Hash
      #   optional :pagination, type: Hash
      # end
      # post '/index' do
      #   authenticate! :partner_admin
      #
      #   authorize ::Widget.new(managed_by_id: params[:managed_by_id]), :index?
      #
      #   status 200
      #
      #   params[:where] = (params[:where] || {}).merge(managed_by_id: params[:managed_by_id])
      #   params[:order] = { id: :desc }
      #
      #   widgets = ::Widget._index(params)
      #   widgets.as_json(params[:as_json])
      # end

      params do
        requires :external_id, type: String
        optional :opt, type: String, desc: 'Options, format: [key1:value1[;key2:value2[;...]]]'
      end
      get '/show/:external_id' do
        authenticate! :partner_admin
        widget = ::Widget._show(params.slice(:external_id))

        raise ActiveRecord::RecordNotFound if !widget

        authorize widget, :show?
        status 200
        return widget
      end

      # params do
      #   requires :external_id, type: String
      #   optional :as_json, type: Hash
      # end
      # post '/show' do
      #   authenticate! :partner_admin
      #   widget = ::Widget._show(params.slice(:external_id))
      #
      #   raise ActiveRecord::RecordNotFound if !widget
      #
      #   authorize widget, :show?
      #   status 200
      #   widget.as_json(params[:as_json])
      # end

      params do
        requires :managed_by_id, type: Integer
        requires :widget_type, type: String
        requires :title, type: Hash do I18n.available_locales.each { |l| requires l, type: String } end
        requires :headline, type: Hash do I18n.available_locales.each { |l| requires l, type: String } end
        requires :conversion_text, type: Hash do I18n.available_locales.each { |l| requires l, type: String } end
        optional :directory_type, type: String
        optional :directory, type: String 
        requires :color_scheme, type: Hash do
          requires :button_color, type: String
          requires :text_color, type: String
        end
        optional :is_publish, type: Boolean
      end
      post '/create' do
        authenticate! :partner_admin
        authorize Widget.new(managed_by_id: params[:managed_by_id]), :create?
        widget = Widget._create!({ created_by: @current_member }.merge(params))
        present widget, with: API::V1::Entities::Widget
      end

      params do
        requires :external_id, type: String
        optional :title, type: Hash do I18n.available_locales.each { |l| optional l, type: String } end
        optional :headline, type: Hash do I18n.available_locales.each { |l| optional l, type: String } end
        optional :conversion_text, type: Hash do I18n.available_locales.each { |l| optional l, type: String } end
        optional :directory_type, type: String
        optional :directory, type: String
        optional :color_scheme, type: Hash do
          requires :button_color, type: String
          requires :text_color, type: String
        end
      end
      put '/modify/:external_id' do
        authenticate! :partner_admin
        widget = ::Widget._show(params.slice(:external_id))

        raise ActiveRecord::RecordNotFound if !widget

        authorize widget, :update?

        widget._update!(params)
        present widget, with: API::V1::Entities::Widget
      end
    end
  end
end
