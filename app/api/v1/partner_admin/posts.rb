module API::V1::PartnerAdmin
  class Posts < Grape::API
    namespace '/posts' do
      
      params do
        requires :managed_by_id, type: Integer
        optional :state, type: String
        optional :order, type: String, desc: 'Ordering, format: [key1:asc|desc[;key2:asc|desc[;...]]]'
        optional :opt, type: String, desc: 'Options, format: [key1:value1[;key2:value2[;...]]]'
        optional :pagination, type: String, desc: 'Pagination, format: [page:Integer[;per:Integer]]'
      end
      get '/index' do
        authenticate! :partner_admin

        authorize ::Post.new(managed_by_id: params[:managed_by_id]), :index?

        parsed_params = parse_qs(params).merge({
          where: { managed_by_id: params[:managed_by_id] }.merge((params[:state].present? && params[:state]!='all') ? { state: params[:state].split(';') } : {}),
          as_json: {
            include: {
              created_by: {
                only: [ :display_name ]
              },
              series: {
                chain: [ :root ],
                only: [ :external_id, :title ]
              }
            },
            only: [ :id, :external_id, :post_type, :title, :state, :created_at ]
          }
        })

        posts = ::Post._index(parsed_params)
        comments = Comment.where(post_id: posts[:result].pluck(:id)).group(:post_id).count
        posts[:result] = posts[:result].map do |post|
          post.merge!(comments_count: comments[post[:id]])
        end
        return posts
      end

      params do
        requires :managed_by_id, type: Integer
        optional :where, type: Hash
        optional :as_json, type: Hash
        optional :pagination, type: Hash
      end
      post '/index' do
        authenticate! :partner_admin

        authorize ::Post.new(managed_by_id: params[:managed_by_id]), :index?

        status 200

        params[:where] = (params[:where] || {}).merge(managed_by_id: params[:managed_by_id])
        params[:order] = { id: :desc }

        ::Post._index(params)
      end

      params do
        requires :external_id, type: String
        optional :opt, type: String, desc: 'Options, format: [key1:value1[;key2:value2[;...]]]'
      end
      get '/show/:external_id' do
        authenticate! :partner_admin
        post = ::Post._show(params.slice(:external_id))

        raise ActiveRecord::RecordNotFound if !post

        authorize post, :show?
        status 200
        return post.as_json(include: [:attached_media, :series, :columns])
      end

      params do
        requires :external_id, type: String
        optional :as_json, type: Hash
      end
      post '/show' do
        authenticate! :partner_admin
        post = ::Post._show(params.slice(:external_id))

        raise ActiveRecord::RecordNotFound if !post

        authorize post, :show?
        status 200
        post.as_json(params[:as_json])
      end

      params do
        requires :managed_by_id, type: Integer
        requires :post_type, type: String
        requires :title, type: Hash do I18n.available_locales.each { |l| requires l, type: String } end
        optional :note, type: Hash do I18n.available_locales.each { |l| requires l, type: String } end
        requires :is_masked, type: Boolean
        requires :is_emoji_blocked, type: Boolean
        requires :is_comment_blocked, type: Boolean
        requires :is_capture_blocked, type: Boolean
        requires :permitted_emojis, type: Array[String]
        optional :attached_media_details, type: Array do
          requires :external_id, type: String
          requires :attached_for, type: String
        end
        optional :series_external_ids, type: Array[String]
        optional :card_external_ids, type: Array[String]
        optional :is_publish, type: Boolean
      end
      post '/create' do
        authenticate! :partner_admin
        authorize Post.new(managed_by_id: params[:managed_by_id]), :create?
        begin
          post = Post._create!({ created_by: @current_member }.merge(params))
          present post, with: API::V1::Entities::Post
        rescue StandardError => e
          status 500
          return {
            error: e.message
          }
        end
      end

      params do
        requires :external_id, type: String
        optional :title, type: Hash do I18n.available_locales.each { |l| requires l, type: String } end
        optional :note, type: Hash do I18n.available_locales.each { |l| optional l, type: String } end
        optional :is_masked, type: Boolean
        optional :is_emoji_blocked, type: Boolean
        optional :is_comment_blocked, type: Boolean
        optional :is_capture_blocked, type: Boolean
        optional :permitted_emojis, type: Array[String]
        optional :attached_media_details, type: Array do
          requires :external_id, type: String
          requires :attached_for, type: String
        end
        optional :series_external_ids, type: Array[String]
        optional :card_external_ids, type: Array[String]
      end
      put '/modify/:external_id' do
        authenticate! :partner_admin
        post = ::Post._show(params.slice(:external_id))

        raise ActiveRecord::RecordNotFound if !post

        authorize post, :update?

        begin
          post._update!(params)
          present post, with: API::V1::Entities::Post
        rescue StandardError => e
          status 500
          return {
            error: e.message
          }
        end
      end
    end
  end
end
