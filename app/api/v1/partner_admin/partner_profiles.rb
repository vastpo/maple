module API::V1::PartnerAdmin
  class PartnerProfiles < Grape::API
    namespace '/partner_profiles' do

      params do
        requires :external_id, type: String
        optional :opt, type: String, desc: 'Options, format: [key1:value1[;key2:value2[;...]]]'
      end
      get '/show/:external_id' do
        authenticate! :partner_admin
        partner_profile = ::PartnerProfile._show(params.slice(:external_id))

        raise ActiveRecord::RecordNotFound if !partner_profile

        authorize partner_profile, :show?
        status 200
        return partner_profile.as_json
      end

      params do
        requires :external_id, type: String
        optional :as_json, type: Hash
      end
      post '/show' do
        authenticate! :partner_admin
        partner_profile = ::PartnerProfile._show(params.slice(:external_id))

        raise ActiveRecord::RecordNotFound if !partner_profile

        authorize partner_profile, :show?
        status 200
        partner_profile.as_json(params[:as_json])
      end

    end
  end
end
