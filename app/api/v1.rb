module API::V1
  class Base < Grape::API
    version 'v1', using: :header, vendor: 'acme', cascade: true

    helpers Pundit

    helpers do
      def authenticate!(*_classes)
        begin
          @current_member = AuthorizeAPIRequest.call([:admin] + _classes, request.path, request.headers, request.params).result
          RequestStore.store[:current_member] = @current_member
          RequestStore.store[:request] = request
          return @current_member
        rescue Exception => e
          error!(e.message, 500)
        end
      end

      def signin(_class, login, password)
        command = Authenticator.call(_class, login, password)

        if command.success?
          command.result
        else
          error!({ status: :error, messages: command.errors }, 500)
        end
      end

      def pundit_user
        @current_member
      end
    end

    before_validation do
      status 200
      @params = (declared @params).compact.deep_symbolize_keys
    end

    if !Rails.env.production?
      params do
        requires :type, type: String
      end
      post '/test' do
        authenticate! params[:type]
        { version: 'v1', type: params[:type], result: :OK }
      end
    end

    rescue_from Pundit::NotAuthorizedError do |e|
      error_message = API::PunditExceptionHandler.new(RequestStore.store[:current_member], e).perform!
      error!(error_message)
    end

    mount API::V1::Admin::Base
    mount API::V1::Partner::Base
    mount API::V1::PartnerAdmin::Base
    mount API::V1::User::Base
    mount API::V1::General::Base

    add_swagger_documentation mount_path: '/v1/swagger', base_path: '/api', info: {
      title: 'VastPo API',
      # description: "A description of the API.",
      # contact_name: "Contact name",
      # contact_email: "Contact@email.com",
      # contact_url: "Contact URL",
      # license: "The name of the license.",
      # license_url: "www.The-URL-of-the-license.org",
      # terms_of_service_url: "www.The-URL-of-the-terms-and-service.com"
    }
  end
end
