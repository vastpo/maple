module API::V2
  class Base < Grape::API
    version 'v2', using: :header, vendor: 'acme', cascade: true

    helpers Pundit

    helpers do
      def authenticate!(*_classes)
        begin
          @current_member = AuthorizeAPIRequest.call([:admin] + _classes, request.headers).result
        rescue Exception => e
          error!(e.message, 403)
        end
      end

      def signin(_class, login, password)
        command = Authenticator.call(_class, login, password)

        if command.success?
          command.result
        else
          error!({ errors: command.errors }, 403)
        end
      end

      def pundit_user
        @current_member
      end
    end

    before_validation do
      @params = (declared @params).compact.deep_symbolize_keys
    end

    if !Rails.env.production?
      params do
        requires :type, type: String
      end
      post '/test' do
        authenticate! params[:type]
        { version: 'v2', type: params[:type], result: :OK }
      end
    end

    rescue_from Pundit::NotAuthorizedError do |e|
      error_message = API::PunditExceptionHandler.new(@current_member, e).perform!
      error!(error_message)
    end

    mount API::V2::General::Base
  end
end
