module API
  class Base < Grape::API
    format :json

    helpers do
      def parse_qs(params)
        equal_sign = ':'
        separator = ';'

        attrs = [:order, :opt, :pagination]

        attrs.each do |attr|
          next if params[attr].nil?
          params[attr] = params[attr].split(separator).inject({}) do |result, kvp|
            key, value = kvp.split(equal_sign)
            result[key] = value
            result
          end
        end

        return params.deep_symbolize_keys
      end
    end

    rescue_from Grape::Exceptions::ValidationErrors do |e|
      error!({ status: :api_validation_failure, message: e.message })
    end

    mount API::V2::Base => '/api'
    mount API::V1::Base => '/api'
  end
end
