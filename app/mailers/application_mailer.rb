class ApplicationMailer < ActionMailer::Base
  include Resque::Mailer

  default from: 'admin@mail.jlsolutant.com'
  layout 'mailer'

  def demo(email_address)
    mail(to: email_address, subject: 'Sample Fuck You Resque!')
  end
end
