include FactoryHelpers

FactoryBot.define do
  factory :partner do
    login { 'syspartner' }
    password { 'P@ssw0rd' }
    external_id { 'XYZ_PARTNER' }
    display_name { locale('System Partner') }
    preferred_language { :en }
    preferred_currency { :hkd }

    trait :owner do
      login { 'root' }
      display_name { locale('Partner Owner') }
    end

    after(:build) do |partner|
      create(:partner_profile, partner: partner)
      create(:api_key, owner: partner)
    end
  end
end
