include FactoryHelpers

FactoryBot.define do
  factory :partner_profile do
    association :partner
    short_note { locale('short_note') }
    long_note { locale('locale') }
    media_details {{}}
    interaction_details {{}}
    score_details {{}}
    links {[]}
    base_url { '' }
  end
end
