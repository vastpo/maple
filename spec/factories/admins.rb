FactoryBot.define do
  factory :admin do
    login { 'sysadmin' }
    password { 'P@ssw0rd' }
    display_name { 'System Admin' }
    roles { [:super_admin] }

    trait :super_admin do
      login { 'root' }
      display_name { 'Super Admin' }
      roles { [:super_admin] }
    end

    trait :account_manager do
      login { 'account_manager' }
      display_name { 'Account Manager' }
      roles { [:account_manager] }
    end

    trait :account_executive do
      login { 'account_executive' }
      display_name { 'Account Executive' }
      roles { [:account_executive] }
    end

    trait :viewer do
      login { 'viewer' }
      display_name { 'Viewer' }
      roles { [:viewer] }
    end

    after(:build) do |admin|
      create(:admin_profile, admin: admin)
      create(:api_key, owner: admin)
    end
  end
end
