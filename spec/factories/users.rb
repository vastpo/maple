FactoryBot.define do
  factory :user do
    login { 'a_user' }
    password { 'P@ssw0rd' }
    display_name { 'A user' }
    gender { 'X' }
    date_of_birth { 20.years.ago.to_date }
    timezone { 8}
    location { :hong_kong }
    phone { '+852 66666666' }
    is_hd_preferred { true }
    is_video_autoplay { true }

    trait :predefined do
      login { 'predefined_user' }
      display_name { 'Predefined User' }
    end

    after(:build) do |user|
      create(:api_key, owner: user)
    end
  end
end
