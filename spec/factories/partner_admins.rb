include FactoryHelpers

FactoryBot.define do
  factory :partner_admin do
    login { 'syspartneradmin' }
    password { 'P@ssw0rd' }
    timezone { 8 }

    trait :owner do
      login { 'root' }
    end

    after(:build) do |partner_admin|
      create(:api_key, owner: partner_admin)
    end
  end
end
