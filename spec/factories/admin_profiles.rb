FactoryBot.define do
  factory :admin_profile do
    association :admin
    first_name { '' }
    last_name { '' }
    nickname { '' }
    gender { 'X' }
    phones { ['61234567', '98765432'] }
    emails { ['admin1@vastpo.com', 'admin2@vastpo.com'] }
    role_details {{}}
  end
end
