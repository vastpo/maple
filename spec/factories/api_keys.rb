FactoryBot.define do
  factory :api_key do
    association :owner

    after(:build) do |key|
      key.access_key = Digest::SHA256.hexdigest(JSON.generate({ id: key.owner.id, time: Time.now.to_f, type: :access_key }))
      key.secret_key = Digest::SHA256.hexdigest(JSON.generate({ id: key.owner.id, time: Time.now.to_f, type: :secret_key }))
    end
  end
end
