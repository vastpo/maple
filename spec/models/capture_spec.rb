require 'rails_helper'

RSpec.describe Capture, type: :model do
  context 'Associations' do
    it { should belong_to(:message) }
    it { should belong_to(:video) }
  end
end
