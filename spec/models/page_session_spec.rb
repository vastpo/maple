require 'rails_helper'

RSpec.describe PageSession, type: :model do
  context 'Associations' do
    it { should belong_to(:device) }
    it { should belong_to(:user) }

    it { should have_many(:delivered_components).dependent(:destroy) }
    it { should have_many(:engagements).dependent(:destroy) }
  end
end
