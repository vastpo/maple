require 'rails_helper'
require 'active_record/acts_as/matchers'

RSpec.describe Video, type: :model do
  context 'Table inheritance' do
    it { expect(Image).to act_as(Media) }
  end

  context 'Associations' do
    it { should have_many(:captures).dependent(:destroy) }
  end
end
