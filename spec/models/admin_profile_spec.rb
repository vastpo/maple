require 'rails_helper'

RSpec.describe AdminProfile, type: :model do
  context 'Associations' do
    it { should belong_to(:admin) }
  end
end
