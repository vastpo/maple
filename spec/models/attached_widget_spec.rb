require 'rails_helper'

RSpec.describe AttachedWidget, type: :model do
  context 'Associations' do
    it { should belong_to(:post) }
    it { should belong_to(:widget) }
  end
end
