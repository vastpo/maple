require 'rails_helper'

RSpec.describe PartnerAccess, type: :model do
  context 'Associations' do
    it { should belong_to(:invitation).class_name('PartnerAccessInvitation') }
    it { should belong_to(:partner_admin) }
    it { should belong_to(:partner) }
  end
end
