require 'rails_helper'

RSpec.describe DeliveryLocation, type: :model do
  context 'Associations' do
    it { should belong_to(:deliverable) }
  end
end
