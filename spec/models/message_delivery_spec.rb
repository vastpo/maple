require 'rails_helper'

RSpec.describe MessageDelivery, type: :model do
  context 'Associations' do
    it { should belong_to(:message) }
    it { should belong_to(:recipient).class_name('User') }
  end
end
