require 'rails_helper'
require 'active_record/acts_as/matchers'

RSpec.describe Image, type: :model do
  context 'Table inheritance' do
    it { expect(Image).to act_as(Media) }
  end
end
