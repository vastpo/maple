require 'rails_helper'

RSpec.describe Media, type: :model do
  context 'Associations' do
    it { should have_many(:attached_media).dependent(:destroy) }
    it { should have_many(:engagements).dependent(:destroy) }
  end

  context 'Table inheritance' do
    it { expect(Media).to be_actable }
  end
end
