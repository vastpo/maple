require 'rails_helper'

RSpec.describe Post, type: :model do
  context 'Associations' do
    it { should belong_to(:created_by) }
    it { should belong_to(:managed_by).class_name('Partner') }

    it { should have_many(:attached_emojis).dependent(:destroy) }
    it { should have_many(:attached_media).dependent(:destroy) }
    it { should have_many(:attached_widgets).dependent(:destroy) }
    it { should have_many(:delivery_locations).dependent(:destroy) }
    it { should have_many(:engagements).dependent(:destroy) }
    it { should have_many(:columns_posts).class_name('ColumnsPosts').dependent(:destroy) }
    it { should have_many(:posts_series).class_name('PostsSeries').dependent(:destroy) }
    it { should have_many(:posts_tags).class_name('PostsTags').dependent(:destroy) }
  end
end
