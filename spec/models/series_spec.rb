require 'rails_helper'

RSpec.describe Series, type: :model do
  context 'Associations' do
    it { should belong_to(:parent).class_name('Series') }

    it { should have_many(:attached_media).dependent(:destroy) }
    it { should have_many(:engagements).dependent(:destroy) }
    it { should have_many(:posts_series).dependent(:destroy) }
    it { should have_many(:series_access).dependent(:destroy) }
  end
end
