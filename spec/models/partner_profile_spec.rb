require 'rails_helper'

RSpec.describe PartnerProfile, type: :model do
  context 'Associations' do
    it { should belong_to(:partner) }

    it { should have_many(:attached_media).dependent(:destroy) }
    it { should have_many(:engagements).dependent(:destroy) }
  end
end
