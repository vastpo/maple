require 'rails_helper'

RSpec.describe User, type: :model do
  context 'Associations' do
    it { should have_one(:api_key) }
    it { should have_one(:partner_admin).dependent(:destroy) }

    it { should have_many(:attached_emojis).dependent(:destroy) }
    it { should have_many(:delivery_inventories).dependent(:destroy) }
    it { should have_many(:devices).dependent(:destroy) }
    it { should have_many(:engagements).dependent(:destroy) }
    it { should have_many(:message_deliveries).dependent(:destroy) }
    it { should have_many(:page_sessions).dependent(:destroy) }
    it { should have_many(:received_messages).class_name('Message').dependent(:destroy).through(:message_deliveries) }
    it { should have_many(:sent_messages).class_name('Message').dependent(:destroy) }
  end
end
