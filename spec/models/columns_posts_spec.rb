require 'rails_helper'

RSpec.describe ColumnsPosts, type: :model do
  context 'Associations' do
    it { should belong_to(:column) }
    it { should belong_to(:post) }
  end
end
