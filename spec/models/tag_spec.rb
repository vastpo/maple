require 'rails_helper'

RSpec.describe Tag, type: :model do
  context 'Associations' do
    it { should have_many(:engagements).dependent(:destroy) }
    it { should have_many(:posts_tags).class_name('PostsTags').dependent(:destroy) }
  end
end
