require 'rails_helper'

RSpec.describe PostsTags, type: :model do
  context 'Associations' do
    it { should belong_to(:post) }
    it { should belong_to(:tag) }
  end
end
