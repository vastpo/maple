require 'rails_helper'

RSpec.describe SeriesAccess, type: :model do
  context 'Associations' do
    it { should belong_to(:partner) }
    it { should belong_to(:series) }
  end
end
