require 'rails_helper'

RSpec.describe Engagement, type: :model do
  context 'Associations' do
    it { should belong_to(:component) }
    it { should belong_to(:device) }
    it { should belong_to(:media) }
    it { should belong_to(:page_session) }
    it { should belong_to(:user) }
  end
end
