require 'rails_helper'

RSpec.describe DeliveredComponent, type: :model do
  context 'Associations' do
    it { should belong_to(:page_session) }
  end
end
