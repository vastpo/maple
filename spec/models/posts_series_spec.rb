require 'rails_helper'

RSpec.describe PostsSeries, type: :model do
  context 'Associations' do
    it { should belong_to(:post) }
    it { should belong_to(:series) }
  end
end
