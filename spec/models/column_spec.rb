require 'rails_helper'

RSpec.describe Column, type: :model do
  context 'Associations' do
    it { should have_many(:attached_media).dependent(:destroy) }
    it { should have_many(:columns_posts).class_name('ColumnsPosts').dependent(:destroy) }
    it { should have_many(:delivery_locations).dependent(:destroy) }
    it { should have_many(:engagements).dependent(:destroy) }
  end
end
