require 'rails_helper'

RSpec.describe AttachedEmoji, type: :model do
  context 'Associations' do
    it { should belong_to(:post) }
    it { should belong_to(:user) }
  end
end
