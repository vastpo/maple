require 'rails_helper'

RSpec.describe PartnerAccessInvitation, type: :model do
  context 'Associations' do
    it { should belong_to(:invited_by) }
    it { should belong_to(:partner) }

    it { should have_one(:partner_access).dependent(:nullify) }
  end
end
