require 'rails_helper'

RSpec.describe APIKey, type: :model do
  context 'Associations' do
    it { should belong_to(:owner) }
  end
end
