require 'rails_helper'

RSpec.describe Device, type: :model do
  context 'Associations' do
    it { should belong_to(:user) }

    it { should have_many(:delivery_inventories).dependent(:destroy) }
    it { should have_many(:engagements).dependent(:destroy) }
    it { should have_many(:page_sessions).dependent(:destroy) }
  end
end
