require 'rails_helper'


RSpec.describe PartnerAdmin, type: :model do
  context 'Associations' do
    it { should belong_to(:user) }

    it { should have_one(:api_key) }

    it { should have_many(:partner_access).dependent(:destroy) }
  end
end
