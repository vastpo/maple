require 'rails_helper'

RSpec.describe PartnerManagement, type: :model do
  context 'Associations' do
    it { should belong_to :admin }
    it { should belong_to :partner }
  end
end
