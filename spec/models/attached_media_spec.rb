require 'rails_helper'

RSpec.describe AttachedMedia, type: :model do
  context 'Associations' do
    it { should belong_to(:attachable) }
    it { should belong_to(:media) }
  end
end
