require 'rails_helper'

RSpec.describe Partner, type: :model do
  context 'Associations' do
    it { should have_one(:api_key) }
    it { should have_one(:profile).class_name('PartnerProfile').dependent(:destroy) }

    it { should have_many(:access_invitations).class_name('PartnerAccessInvitation').dependent(:destroy) }
    it { should have_many(:partner_access).dependent(:destroy) }
    it { should have_many(:partner_managements).dependent(:destroy) }
    it { should have_many(:posts).dependent(:destroy) }
    it { should have_many(:series_access).dependent(:destroy) }
  end
end
