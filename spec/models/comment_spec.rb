require 'rails_helper'

RSpec.describe Comment, type: :model do
  context 'Association' do
    it { should belong_to(:parent).class_name('Comment') }
    it { should belong_to(:post) }
    it { should belong_to(:user) }

    it { should have_many(:sub_comments).class_name('Comment') }
  end
end
