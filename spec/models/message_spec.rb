require 'rails_helper'

RSpec.describe Message, type: :model do
  context 'Associations' do
    it { should belong_to(:sent_by).class_name('User') }

    it { should have_one(:capture).dependent(:destroy) }

    it { should have_many(:deliveries).class_name('MessageDelivery').dependent(:destroy) }
  end
end
