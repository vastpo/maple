require 'rails_helper'

RSpec.describe Admin, type: :model do
  context 'Associations' do
    it { should have_one(:api_key) }
    it { should have_one(:profile).class_name('AdminProfile').dependent(:destroy) }

    it { should have_many(:partner_access_invitations).dependent(:destroy) }
    it { should have_many(:partner_managements).dependent(:destroy) }
    it { should have_many(:managing_partners).dependent(:destroy) }
  end
end
