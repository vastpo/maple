require 'rails_helper'

RSpec.describe DeliveryInventory, type: :model do
  context 'Associations' do
    it { should belong_to(:device) }
    it { should belong_to(:user) }
  end
end
