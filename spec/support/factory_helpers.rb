module FactoryHelpers
  def locale(str, locales = I18n.available_locales)
    locales.inject({}) { |h, l| h[l] = "#{str} (#{l})"; h }
  end

  module_function :locale
end
