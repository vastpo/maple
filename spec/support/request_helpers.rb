module RequestHelpers
  def json
    JSON.parse(response.body).deep_symbolize_keys
  end

  def v1
    'application/vnd.acme-v1+json'
  end

  def signin(member)
    post "/api/#{member.class.name.downcase}/signin", params: { login: member.login, password: 'P@ssw0rd' }, headers: { 'Accept' => version }
    json
  end

  def payload(auth: {})
    headers = { Accept: version }.merge!(auth)
    { params: @params || {}, headers: headers }
  end
end
