require 'rails_helper'

RSpec.describe API::V1::User do
  let(:version) { v1 }

  context 'POST /api/user/signup' do
    let(:route) { api_user_signup_path }
    let(:params) {{
      login: 'some_user', password: '123456', display_name: 'Some User',
      gender: 'X', date_of_birth: 20.years.ago.to_date, timezone: 8,
      location: :hong_kong, phone: '+852 66666666'
    }}

    it 'normal signup' do
      @params = params
      post route, payload

      response_json = json

      expect(response_json[:login]).to eq params[:login]
      expect(BCrypt::Password.new(response_json[:password_digest])).to eq params[:password]
      expect(response_json[:display_name]).to eq params[:display_name]

      api_key = APIKey.where(owner_type: 'User', owner_id: response_json[:id])
      expect(api_key).to exist
      expect(api_key.first.access_key).to be_present
      expect(api_key.first.secret_key).to be_present
    end

    it 'signup with existing login name' do
      @params = params
      post route, payload
      post route, payload

      expect(json[:error]).to eq 'Validation failed: Login has already been taken'
    end

    [(32..45).to_a, 47, (58..64).to_a, (91..94).to_a, 96, (123..126).to_a].flatten.each do |c|
      it "signup with login name contains invalid character #{c.chr}" do
        @params = params
        @params[:login] = "stephy#{c.chr}cat"
        post route, payload

        expect(json[:error]).to eq 'login is invalid'
      end
    end
  end
end
