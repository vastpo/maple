require 'rails_helper'

RSpec.describe API::V1::Partner do
  let(:version) { v1 }

  context 'POST /api/partner/signup' do
    let(:admin) { create(:admin, :account_manager) }
    let!(:auth) { signin(admin) }
    let(:route) { api_partner_signup_path }
    let(:params) {{
      login: 'some_partner', password: '123456', display_name: locale('Some Partner'),
      preferred_language: :tc, preferred_currency: :hkd
    }}

    context 'normal signup by admin' do
      it 'without profile review' do
        @params = params
        post route, payload(auth: auth)

        response_json = json

        expect(response_json[:login]).to eq params[:login]
        expect(BCrypt::Password.new(response_json[:password_digest])).to eq params[:password]
        expect(response_json[:display_name]).to eq params[:display_name]

        api_key = APIKey.where(owner_type: 'Partner', owner_id: response_json[:id])
        expect(api_key).to exist
        expect(api_key.first.access_key).to be_present
        expect(api_key.first.secret_key).to be_present

        profile = PartnerProfile.where(partner_id: response_json[:id])
        expect(profile).to exist
        expect(profile.first.state).to eq :draft
      end

      it 'with profile review' do
        @params = params.merge!(is_review_profile: true)
        post route, payload(auth: auth)

        response_json = json

        expect(response_json[:login]).to eq params[:login]
        expect(BCrypt::Password.new(response_json[:password_digest])).to eq params[:password]
        expect(response_json[:display_name]).to eq params[:display_name]

        api_key = APIKey.where(owner_type: 'Partner', owner_id: response_json[:id])
        expect(api_key).to exist
        expect(api_key.first.access_key).to be_present
        expect(api_key.first.secret_key).to be_present

        profile = PartnerProfile.where(partner_id: response_json[:id])
        expect(profile).to exist
        expect(profile.first.state).to eq :reviewing
      end
    end

    it 'signup with existing login name' do
      @params = params
      post route, payload(auth: signin(admin))
      travel_to 1.seconds.from_now
      post route, payload(auth: signin(admin))

      expect(json[:error]).to eq 'Validation failed: Login has already been taken'
    end

    it 'signup without admin signed in' do
      @params = params
      post route, payload

      expect(json[:error]).to eq 'Missing Access-Key'
    end

    [(32..45).to_a, 47, (58..64).to_a, (91..94).to_a, 96, (123..126).to_a].flatten.each do |c|
      it "signup with login name contains invalid character #{c.chr}" do
        @params = params
        @params[:login] = "stephy#{c.chr}cat"
        post route, payload

        expect(json[:error]).to eq 'login is invalid'
      end
    end
  end

  context 'POST /api/partner/show' do
    let(:route) { api_partner_show_path }
    let(:partner) { create(:partner) }

    context 'signin as admin' do
      let(:admin) { create(:admin, :viewer) }
      let(:auth) { signin(admin) }

      it 'should able to show a partner' do
        @params = { id: partner.id }
        post route, payload(auth: auth)

        expect(json[:id]).to eq partner.id
      end
    end

    context 'signin as partner' do
      let(:auth) { signin(partner) }

      it 'should able to show self' do
        @params = { id: partner.id }
        post route, payload(auth: auth)

        expect(json[:id]).to eq partner.id
      end

      it 'should not able to show another partner' do
        another_partner = create(:partner, login: 'another_partner')
        @params = { id: another_partner.id }
        post route, payload(auth: auth)

        expect(response).to have_http_status(500)
      end
    end
  end

  context 'PUT /api/partner/modify' do
    let(:route) { api_partner_modify_path }

    let(:partner_params) {{
      login: 'hehe',
      password: 'haha',
      external_id: 'hehehahahehehahahehehahahehehaha',
      display_name: locale('hehe haha'),
      preferred_language: 'tc',
      preferred_currency: 'twd'
    }}

    let(:profile_params) {{
      short_note: locale('new short note'),
      long_note: locale('new long note'),
      media_details: {},
      links: [ 'http://www.vastpo.com', 'http://www.icable.com' ]
    }}

    context 'signin as admin' do
      let(:admin) { create(:admin, :account_executive) }
      let!(:auth) { signin(admin) }

      it 'should able to modify a partner' do
        @params = { id: create(:partner).id }.merge!(partner_params).merge(profile_params)
        put route, payload(auth: auth)

        response_json = json
        expect(response_json).to include(partner_params.except(:password))
        expect(response_json[:profile]).to include(profile_params)
      end
    end

    context 'signin as partner' do
      let(:partner) { create(:partner) }
      let!(:auth) { signin(partner) }

      it 'should able to modify self' do
        @params = { id: partner.id }.merge(partner_params.except(:login, :external_id)).merge(profile_params)
        put route, payload(auth: auth)

        response_json = json
        expect(response_json).to include(partner_params.except(:login, :password, :external_id))
        expect(response_json[:profile]).to include(profile_params)
      end

      it 'should not able to modify self login' do
        @params = { id: partner.id }.merge(partner_params).merge(profile_params)

        expect do
          put route, payload(auth: auth)
          expect(response).to have_http_status(500)
        end.not_to change{ Partner.first.login }
      end

      it 'should not able to modify self external_id' do
        @params = { id: partner.id }.merge(partner_params).merge(profile_params)

        expect do
          put route, payload(auth: auth)
          expect(response).to have_http_status(500)
        end.not_to change{ Partner.first.external_id }
      end

      it 'should not able to modify another partner' do
        another_partner = create(:partner, login: 'another_partner')

        @params = { id: another_partner.id }.merge(partner_params).merge(profile_params)
        put route, payload(auth: auth)

        expect(response).to have_http_status(500)
      end
    end
  end
end
