require 'rails_helper'

RSpec.describe API::V1::PartnerAdmin do
  let(:admin) { create(:admin, :super_admin) }
  let!(:auth) { signin(admin) }
  let(:version) { v1 }

  context 'POST /api/partner_admin/signup' do
    let(:route) { api_partner_admin_signup_path }
    let(:login) { 'some_partner_admin' }
    let(:password) { '123456' }
    let(:timezone) { 8 }

    context 'normal signup by admin' do
      it 'normal signup' do
        @params = { login: login, password: password, timezone: timezone  }
        post route, payload(auth: auth)

        response_json = json

        expect(response_json[:login]).to eq login
        expect(BCrypt::Password.new(response_json[:password_digest])).to eq password
        expect(response_json[:timezone]).to eq timezone

        api_key = APIKey.where(owner_type: 'PartnerAdmin', owner_id: response_json[:id])
        expect(api_key).to exist
        expect(api_key.first.access_key).to be_present
        expect(api_key.first.secret_key).to be_present
      end

      it 'signup with existing login name' do
        @params = { login: login, password: password, timezone: timezone }
        post route, payload(auth: signin(admin))
        travel_to 1.seconds.from_now
        post route, payload(auth: signin(admin))

        expect(json[:error]).to eq 'Validation failed: Login has already been taken'
      end

      it 'signup without admin signed in' do
        @params = { login: login, password: password, timezone: timezone }
        post route, payload

        expect(json[:error]).to eq 'Missing Access-Key'
      end

      [(32..45).to_a, 47, (58..64).to_a, (91..94).to_a, 96, (123..126).to_a].flatten.each do |c|
        it "signup with login name contains invalid character #{c.chr}" do
          login = "stephy#{c.chr}cat"
          @params = { login: login, password: password, timezone: timezone }
          post route, payload

          expect(json[:error]).to eq 'login is invalid'
        end
      end
    end
  end
end
