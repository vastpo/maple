require 'rails_helper'

RSpec.describe API::V1::Admin do
  let!(:auth) { signin(admin) }
  let(:version) { v1 }

  context 'POST /api/admin/signup' do
    let(:admin) { create(:admin, :super_admin) }
    let(:route) { api_admin_signup_path }
    let(:login) { 'some_admin' }
    let(:password) { '123456' }
    let(:display_name) { 'Some Admin' }

    it 'should able to signup by super_admin' do
      @params = { login: login, password: password, display_name: display_name, nickname: 'admin bb'  }
      post route, payload(auth: auth)

      response_json = json

      expect(response_json[:login]).to eq login
      expect(BCrypt::Password.new(response_json[:password_digest])).to eq password
      expect(response_json[:display_name]).to eq display_name

      api_key = APIKey.where(owner_type: 'Admin', owner_id: response_json[:id])
      expect(api_key).to exist
      expect(api_key.first.access_key).to be_present
      expect(api_key.first.secret_key).to be_present

      profile = AdminProfile.where(id: response_json[:id])
      expect(profile).to exist
    end

    it 'should not able to signup with existing login name' do
      @params = { login: login, password: password, display_name: display_name, nickname: 'admin bb' }
      post route, payload(auth: signin(admin))
      travel_to 1.seconds.from_now
      post route, payload(auth: signin(admin))

      expect(json[:error]).to eq 'Validation failed: Login has already been taken'
    end

    it 'should not able to signup without admin signed in' do
      @params = { login: login, password: password, display_name: display_name, nickname: 'admin bb' }
      post route, payload

      expect(json[:error]).to eq 'Missing Access-Key'
    end

    (Admin::ROLES - [ :super_admin ]).each do |role|
      it "should not able to signup by #{role}" do
        @params = { login: login, password: password, display_name: display_name, nickname: 'admin bb' }
        post route, payload(auth: signin(create(:admin, role)))

        expect(response).to have_http_status(500)
      end
    end

    [(32..45).to_a, 47, (58..64).to_a, (91..94).to_a, 96, (123..126).to_a].flatten.each do |c|
      it "should not able to signup with login name contains invalid character #{c.chr}" do
        login = "stephy#{c.chr}cat"
        @params = { login: login, password: password, display_name: display_name, nickname: 'admin bb' }
        post route, payload

        expect(json[:error]).to eq 'login is invalid'
      end
    end
  end

  context 'POST /api/admin/show' do
    let(:route) { api_admin_show_path }

    context 'signin as super admin' do
      let(:admin) { create(:admin, :super_admin) }

      it 'should able to show self' do
        @params = { id: admin.id }
        post route, payload(auth: auth)

        expect(json[:id]).to eq admin.id
      end

      it 'should able to show another admin' do
        another_admin = create(:admin, :super_admin, login: 'another_super_admin')
        @params = { id: another_admin.id }
        post route, payload(auth: auth)

        expect(json[:id]).to eq another_admin.id
      end
    end

    context 'signin as non super_admin' do
      let(:admin) { create(:admin, :viewer) }

      it 'should able to show self' do
        @params = { id: admin.id }
        post route, payload(auth: auth)

        expect(json[:id]).to eq admin.id
      end

      it 'should not able to show another admin' do
        another_admin = create(:admin, :viewer, login: 'another_admin')
        @params = { id: another_admin.id }
        post route, payload(auth: auth)

        expect(response).to have_http_status(500)
      end
    end
  end

  context 'PUT /api/admin/modify' do
    let(:route) { api_admin_modify_path }

    let(:admin_params) {{
      login: 'hehe',
      password: 'haha',
      display_name: 'hehe haha',
      roles: ['super_admin', 'another_admin_role']
    }}

    let(:profile_params) {{
      first_name: 'Hehe',
      last_name: 'Haha',
      nickname: 'She She',
      gender: 'F',
      phones: ['+85223456789', '+85234567890'],
      emails: ['hehe@gmail.com', 'sheshe@yahoo.com.hk']
    }}

    context 'signin as super admin' do
      let(:admin) { create(:admin, :super_admin) }

      it 'should able to modify self' do
        @params = { id: Admin.first.id }.merge!(admin_params).merge(profile_params)
        put route, payload(auth: auth)

        response_json = json
        expect(response_json).to include(admin_params.except(:password))
        expect(response_json[:profile]).to include(profile_params)
      end

      it 'should able to modify another super_admin' do
        another_admin = create(:admin, :super_admin, login: 'another_super_admin')
        @params = { id: another_admin.id }.merge!(admin_params).merge(profile_params)
        put route, payload(auth: auth)

        response_json = json
        expect(response_json).to include(admin_params.except(:password))
        expect(response_json[:profile]).to include(profile_params)
      end

      it 'should able to modify another non super_admin' do
        another_admin = create(:admin, :viewer, login: 'another_admin')
        @params = { id: another_admin.id }.merge!(admin_params).merge(profile_params)
        put route, payload(auth: auth)

        response_json = json
        expect(response_json).to include(admin_params.except(:password))
        expect(response_json[:profile]).to include(profile_params)
      end
    end

    context 'signin as non super_admin' do
      let!(:admin) { create(:admin, :viewer) }

      it 'should able to modify self' do
        @params = { id: Admin.first.id }.merge!(admin_params.except(:login, :roles)).merge(profile_params)
        put route, payload(auth: signin(admin))

        response_json = json
        expect(response_json).to include(admin_params.except(:login, :password, :roles))
        expect(response_json[:profile]).to include(profile_params)
      end

      it 'should not able to modify self login' do
        @params = { id: Admin.first.id }.merge!(admin_params).merge(profile_params)

        expect do
          put route, payload(auth: signin(admin))
          expect(response).to have_http_status(500)
        end.not_to change{ Admin.first.login }
      end

      it 'should not able to modify self roles' do
        @params = { id: Admin.first.id }.merge!(admin_params).merge(profile_params)

        expect do
          put route, payload(auth: signin(admin))
          expect(response).to have_http_status(500)
        end.not_to change{ Admin.first.roles }
      end

      it 'should not able to modify another admin' do
        another_admin = create(:admin, :viewer, login: 'another_super_admin')

        @params = { id: another_admin.id }.merge!(admin_params).merge(profile_params)
        put route, payload(auth: signin(admin))

        expect(response).to have_http_status(500)
      end
    end
  end
end
