require 'rails_helper'

RSpec.describe API::V1 do
  let(:admin) { create(:admin, :super_admin) }
  let!(:auth) { signin(admin) }
  let(:version) { v1 }

  let(:type) { 'Admin' }
  let(:route) { api_test_path }

  it 'normal request' do
    @params = { type: type }
    post route, payload(auth: auth)
    expect(json).to eq({ version: 'v1', type: 'Admin', result: 'OK' })
  end

  it 'Missing Access-Key' do
    @params = { type: type }
    auth['Access-Key'] = nil
    post route, payload(auth: auth)
    expect(json).to eq({ error: 'Missing Access-Key' })
  end

  it 'Missing Signature' do
    @params = { type: type }
    auth['Signature'] = nil
    post route, payload(auth: auth)
    expect(json).to eq({ error: 'Missing Signature' })
  end

  it 'Invalid Access-Key' do
    @params = { type: type }
    auth['Access-Key'] = 'xxxxxxxx'
    post route, payload(auth: auth)
    expect(json).to eq({ error: 'Invalid Access-Key' })
  end

  it 'Invalid Signature' do
    @params = { type: type }
    auth['Signature'] = 'xxxxxxxx'
    post route, payload(auth: auth)
    expect(json).to eq({ error: 'Not enough or too many segments' })
  end

  it 'request before designated time' do
    @params = { type: type }
    travel_to "-#{ENV['JWT_EXPIRE_AFTER']}".to_i.seconds.from_now do
      post route, payload(auth: auth)
      expect(json).to eq({ error: 'Invalid Signature' })
    end
  end

  it "request after #{ENV['JWT_EXPIRE_AFTER']} seconds" do
    @params = { type: type }
    travel_to ENV['JWT_EXPIRE_AFTER'].to_i.seconds.from_now do
      post route, payload(auth: auth)
      expect(json).to eq({ error: 'Signature has expired' })
    end
  end

  # it 'repeated use of signature' do
  #   @params = { type: type }
  #   post route, payload(auth: auth)
  #   post route, payload(auth: auth)
  #   expect(json).to eq({ error: 'Signature has expired' })
  # end
end
