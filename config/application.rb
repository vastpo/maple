require_relative 'boot'

require "rails"
# Pick the frameworks you want:
require "active_model/railtie"
require "active_job/railtie"
require "active_record/railtie"
require "action_controller/railtie"
require "action_mailer/railtie"
require "action_view/railtie"
require "action_cable/engine"
require "i18n/backend/fallbacks"
require "sprockets/railtie"
# require "rails/test_unit/railtie"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)
I18n::Backend::Simple.send(:include, I18n::Backend::Fallbacks)

module Maple
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.1

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
    config.time_zone = 'Etc/UTC'
    config.active_record.default_timezone = :local

    # Custom directories with classes and modules you want to be autoloadable.
    # Note: we want the autoloadable libraries are also eager loaded in
    # production, reference this nice article on autoloading/eager-loading:
    # http://blog.arkency.com/2014/11/dont-forget-about-eager-load-when-extending-autoload
    config.eager_load_paths += Dir[Rails.root.join('lib', '**/')]
    config.eager_load_paths += Dir[Rails.root.join('spec', 'support', '**/')]

    # Only loads a smaller set of middleware suitable for API only apps.
    # Middleware like session, flash, cookies can be added back manually.
    # Skip views, helpers and assets when generating a new resource.
    config.api_only = true

    # Redis cache store configuration
    config.cache_store = :redis_store, {
      servers: [
        {
          host: ENV['REDIS_HOST'],
          port: ENV['REDIS_PORT'],
          db: ENV['REDIS_DB'],
          password: ENV['REDIS_PASSWORD'],
          namespace: 'cache'
        },
      ],
      expire_after: ENV['REDIS_EXPIRE_AFTER'].to_i.minutes
    }

    # Internationalization configuration
    config.i18n.load_path += Dir[Rails.root.join('config', 'locales', '**', '*.{rb,yml}')]
    config.i18n.available_locales = [:'en-US', :'zh-HK', :'zh-CN', :'zh-TW']
    config.i18n.default_locale = :'en-US'
    config.i18n.enforce_available_locales = false

    # cors configuration
    config.middleware.insert_before 0, Rack::Cors do
      allow do
        origins '*'
        resource '*', headers: :any, methods: [:get, :post, :options]
      end
    end

    # Support grape API
    config.paths.add File.join('app', 'api'), glob: File.join('**', '*.rb')
    config.autoload_paths += Dir[Rails.root.join('app')]
    config.eager_load_paths += Dir[Rails.root.join('app')]

    config.generators do |g|
      g.orm :mongoid
    end
    config.i18n.fallbacks = {'es' => 'en'}
  end
end
