require 'orientdb4r'

$orientdb = Orientdb4r.client
$orientdb.connect database: ENV['ORIENTDB_DB_NAME'], user: ENV['ORIENTDB_USERNAME'], password: ENV['ORIENTDB_PASSWORD']
