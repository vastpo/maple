if Rails.env.development?
  GrapeSwaggerRails.options.app_name = 'VastPo'
  GrapeSwaggerRails.options.app_url  = '/api'
  GrapeSwaggerRails.options.url      = '/v1/swagger.json'
end
