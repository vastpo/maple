$redis = Redis::Namespace.new("Maple", redis: Redis.new)
$redis.auth(ENV['REDIS_PASSWORD']) if ENV['REDIS_PASSWORD'].present?
