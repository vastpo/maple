Maple::Application.config.session_store :redis_store, {
  servers: [
    {
      host: ENV['REDIS_HOST'],
      port: ENV['REDIS_PORT'],
      db: ENV['REDIS_DB'],
      password: ENV['REDIS_PASSWORD'],
      namespace: 'session'
    },
  ],
  expire_after: ENV['REDIS_EXPIRE_AFTER'].to_i.minutes,
  key: "_#{Rails.application.class.parent_name.downcase}_session"
}
