# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

ActiveRecord::Base.transaction do
  Configs::Currency.create!(iso_code: :hkd, is_enabled: true)
  Configs::Currency.create!(iso_code: :cny, is_enabled: true)
  Configs::Currency.create!(iso_code: :twd, is_enabled: true)
  Configs::Currency.create!(iso_code: :sgd, is_enabled: true)
  Configs::Currency.create!(iso_code: :thb, is_enabled: true)
  Configs::Currency.create!(iso_code: :myr, is_enabled: true)
  Configs::Currency.create!(iso_code: :jpy, is_enabled: true)
  Configs::Currency.create!(iso_code: :rub, is_enabled: true)
  Configs::Currency.create!(iso_code: :aed, is_enabled: true)
  Configs::Currency.create!(iso_code: :aud, is_enabled: true)
  Configs::Currency.create!(iso_code: :eur, is_enabled: true)
  Configs::Currency.create!(iso_code: :gbp, is_enabled: true)
  Configs::Currency.create!(iso_code: :chf, is_enabled: true)
  Configs::Currency.create!(iso_code: :cad, is_enabled: true)
  Configs::Currency.create!(iso_code: :usd, is_enabled: true)

  stephy = Admin.create!(
    login: 'stephycat',
    password: 'victoriabb',
    display_name: 'Stephy Cat',
    roles: [:super_admin]
  )
  AdminProfile.create!(
    admin: stephy,
    first_name: 'Stephy',
    last_name: 'Cat',
    nickname: 'Cathy',
    gender: 'X',
    phones: ['+85262022001'],
    emails: ['stephycat@link.cuhk.edu.hk'],
    role_details: {}
  )
  APIKey.create!(
    owner: stephy,
    access_key: '4d77b4dd21278ab56c4e09af48c35e2b0d8847961d36480eeaab687cfedbc5af',
    secret_key: 'cb1696d240e42aabeff2da6ab1e0969fb33d63645abf673bee2ac2a0d91683ec'
  )
  stephy.active!

  ActiveRecord::Base.transaction do
    jl = Partner.create!(
      external_id: '69efa484f7332daeac52c01cf2611174fc258909',
      login: 'jl_solutant',
      password: 'jl',
      display_name: I18n.available_locales.inject({}) { |h,k| h[k] = "JL Solutant (#{k})"; h },
      preferred_language: :en,
      preferred_currency: :hkd
    )
    PartnerProfile.create!(
      partner: jl,
      external_id: 'jlprofile',
      display_name: I18n.available_locales.inject({}) { |h,k| h[k] = "JL Solutant (#{k})"; h },
      is_badged: false,
      media_details: {},
      interaction_details: {},
      score_details: {},
      links: {},
      is_ad_disabled: false,
      is_running_ad: false,
      base_url: 'http://www.jlsolutant.com'
    )
    PartnerPayoutPolicy.create!(
      partner: jl,
      fee_rate: {
        hkd: 0.1,
        usd: 0.15,
        twd: 0.125
      },
      fee_fix: {
        hkd: 1,
        usd: 0.1282,
        twd: 3.8
      },
      currency: :hkd,
      rpe: 0.01,
      is_active: true,
      remarks: 'Sample Policy'
    )
    APIKey.create!(
      owner: jl,
      access_key: '8fe9eeea8cb4b72e206bd0dd332f7b9e73833e3753264cfa31d6c3d49ff471af',
      secret_key: '76142b2aacca090f0beb36e542fcd547237cd7330af5be7a8cb063fabd9bd6e0'
    )
    jl.active!
    jl.profile.review!

    PartnerManagement.create!(admin: stephy, partner: jl, is_active: true)

    invitation = PartnerAccessInvitation.create!(
      invited_by: stephy,
      partner: jl,
      email: "stephycat@link.cuhk.edu.hk",
      roles: [:publisher, :editor],
      token: Digest::SHA512.hexdigest(Time.current.to_f.to_s),
      expired_at: 10.years.from_now
    )

    jl_admin = PartnerAdmin.create!(
      user: nil,
      login: 'jl',
      password: 'jl',
      timezone: 'Europe/Ljubljana',
      access_details: {}
    )
    APIKey.create!(
      owner: jl_admin,
      access_key: '082dc3de8d9a58f638c6202c776c389da10f1350064a71525cbefa6d0242f075',
      secret_key: '52eedb56a16d1a726b980cd362ac3a23d0b2c854d9c688fd7a4bf2e2e2348373'
    )

    PartnerAccess.create!(
      partner_admin: jl_admin,
      partner: jl,
      invitation: invitation,
      display_name: 'Super Admin',
      roles: [:super_admin],
      location_accesses: [],
      phones: [],
      emails: [],
      sort_order: 0,
      is_active: true
    )
  end

  ActiveRecord::Base.transaction do
    vic = Partner.create!(
      external_id: '69efa484f7332daeac52c01cf2611174fc258909',
      login: 'vic',
      password: 'vic',
      display_name: I18n.available_locales.inject({}) { |h,k| h[k] = "Victoria Kwok (#{k})"; h },
      preferred_language: :en,
      preferred_currency: :hkd
    )
    PartnerProfile.create!(
      partner: vic,
      external_id: 'vicprofile',
      display_name: I18n.available_locales.inject({}) { |h,k| h[k] = "Victoria Kwok (#{k})"; h },
      is_badged: false,
      media_details: {},
      interaction_details: {},
      score_details: {},
      links: {},
      is_ad_disabled: false,
      is_running_ad: false,
      base_url: 'http://www.victoriasecret.com'
    )
    PartnerPayoutPolicy.create!(
      partner: vic,
      fee_rate: {
        hkd: 0.1,
        usd: 0.15,
        twd: 0.125
      },
      fee_fix: {
        hkd: 1,
        usd: 0.1282,
        twd: 3.8
      },
      currency: :hkd,
      rpe: 0.01,
      is_active: true,
      remarks: 'Sample Policy'
    )
    APIKey.create!(
      owner: vic,
      access_key: '0cb99671780b9a108f2d4e753e31fdd1d47824410747ce7412a53bbdf7ffb7e2',
      secret_key: '1ffe2a5f5052a025d8dee98bcc8cda8bdbdc9b3f34575b981ec9a8621df23418'
    )
    vic.active!
    vic.profile.review!

    PartnerManagement.create!(admin: stephy, partner: vic, is_active: true)

    invitation = PartnerAccessInvitation.create!(
      invited_by: stephy,
      partner: vic,
      email: "victoria_tyy@gmail.com",
      roles: [:publisher, :editor],
      token: Digest::SHA512.hexdigest(Time.current.to_f.to_s),
      expired_at: 10.years.from_now
    )

    vic_admin = PartnerAdmin.create!(
      user: nil,
      login: 'vic',
      password: 'vic',
      timezone: 'Asia/Hong_Kong',
      access_details: {}
    )
    APIKey.create!(
      owner: vic_admin,
      access_key: 'b60fd5a61919e5f920e98b049948e2aa288ca6506691010d2bc1d36dc15864ed',
      secret_key: '4d81348fbda67396b82521d36b191bdc2bc57ceac74053dfa183724a7f7d0e22'
    )

    PartnerAccess.create!(
      partner_admin: vic_admin,
      partner: vic,
      invitation: invitation,
      display_name: 'Super Admin',
      roles: [:super_admin],
      location_accesses: [],
      phones: [],
      emails: [],
      sort_order: 0,
      is_active: true
    )

    (1..1000).each do
      title = Faker::Lorem.sentence(rand(5..8))
      note = Faker::Lorem.sentences(rand(3..6)).join(' ')
      post = vic.posts.create!(
        created_by: vic_admin,
        external_id: Digest::SHA256.hexdigest(SecureRandom.uuid),
        post_type: [:image, :video].sample,
        title: I18n.available_locales.inject({}) { |h, k| h[k] = "#{title} (#{k})"; h },
        note: I18n.available_locales.inject({}) { |h, k| h[k] = "#{note} (#{k})"; h },
        is_masked: false,
        is_emoji_blocked: false,
        is_comment_blocked: false,
        is_capture_blocked: false,
        is_ad_disabled: false,
        is_running_ad: true,
        is_counting_rpe: [true, false].sample,
        base_url: ''
      )
      if rand > 0.2
        (rand > 0.1) ? post.publish! : post.review!
      end
    end

    (1..200).each do |n|
      title = Faker::Lorem.sentence(rand(5..8))
      short_note = Faker::Lorem.sentences(rand(3..6)).join(' ')
      long_note = Faker::Lorem.sentences(rand(3..6)).join(' ')
      series = Series._create!(
        created_by: vic_admin,
        external_id: Digest::SHA256.hexdigest(SecureRandom.uuid),
        title: I18n.available_locales.inject({}) { |h, k| h[k] = "#{title} (#{k})"; h },
        short_note: I18n.available_locales.inject({}) { |h, k| h[k] = "#{short_note} (#{k})"; h },
        long_note: I18n.available_locales.inject({}) { |h, k| h[k] = "#{long_note} (#{k})"; h },
        is_ad_disabled: false,
        is_running_ad: false,
        base_url: ''
      )
      series_access = SeriesAccess.create!(
        partner: vic,
        series: series,
        is_owner: true,
        is_editing_allowed: true,
        is_contribution_allowed: true,
        is_active: true
      )
      if rand > 0.2
        (rand > 0.1) ? series.publish! : series.review!
      end
    end
  end

  johnny = User.create!(
    login: 'johnny',
    password: 'johnny',
    display_name: 'Cheung Chi Fung Johnny',
    gender: 'M',
    dob_year: 1989,
    dob_month: 3,
    dob_day: 5,
    timezone: 8,
    location: 'hk',
    phone: '+85267510577',
    phone_verified_at: nil,
    email: nil,
    email_verified_at: nil,
    connected_profiles: {},
    preferred_currency: 'hkd',
    is_hd_preferred: true,
    is_video_autoplay: false,
    partner_admin_details: {}
  )
  APIKey.create!(
    owner: johnny,
    access_key: 'b7708228db3dd0148d1c30bc8ba8ca338b729ffa14d5bd15dc64edc294498e79',
    secret_key: '6032b2f40651a268d3d9c6d749898d5a20597f2e98ddeab29b1c50595ee302da'
  )
  johnny.active!
  (1..19).each do
    cred = Proc.new{ s = nil; loop { s = Faker::Lorem.characters(3); break if s =~ /^[a-z]+$/ }; s }.call;
    user = User.create!(
      login: cred,
      password: cred,
      display_name: Faker::Name.name,
      gender: ['F', 'M', 'X'].sample,
      dob_year: 1989,
      dob_month: 6,
      dob_day: 4,
      timezone: 8,
      location: 'hk',
      phone: "+852#{[5,6,9].sample}#{Faker::Number.number(7)}",
      phone_verified_at: nil,
      email: nil,
      email_verified_at: nil,
      connected_profiles: {},
      preferred_currency: 'hkd',
      is_hd_preferred: true,
      is_video_autoplay: false,
      partner_admin_details: {}
    );
    APIKey.create!(
      owner: user,
      access_key: Digest::SHA256.hexdigest(SecureRandom.uuid),
      secret_key: Digest::SHA256.hexdigest(SecureRandom.uuid)
    );
    6.times.each do
      device_id = SecureRandom.uuid
      service = [['APN'] * 50, ['FCM'] * 50, 'Unknown'].flatten.sample
      os = { 'APN' => 'ios', 'FCM' => ['ios', 'android'].sample, 'Unknown' => ['ios', 'android'].sample }
      device = Device.create!(
        users: [ user ],
        device_id: device_id,
        operating_system: os[service],
        token: Digest::SHA512.hexdigest(device_id),
        country: ISO3166::Country.countries.map(&:alpha2).sort.sample,
        service: service,
      );
      UserDevicePushNotification.create!(
        user: user,
        device: device,
        service: 'MessageEntry::Personal',
        is_active: true
      )
    end
    user.active!;
  end
end
