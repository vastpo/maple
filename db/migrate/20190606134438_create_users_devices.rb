class CreateUsersDevices < ActiveRecord::Migration[5.2]
  def change
    create_table :users_devices, id: false do |t|
      t.references :user, foreign_key: true, null: false
      t.references :device, foreign_key: { primary_key: :device_id }, null: false, type: :string
      t.boolean    :is_active, null: false, default: true

      t.index      [ :user_id, :device_id ], name: :index_users_devices_on_unique_key, unique: true
    end
  end
end
