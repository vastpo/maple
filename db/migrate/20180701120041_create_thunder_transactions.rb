class CreateThunderTransactions < ActiveRecord::Migration[5.1]
  def change
    create_table :thunder_transactions do |t|
      t.references :thunder_offer, foreign_key: true, null: false
      t.references :user, foreign_key: true, null: true
      t.string     :stripe_customer_id, null: true
      t.string     :stripe_charge_id, null: true
      t.string     :stripe_transfer_id, null: true
      t.boolean    :is_stripe_payout, null: false
      t.string     :destination_account, null: true
      t.string     :currency, null: false
      t.decimal    :amount, precision: 9, scale: 3, null: false
      t.decimal    :fee, precision: 8, scale: 3, null: false
      t.boolean    :is_captured, null: false

      t.timestamps
    end
  end
end
