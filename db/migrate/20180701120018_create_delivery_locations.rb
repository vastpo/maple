class CreateDeliveryLocations < ActiveRecord::Migration[5.1]
  def change
    create_table :delivery_locations do |t|
      t.references :deliverable, polymorphic: true
      t.string     :location, null: false
      t.boolean    :is_active, null: false, default: true

      t.timestamps
    end
  end
end
