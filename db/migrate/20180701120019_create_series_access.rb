class CreateSeriesAccess < ActiveRecord::Migration[5.1]
  def change
    create_table :series_access do |t|
      t.references :partner, foreign_key: true, null: false
      t.references :series, foreign_key: true, null: false
      t.boolean    :is_owner, null: false
      t.boolean    :is_editing_allowed, null: false
      t.boolean    :is_contribution_allowed, null: false
      t.boolean    :is_active, null: false, default: true

      t.timestamps

      t.index      [ :partner_id, :series_id ], unique: true
    end
  end
end
