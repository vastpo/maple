class CreatePostsSeries < ActiveRecord::Migration[5.1]
  def change
    create_table :posts_series, id: false do |t|
      t.references :post, foreign_key: true, null: false
      t.references :series, foreign_key: true, null: false
      t.boolean    :is_active, null: false, default: true

      t.timestamps

      t.index      [ :post_id, :series_id ], unique: true
      t.index      [ :series_id, :post_id ], unique: true
    end
  end
end
