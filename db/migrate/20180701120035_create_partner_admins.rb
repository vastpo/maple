class CreatePartnerAdmins < ActiveRecord::Migration[5.1]
  def change
    create_table :partner_admins do |t|
      t.references :user, foreign_key: true, null: true
      t.string     :login, null: false, unique: true
      t.string     :password_digest, null: false
      t.string     :timezone, null: false
      t.jsonb      :access_details, null: false, default: {}
      t.string     :state, null: false

      t.timestamps
    end
  end
end
