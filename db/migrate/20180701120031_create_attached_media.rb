class CreateAttachedMedia < ActiveRecord::Migration[5.1]
  def change
    create_table :attached_media, id: false do |t|
      t.references :media, foreign_key: true, null: false
      t.references :attachable, polymorphic: true, index: { name: :index_attached_media_on_attachable }, null: false
      t.string     :attached_for, null: false

      t.timestamps
      t.index [ :media_id, :attachable_type, :attachable_id ], name: :index_attached_media_on_unique_key, unique: true
    end
  end
end
