class CreateMessages < ActiveRecord::Migration[5.1]
  def change
    create_table :messages do |t|
      t.references :sent_by, foreign_key: { to_table: :users }, null: false
      t.string     :type, null: false
      t.text       :content, null: false

      t.timestamps
    end
  end
end
