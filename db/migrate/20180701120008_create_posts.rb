class CreatePosts < ActiveRecord::Migration[5.1]
  def change
    create_table :posts do |t|
      t.references :created_by, polymorphic: true
      t.references :managed_by, foreign_key: { to_table: :partners }, index: true, null: false
      t.string     :external_id, null: false
      t.string     :post_type, null: false
      t.jsonb      :title, null: false, default: {}
      t.jsonb      :note, null: true
      t.boolean    :is_masked, null: false
      t.boolean    :is_emoji_blocked, null: false
      t.boolean    :is_comment_blocked, null: false
      t.boolean    :is_capture_blocked, null: false
      t.jsonb      :permitted_emojis, null: false, default: []
      t.jsonb      :widget_details, null: false, default: {}
      t.jsonb      :media_details, null: false, default: {}
      t.jsonb      :interaction_details, null: false, default: {}
      t.jsonb      :score_details, null: false, default: {}
      t.boolean    :is_ad_disabled, null: false
      t.boolean    :is_running_ad, null: false
      t.boolean    :is_counting_rpe, null: false
      t.string     :base_url, null: false
      t.string     :state, null: false

      t.timestamps
    end
  end
end
