class CreateAdminProfiles < ActiveRecord::Migration[5.1]
  def change
    create_table :admin_profiles do |t|
      t.references :admin, null: false, foreign_key: true
      t.string     :first_name, null: true
      t.string     :last_name, null: true
      t.string     :nickname, null: false
      t.string     :gender, null: true
      t.jsonb      :phones, null: false, default: []
      t.jsonb      :emails, null: false, default: []
      t.jsonb      :role_details, null: false, default: {}

      t.timestamps
    end
  end
end
