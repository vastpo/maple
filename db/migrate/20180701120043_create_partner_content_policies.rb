class CreatePartnerContentPolicies < ActiveRecord::Migration[5.1]
  def change
    create_table :partner_content_policies do |t|
      t.references :partner, foreign_key: true, null: false
      t.boolean    :is_allowing_post, null: false, default: true
      t.boolean    :is_allowing_series, null: false, default: true
      t.boolean    :is_allowing_widget, null: false, default: true
      t.boolean    :is_allowing_live, null: false, default: true
      t.boolean    :is_allowing_ad, null: false, default: true
      t.boolean    :is_post_ad_compulsory, null: false, default: false
      t.boolean    :is_series_ad_compulsory, null: false, default: false
      t.boolean    :is_widget_ad_compulsory, null: false, default: false
      t.boolean    :is_live_ad_compulsory, null: false, default: false
      t.boolean    :is_review_required, null: false, default: false
      t.jsonb      :default_delivery_locations, null: false, default: {}
      t.integer    :post_limit_per_day, null: false, default: 10
      t.integer    :post_limit_per_month, null: false, default: 300
      t.integer    :series_limit_per_day, null: false, default: 10
      t.integer    :series_limit_per_month, null: false, default: 30
      t.integer    :widget_limit_per_day, null: false, default: 10
      t.integer    :widget_limit_per_month, null: false, default: 100
      t.integer    :video_max_sec, null: false, default: 1000
      t.integer    :video_max_kb, null: false, default: 500000
      t.integer    :image_max_kb, null: false, default: 5000
      t.boolean    :is_active, null: false
      t.text       :remarks, null: true

      t.timestamps
    end
  end
end
