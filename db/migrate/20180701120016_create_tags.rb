class CreateTags < ActiveRecord::Migration[5.1]
  def change
    create_table :tags do |t|
      t.string  :external_id, null: false
      t.string  :title, null: false
      t.jsonb   :color_scheme, null: false, default: {}
      t.jsonb   :interaction_details, null: false, default: {}
      t.jsonb   :score_details, null: false, default: {}
      t.string  :base_url, null: false
      t.boolean :is_active, null: false

      t.timestamps
    end
  end
end
