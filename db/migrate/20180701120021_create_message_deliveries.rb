class CreateMessageDeliveries < ActiveRecord::Migration[5.1]
  def change
    create_table :message_deliveries do |t|
      t.references :message, foreign_key: true, null: false
      t.references :recipient, foreign_key: { to_table: :users }, null: false
      t.datetime   :opened_at, null: true

      t.timestamps
    end
  end
end
