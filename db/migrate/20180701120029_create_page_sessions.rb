class CreatePageSessions < ActiveRecord::Migration[5.1]
  def change
    create_table :page_sessions do |t|
      t.references :user, foreign_key: true, null: true
      t.references :device, foreign_key: { primary_key: :device_id }, null: true, type: :string
      t.string     :system_name, null: false
      t.string     :page, null: false
      t.datetime   :start_at, null: false
      t.datetime   :end_at, null: true
      t.string     :source_page, null: true
      t.string     :source_medium, null: true
      t.string     :source_element, null: true
      t.string     :source_terms, null: true
      t.string     :search_terms, null: true
      t.jsonb      :component_details, null: false, default: {}
      t.string     :rule, null: false

      t.timestamps
    end
  end
end
