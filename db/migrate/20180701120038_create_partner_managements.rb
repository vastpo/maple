class CreatePartnerManagements < ActiveRecord::Migration[5.1]
  def change
    create_table :partner_managements do |t|
      t.references :admin, foreign_key: true, index: true, null: false
      t.references :partner, foreign_key: true, index: true, null: false
      t.boolean    :is_active, null: false
      t.text       :remarks, null: true

      t.timestamps
    end
  end
end
