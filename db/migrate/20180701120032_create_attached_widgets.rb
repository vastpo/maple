class CreateAttachedWidgets < ActiveRecord::Migration[5.1]
  def change
    create_table :attached_widgets do |t|
      t.references :post, foreign_key: true, null: false
      t.references :widget, foreign_key: true, null: false
      t.datetime   :start_at, null: false
      t.datetime   :end_at, null: false
      t.boolean    :is_active, null: false

      t.timestamps
    end
  end
end
