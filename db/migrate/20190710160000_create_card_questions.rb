class CreateCardQuestions < ActiveRecord::Migration[5.2]
  def change
    create_table :card_questions do |t|
      t.references :card, foreign_key: true, null: false
      t.string     :question, null: false
      t.string     :response_type, null: false
      t.boolean    :is_required, null: false

      t.timestamps
    end
  end
end
