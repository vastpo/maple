class CreateColumns < ActiveRecord::Migration[5.1]
  def change
    create_table :columns do |t|
      t.references :created_by, polymorphic: true
      t.string     :external_id, null: false
      t.jsonb      :title, null: false, default: {}
      t.jsonb      :short_note, null: true
      t.jsonb      :long_note, null: true
      t.jsonb      :color_scheme, null: false, default: {}
      t.jsonb      :media_details, null: false, default: {}
      t.jsonb      :interaction_details, null: false, default: {}
      t.jsonb      :score_details, null: false, default: {}
      t.string     :base_url, null: false
      t.string     :state, null: false

      t.timestamps
    end
  end
end
