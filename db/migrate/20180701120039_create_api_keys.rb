class CreateAPIKeys < ActiveRecord::Migration[5.1]
  def change
    create_table :api_keys do |t|
      t.references :owner, polymorphic: true, null: false
      t.string     :access_key, null: true
      t.string     :secret_key, null: true

      t.timestamps
    end
  end
end
