class CreatePartnerProfiles < ActiveRecord::Migration[5.1]
  def change
    create_table :partner_profiles do |t|
      t.references :partner, foreign_key: true, null: false
      t.string     :external_id, null: false
      t.jsonb      :display_name, null: false, default: {}
      t.jsonb      :short_note, null: false, default: {}
      t.jsonb      :long_note, null: false, default: {}
      t.jsonb      :links, null: false, default: {}
      t.boolean    :is_badged, null: false, default: false
      t.jsonb      :media_details, null: false, default: {}
      t.jsonb      :interaction_details, null: false, default: {}
      t.jsonb      :score_details, null: false, default: {}
      t.boolean    :is_ad_disabled, null: false
      t.boolean    :is_running_ad, null: false
      t.string     :base_url, null: false
      t.string     :state, null: false

      t.timestamps
    end
  end
end
