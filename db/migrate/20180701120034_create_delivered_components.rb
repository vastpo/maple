class CreateDeliveredComponents < ActiveRecord::Migration[5.1]
  def change
    create_table :delivered_components do |t|
      t.references :page_session, foreign_key: true, null: false
      t.string     :operating_system, null: false
      t.string     :page_type, null: false
      t.string     :presentation_type, null: false
      t.references :user, foreign_key: false, null: true
      t.references :device, foreign_key: false, null: true
      t.references :component, polymorphic: true, null: false
      t.boolean    :is_clicked, null: false, default: false

      t.timestamps
    end
  end
end
