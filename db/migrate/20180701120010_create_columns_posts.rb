class CreateColumnsPosts < ActiveRecord::Migration[5.1]
  def change
    create_table :columns_posts, id: false do |t|
      t.references :column, foreign_key: true, null: false
      t.references :post, foreign_key: true, null: false
      t.boolean    :is_active, null: false, default: true

      t.timestamps

      t.index      [ :column_id, :post_id ], unique: true
      t.index      [ :post_id, :column_id ], unique: true
    end
  end
end
