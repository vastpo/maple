class CreateWidgets < ActiveRecord::Migration[5.1]
  def change
    create_table :widgets do |t|
      t.references :created_by, polymorphic: true, null: true
      t.references :managed_by, foreign_key: { to_table: :partners }, null: false
      t.string     :external_id, null: false
      t.string     :widget_type, null: false
      t.jsonb      :title, null: false, default: {}
      t.jsonb      :headline, null: false, default: {}
      t.jsonb      :conversion_text, null: false, default: {}
      t.string     :directory_type, null: true
      t.string     :directory, null: true
      t.jsonb      :color_scheme, null: false, default: {}
      t.jsonb      :media_details, null: false, default: {}
      t.jsonb      :interaction_details, null: false, default: {}
      t.jsonb      :score_details, null: false, default: {}
      t.boolean    :is_ad_disabled, null: false
      t.boolean    :is_running_ad, null: false
      t.string     :state, null: false

      t.timestamps
    end
  end
end
