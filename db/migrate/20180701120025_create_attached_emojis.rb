class CreateAttachedEmojis < ActiveRecord::Migration[5.1]
  def change
    create_table :attached_emojis, id: false do |t|
      t.references :user, foreign_key: true, null: false
      t.references :post, foreign_key: true, null: false
      t.string     :emoji_code, null: false
      t.boolean    :is_countable, null: false

      t.timestamps
    end
  end
end
