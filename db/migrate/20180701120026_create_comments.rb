class CreateComments < ActiveRecord::Migration[5.1]
  def change
    create_table :comments do |t|
      t.references :created_by, polymorphic: true
      t.references :post, foreign_key: true, null: false
      t.references :parent, foreign_key: { to_table: :comments }, null: true
      t.string     :external_id, null: false 
      t.text       :content, null: false
      t.jsonb      :score_details, null: false, default: {}
      t.boolean    :is_masked, null: false, default: false
      t.boolean    :is_hidden, null: false, default: false

      t.timestamps
    end
  end
end
