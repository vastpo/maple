class CreateImages < ActiveRecord::Migration[5.1]
  def change
    create_table :images do |t|
      t.boolean :is_grayscale, null: true
      t.boolean :is_line_drawing, null: true

      t.timestamps
    end
  end
end
