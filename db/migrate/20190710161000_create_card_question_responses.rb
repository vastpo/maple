class CreateCardQuestionResponses < ActiveRecord::Migration[5.2]
  def change
    create_table :card_question_responses do |t|
      t.references :question, foreign_key: { to_table: :card_questions }, null: false
      t.references :user, foreign_key: true, null: true
      t.references :device, foreign_key: { primary_key: :device_id }, null: false, type: :string
      t.jsonb      :response, null: false

      t.timestamps

      t.index      [ :question_id, :user_id, :device_id ], name: :index_card_question_responses_on_unique_key, unique: true
    end
  end
end
