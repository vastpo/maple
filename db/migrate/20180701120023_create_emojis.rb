class CreateEmojis < ActiveRecord::Migration[5.1]
  def change
    create_table :emojis, id: false do |t|
      t.string  :group, null: false
      t.string  :subgroup, null: false
      t.string  :code, null: false, primary_key: true
      t.string  :status, null: false
      t.string  :emoji, null: false, unique: true
      t.string  :name, null: false
      t.boolean :is_active, null: false, default: true
    end
  end
end
