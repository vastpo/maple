class CreateEngagements < ActiveRecord::Migration[5.1]
  def change
    create_table :engagements do |t|
      t.references :user, foreign_key: true, null: true
      t.references :device, foreign_key: { primary_key: :device_id }, null: true, type: :string
      t.references :page_session, foreign_key: true, null: false
      t.references :component, polymorphic: true, null: false
      t.references :media, foreign_key: true, null: true
      t.jsonb      :media_behaviors, null: false, default: {}
      t.jsonb      :comment_behaviors, null: false, default: {}
      t.jsonb      :emoji_behaviors, null: false, default: {}
      t.jsonb      :thunder_behaviors, null: false, default: {}
      t.integer    :valuable_count, null: true
      t.jsonb      :rpe_claim_details, null: false, default: {}
      t.boolean    :is_rpe_claimable, null: false
      t.boolean    :is_rpe_claimed, null: false, default: true

      t.timestamps
    end
  end
end
