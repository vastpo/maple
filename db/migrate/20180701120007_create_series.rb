class CreateSeries < ActiveRecord::Migration[5.1]
  def change
    create_table :series do |t|
      t.references :created_by, polymorphic: true
      t.references :parent, foreign_key: { to_table: :series }, null: true
      t.jsonb      :hierarchy, null: true
      t.string     :external_id, null: false
      t.jsonb      :title, null: false
      t.jsonb      :short_note, null: true
      t.jsonb      :long_note, null: true
      t.jsonb      :color_scheme, null: false, default: {}
      t.jsonb      :media_details, null: false, default: {}
      t.jsonb      :interaction_details, null: false, default: {}
      t.jsonb      :score_details, null: false, default: {}
      t.boolean    :is_ad_disabled, null: false
      t.boolean    :is_running_ad, null: false
      t.string     :base_url, null: false
      t.string     :state, null: false

      t.timestamps
    end
  end
end
