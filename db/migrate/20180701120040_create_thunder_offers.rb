class CreateThunderOffers < ActiveRecord::Migration[5.1]
  def change
    create_table :thunder_offers do |t|
      t.references :created_by, polymorphic: true, null: false
      t.references :partner, foreign_key: true, null: false
      t.references :post, foreign_key: true, null: true
      t.string     :external_id, null: false
      t.jsonb      :note, default: {}, null: false
      t.boolean    :is_stripe_payout, null: false
      t.string     :destination_account, null: true
      t.jsonb      :amount, default: {}, null: false
      t.jsonb      :fee, default: {}, null: false
      t.boolean    :is_active, null: false

      t.timestamps
    end
  end
end
