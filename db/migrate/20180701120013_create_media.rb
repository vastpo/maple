class CreateMedia < ActiveRecord::Migration[5.1]
  def change
    create_table :media do |t|
      t.actable as: :media, index: { name: :index_media_on_media }
      t.references :managed_by, foreign_key: { to_table: :partners }, null: true
      t.string     :external_id, null: false
      t.string     :media_format, null: false
      t.integer    :width, null: false
      t.integer    :height, null: false
      t.jsonb      :urls, null: false, default: {}
      t.jsonb      :exclusivity, null: true
      t.jsonb      :transferability, null: true
      t.jsonb      :sub_licensibility, null: true
      t.jsonb      :royalty_terms, null: true
      t.jsonb      :territorialities, null: true
      t.text       :remarks, null: true

      t.timestamps
    end
  end
end
