class CreatePostsTags < ActiveRecord::Migration[5.1]
  def change
    create_table :posts_tags, id: false do |t|
      t.references :post, foreign_key: true, null: false
      t.references :tag, foreign_key: true, null: false
      t.boolean    :is_active, null: false, default: true

      t.timestamps

      t.index      [ :post_id, :tag_id ], name: :index_posts_tags_on_unique_key, unique: true
    end
  end
end
