class CreatePartnerAccess < ActiveRecord::Migration[5.1]
  def change
    create_table :partner_access do |t|
      t.references :partner_admin, foreign_key: true, null: false
      t.references :partner, foreign_key: true, null: false
      t.references :invitation, foreign_key: { to_table: :partner_access_invitations }, null: true
      t.string     :display_name, null: false
      t.jsonb      :roles, null: false, default: [:admin]
      t.jsonb      :location_accesses, null: false, default: {}
      t.jsonb      :phones, null: true
      t.jsonb      :emails, null: true
      t.integer    :sort_order, null: false, default: 0
      t.boolean    :is_active, null: false, default: true
      t.boolean    :is_default, null: false, default: false

      t.timestamps
    end
  end
end
