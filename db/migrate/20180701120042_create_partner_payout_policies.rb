class CreatePartnerPayoutPolicies < ActiveRecord::Migration[5.1]
  def change
    create_table :partner_payout_policies do |t|
      t.references :partner, foreign_key: true, null: false
      t.jsonb      :fee_rate, null: false, default: {}
      t.jsonb      :fee_fix, null: false, default: {}
      t.string     :currency, null: false
      t.decimal    :rpe, precision: 6, scale: 3, null: true
      t.boolean    :is_active, null: false
      t.text       :remarks, null: true

      t.timestamps
    end
  end
end
