class CreateCaptures < ActiveRecord::Migration[5.1]
  def change
    create_table :captures do |t|
      t.references :message, foreign_key: true, null: true
      t.references :video, foreign_key: true, null: false
      t.decimal    :start_at, precision: 8, scale: 2, null: false
      t.decimal    :end_at, precision: 8, scale: 2, null: false

      t.timestamps
    end
  end
end
