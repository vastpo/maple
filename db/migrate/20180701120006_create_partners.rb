class CreatePartners < ActiveRecord::Migration[5.1]
  def change
    create_table :partners do |t|
      t.string :external_id, null: false, unique: true
      t.string :login, null: false, unique: true
      t.string :password_digest, null: false
      t.jsonb  :display_name, null: false, unique: false
      t.string :preferred_language, null: false
      t.string :preferred_currency, null: false
      t.string :stripe_account_id, null: true
      t.string :state, null: false

      t.timestamps
    end
  end
end
