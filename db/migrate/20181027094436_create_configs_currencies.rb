class CreateConfigsCurrencies < ActiveRecord::Migration[5.1]
  def change
    create_table :configs_currencies, id: false do |t|
      t.string  :iso_code, primary_key: true
      t.boolean :is_enabled, null: false
    end
  end
end
