class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string   :login,               null: false, unique: true
      t.string   :password_digest,        null: false
      t.string   :display_name,           null: false
      t.string   :gender,                 null: true
      t.integer  :dob_year,               null: true
      t.integer  :dob_month,              null: true
      t.integer  :dob_day,                null: true
      t.integer  :timezone,               null: false
      t.string   :location,               null: true
      t.string   :phone,                  null: true
      t.boolean  :phone_verified_at,      null: true
      t.string   :email,                  null: true
      t.boolean  :email_verified_at,      null: true
      t.jsonb    :connected_profiles,     null: false, default: {}
      t.string   :preferred_currency,     null: false
      t.boolean  :is_hd_preferred,        null: false, default: true
      t.boolean  :is_video_autoplay,      null: false, default: false
      t.jsonb    :partner_admin_details,  null: false, default: {}
      t.string   :state,                  null: false

      t.timestamps
    end
  end
end
