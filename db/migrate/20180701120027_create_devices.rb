class CreateDevices < ActiveRecord::Migration[5.1]
  def change
    create_table :devices, id: false do |t|
      t.string :device_id, primary_key: true
      t.string :operating_system, null: false
      t.string :token, null: false
      t.string :country, null: false
      t.string :service, null: false

      t.timestamps
    end
  end
end
