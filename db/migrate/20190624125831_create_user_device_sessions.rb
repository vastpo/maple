class CreateUserDeviceSessions < ActiveRecord::Migration[5.2]
  def change
    create_table :user_device_sessions, id: false do |t|
      t.references :user, foreign_key: true, null: true
      t.references :device, foreign_key: { primary_key: :device_id }, null: false, type: :string
      t.datetime   :signin_at, null: false
      t.datetime   :signout_at, null: true

      t.index      [ :user_id, :device_id, :signin_at ], name: :index_user_device_sessions_on_unique_key, unique: true
    end
  end
end
