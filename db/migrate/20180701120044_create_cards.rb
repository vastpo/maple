class CreateCards < ActiveRecord::Migration[5.1]
  def change
    create_table :cards do |t|
      t.references :created_by, polymorphic: true, null: true
      t.references :managed_by, foreign_key: { to_table: :partners }, null: false
      t.references :post, foreign_key: true, null: true
      t.string     :external_id, null: false
      t.jsonb      :accesible_locations, null: false, default: {}
      t.jsonb      :tag_text, null: false, default: {}
      t.jsonb      :conversion_text, null: false, default: {}
      t.jsonb      :start_note, null: false, default: {}
      t.jsonb      :end_note, null: false, default: {}
      t.jsonb      :color_scheme, null: false, default: {}
      t.jsonb      :interaction_details, null: false, default: {}
      t.boolean    :is_requiring_login, null: false
      t.boolean    :is_requiring_verification, null: false
      t.boolean    :is_displaying_result, null: false
      t.string     :state, null: false

      t.timestamps
    end
  end
end
