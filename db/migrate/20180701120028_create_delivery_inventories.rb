class CreateDeliveryInventories < ActiveRecord::Migration[5.1]
  def change
    create_table :delivery_inventories do |t|
      t.references :user, foreign_key: true, null: true
      t.references :device, foreign_key: { primary_key: :device_id }, null: true, type: :string
      t.string     :operating_system, null: false
      t.string     :page_type, null: false
      t.string     :rule, null: false
      t.jsonb      :component_details, null: false, default: {}
      t.boolean    :is_delivered, null: false
      t.boolean    :is_expired, null: false

      t.timestamps
    end
  end
end
