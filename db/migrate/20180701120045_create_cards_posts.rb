class CreateCardsPosts < ActiveRecord::Migration[5.1]
  def change
    create_table :cards_posts, id: false do |t|
      t.references :card, foreign_key: true, null: false
      t.references :post, foreign_key: true, null: false
      t.boolean    :is_active, null: false, default: true

      t.timestamps

      t.index      [ :card_id, :post_id ], name: :index_cards_posts_on_unique_key, unique: true
    end
  end
end
