class CreateUserDevicePushNotifications < ActiveRecord::Migration[5.2]
  def change
    create_table :user_device_push_notifications, id: false do |t|
      t.references :user, foreign_key: true, null: true
      t.references :device, foreign_key: { primary_key: :device_id }, null: false, type: :string
      t.string     :service, null: false
      t.boolean    :is_active, null: false

      t.index      [ :user_id, :device_id, :service ], name: :index_user_device_push_notifications_on_unique_key, unique: true
    end
  end
end
