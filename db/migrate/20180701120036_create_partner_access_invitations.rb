class CreatePartnerAccessInvitations < ActiveRecord::Migration[5.1]
  def change
    create_table :partner_access_invitations do |t|
      t.references :invited_by, polymorphic: true, null: false, index: { name: :index_partner_access_invitations_on_invited_by }
      t.references :partner, foreign_key: true, null: false
      t.string     :email, null: false
      t.jsonb      :roles, null: false
      t.string     :token, null: false, unique: true
      t.datetime   :expired_at, null: false
      t.string     :state, null: false

      t.timestamps
    end
  end
end
