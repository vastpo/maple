# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_07_10_161000) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "admin_profiles", force: :cascade do |t|
    t.bigint "admin_id", null: false
    t.string "first_name"
    t.string "last_name"
    t.string "nickname", null: false
    t.string "gender"
    t.jsonb "phones", default: [], null: false
    t.jsonb "emails", default: [], null: false
    t.jsonb "role_details", default: {}, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["admin_id"], name: "index_admin_profiles_on_admin_id"
  end

  create_table "admins", force: :cascade do |t|
    t.string "login", default: "", null: false
    t.string "password_digest", default: "", null: false
    t.string "display_name", default: "", null: false
    t.jsonb "roles", default: ["viewer"], null: false
    t.string "state", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.integer "failed_attempts", default: 0, null: false
    t.string "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["confirmation_token"], name: "index_admins_on_confirmation_token", unique: true
    t.index ["login"], name: "index_admins_on_login", unique: true
    t.index ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true
    t.index ["unlock_token"], name: "index_admins_on_unlock_token", unique: true
  end

  create_table "api_keys", force: :cascade do |t|
    t.string "owner_type", null: false
    t.bigint "owner_id", null: false
    t.string "access_key"
    t.string "secret_key"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["owner_type", "owner_id"], name: "index_api_keys_on_owner_type_and_owner_id"
  end

  create_table "attached_emojis", id: false, force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "post_id", null: false
    t.string "emoji_code", null: false
    t.boolean "is_countable", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["post_id"], name: "index_attached_emojis_on_post_id"
    t.index ["user_id"], name: "index_attached_emojis_on_user_id"
  end

  create_table "attached_media", id: false, force: :cascade do |t|
    t.bigint "media_id", null: false
    t.string "attachable_type", null: false
    t.bigint "attachable_id", null: false
    t.string "attached_for", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["attachable_type", "attachable_id"], name: "index_attached_media_on_attachable"
    t.index ["media_id", "attachable_type", "attachable_id"], name: "index_attached_media_on_unique_key", unique: true
    t.index ["media_id"], name: "index_attached_media_on_media_id"
  end

  create_table "attached_widgets", force: :cascade do |t|
    t.bigint "post_id", null: false
    t.bigint "widget_id", null: false
    t.datetime "start_at", null: false
    t.datetime "end_at", null: false
    t.boolean "is_active", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["post_id"], name: "index_attached_widgets_on_post_id"
    t.index ["widget_id"], name: "index_attached_widgets_on_widget_id"
  end

  create_table "captures", force: :cascade do |t|
    t.bigint "message_id"
    t.bigint "video_id", null: false
    t.decimal "start_at", precision: 8, scale: 2, null: false
    t.decimal "end_at", precision: 8, scale: 2, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["message_id"], name: "index_captures_on_message_id"
    t.index ["video_id"], name: "index_captures_on_video_id"
  end

  create_table "card_question_responses", force: :cascade do |t|
    t.bigint "question_id", null: false
    t.bigint "user_id"
    t.string "device_id", null: false
    t.jsonb "response", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["device_id"], name: "index_card_question_responses_on_device_id"
    t.index ["question_id", "user_id", "device_id"], name: "index_card_question_responses_on_unique_key", unique: true
    t.index ["question_id"], name: "index_card_question_responses_on_question_id"
    t.index ["user_id"], name: "index_card_question_responses_on_user_id"
  end

  create_table "card_questions", force: :cascade do |t|
    t.bigint "card_id", null: false
    t.string "question", null: false
    t.string "response_type", null: false
    t.boolean "is_required", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["card_id"], name: "index_card_questions_on_card_id"
  end

  create_table "cards", force: :cascade do |t|
    t.string "created_by_type"
    t.bigint "created_by_id"
    t.bigint "managed_by_id", null: false
    t.bigint "post_id"
    t.string "external_id", null: false
    t.jsonb "accesible_locations", default: {}, null: false
    t.jsonb "tag_text", default: {}, null: false
    t.jsonb "conversion_text", default: {}, null: false
    t.jsonb "start_note", default: {}, null: false
    t.jsonb "end_note", default: {}, null: false
    t.jsonb "color_scheme", default: {}, null: false
    t.jsonb "interaction_details", default: {}, null: false
    t.boolean "is_requiring_login", null: false
    t.boolean "is_requiring_verification", null: false
    t.boolean "is_displaying_result", null: false
    t.string "state", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["created_by_type", "created_by_id"], name: "index_cards_on_created_by_type_and_created_by_id"
    t.index ["managed_by_id"], name: "index_cards_on_managed_by_id"
    t.index ["post_id"], name: "index_cards_on_post_id"
  end

  create_table "cards_posts", id: false, force: :cascade do |t|
    t.bigint "card_id", null: false
    t.bigint "post_id", null: false
    t.boolean "is_active", default: true, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["card_id", "post_id"], name: "index_cards_posts_on_unique_key", unique: true
    t.index ["card_id"], name: "index_cards_posts_on_card_id"
    t.index ["post_id"], name: "index_cards_posts_on_post_id"
  end

  create_table "columns", force: :cascade do |t|
    t.string "created_by_type"
    t.bigint "created_by_id"
    t.string "external_id", null: false
    t.jsonb "title", default: {}, null: false
    t.jsonb "short_note"
    t.jsonb "long_note"
    t.jsonb "color_scheme", default: {}, null: false
    t.jsonb "media_details", default: {}, null: false
    t.jsonb "interaction_details", default: {}, null: false
    t.jsonb "score_details", default: {}, null: false
    t.string "base_url", null: false
    t.string "state", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["created_by_type", "created_by_id"], name: "index_columns_on_created_by_type_and_created_by_id"
  end

  create_table "columns_posts", id: false, force: :cascade do |t|
    t.bigint "column_id", null: false
    t.bigint "post_id", null: false
    t.boolean "is_active", default: true, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["column_id", "post_id"], name: "index_columns_posts_on_column_id_and_post_id", unique: true
    t.index ["column_id"], name: "index_columns_posts_on_column_id"
    t.index ["post_id", "column_id"], name: "index_columns_posts_on_post_id_and_column_id", unique: true
    t.index ["post_id"], name: "index_columns_posts_on_post_id"
  end

  create_table "comments", force: :cascade do |t|
    t.string "created_by_type"
    t.bigint "created_by_id"
    t.bigint "post_id", null: false
    t.bigint "parent_id"
    t.string "external_id", null: false
    t.text "content", null: false
    t.jsonb "score_details", default: {}, null: false
    t.boolean "is_masked", default: false, null: false
    t.boolean "is_hidden", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["created_by_type", "created_by_id"], name: "index_comments_on_created_by_type_and_created_by_id"
    t.index ["parent_id"], name: "index_comments_on_parent_id"
    t.index ["post_id"], name: "index_comments_on_post_id"
  end

  create_table "configs_currencies", primary_key: "iso_code", id: :string, force: :cascade do |t|
    t.boolean "is_enabled", null: false
  end

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer "priority", default: 0, null: false
    t.integer "attempts", default: 0, null: false
    t.text "handler", null: false
    t.text "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string "locked_by"
    t.string "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["priority", "run_at"], name: "delayed_jobs_priority"
  end

  create_table "delivered_components", force: :cascade do |t|
    t.bigint "page_session_id", null: false
    t.string "operating_system", null: false
    t.string "page_type", null: false
    t.string "presentation_type", null: false
    t.bigint "user_id"
    t.bigint "device_id"
    t.string "component_type", null: false
    t.bigint "component_id", null: false
    t.boolean "is_clicked", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["component_type", "component_id"], name: "index_delivered_components_on_component_type_and_component_id"
    t.index ["device_id"], name: "index_delivered_components_on_device_id"
    t.index ["page_session_id"], name: "index_delivered_components_on_page_session_id"
    t.index ["user_id"], name: "index_delivered_components_on_user_id"
  end

  create_table "delivery_inventories", force: :cascade do |t|
    t.bigint "user_id"
    t.string "device_id"
    t.string "operating_system", null: false
    t.string "page_type", null: false
    t.string "rule", null: false
    t.jsonb "component_details", default: {}, null: false
    t.boolean "is_delivered", null: false
    t.boolean "is_expired", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["device_id"], name: "index_delivery_inventories_on_device_id"
    t.index ["user_id"], name: "index_delivery_inventories_on_user_id"
  end

  create_table "delivery_locations", force: :cascade do |t|
    t.string "deliverable_type"
    t.bigint "deliverable_id"
    t.string "location", null: false
    t.boolean "is_active", default: true, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["deliverable_type", "deliverable_id"], name: "index_delivery_locations_on_deliverable_type_and_deliverable_id"
  end

  create_table "devices", primary_key: "device_id", id: :string, force: :cascade do |t|
    t.string "operating_system", null: false
    t.string "token", null: false
    t.string "country", null: false
    t.string "service", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "emojis", primary_key: "code", id: :string, force: :cascade do |t|
    t.string "group", null: false
    t.string "subgroup", null: false
    t.string "status", null: false
    t.string "emoji", null: false
    t.string "name", null: false
    t.boolean "is_active", default: true, null: false
  end

  create_table "engagements", force: :cascade do |t|
    t.bigint "user_id"
    t.string "device_id"
    t.bigint "page_session_id", null: false
    t.string "component_type", null: false
    t.bigint "component_id", null: false
    t.bigint "media_id"
    t.jsonb "media_behaviors", default: {}, null: false
    t.jsonb "comment_behaviors", default: {}, null: false
    t.jsonb "emoji_behaviors", default: {}, null: false
    t.jsonb "thunder_behaviors", default: {}, null: false
    t.integer "valuable_count"
    t.jsonb "rpe_claim_details", default: {}, null: false
    t.boolean "is_rpe_claimable", null: false
    t.boolean "is_rpe_claimed", default: true, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["component_type", "component_id"], name: "index_engagements_on_component_type_and_component_id"
    t.index ["device_id"], name: "index_engagements_on_device_id"
    t.index ["media_id"], name: "index_engagements_on_media_id"
    t.index ["page_session_id"], name: "index_engagements_on_page_session_id"
    t.index ["user_id"], name: "index_engagements_on_user_id"
  end

  create_table "images", force: :cascade do |t|
    t.boolean "is_grayscale"
    t.boolean "is_line_drawing"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "media", force: :cascade do |t|
    t.string "media_type"
    t.bigint "media_id"
    t.bigint "managed_by_id"
    t.string "external_id", null: false
    t.string "media_format", null: false
    t.integer "width", null: false
    t.integer "height", null: false
    t.jsonb "urls", default: {}, null: false
    t.jsonb "exclusivity"
    t.jsonb "transferability"
    t.jsonb "sub_licensibility"
    t.jsonb "royalty_terms"
    t.jsonb "territorialities"
    t.text "remarks"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["managed_by_id"], name: "index_media_on_managed_by_id"
    t.index ["media_type", "media_id"], name: "index_media_on_media"
  end

  create_table "message_deliveries", force: :cascade do |t|
    t.bigint "message_id", null: false
    t.bigint "recipient_id", null: false
    t.datetime "opened_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["message_id"], name: "index_message_deliveries_on_message_id"
    t.index ["recipient_id"], name: "index_message_deliveries_on_recipient_id"
  end

  create_table "messages", force: :cascade do |t|
    t.bigint "sent_by_id", null: false
    t.string "type", null: false
    t.text "content", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["sent_by_id"], name: "index_messages_on_sent_by_id"
  end

  create_table "operation_tracks", force: :cascade do |t|
    t.string "item_type", null: false
    t.integer "item_id", null: false
    t.string "event", null: false
    t.string "whodunnit"
    t.text "object"
    t.datetime "created_at"
    t.index ["item_type", "item_id"], name: "index_operation_tracks_on_item_type_and_item_id"
  end

  create_table "page_sessions", force: :cascade do |t|
    t.bigint "user_id"
    t.string "device_id"
    t.string "system_name", null: false
    t.string "page", null: false
    t.datetime "start_at", null: false
    t.datetime "end_at"
    t.string "source_page"
    t.string "source_medium"
    t.string "source_element"
    t.string "source_terms"
    t.string "search_terms"
    t.jsonb "component_details", default: {}, null: false
    t.string "rule", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["device_id"], name: "index_page_sessions_on_device_id"
    t.index ["user_id"], name: "index_page_sessions_on_user_id"
  end

  create_table "partner_access", force: :cascade do |t|
    t.bigint "partner_admin_id", null: false
    t.bigint "partner_id", null: false
    t.bigint "invitation_id"
    t.string "display_name", null: false
    t.jsonb "roles", default: ["admin"], null: false
    t.jsonb "location_accesses", default: {}, null: false
    t.jsonb "phones"
    t.jsonb "emails"
    t.integer "sort_order", default: 0, null: false
    t.boolean "is_active", default: true, null: false
    t.boolean "is_default", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["invitation_id"], name: "index_partner_access_on_invitation_id"
    t.index ["partner_admin_id"], name: "index_partner_access_on_partner_admin_id"
    t.index ["partner_id"], name: "index_partner_access_on_partner_id"
  end

  create_table "partner_access_invitations", force: :cascade do |t|
    t.string "invited_by_type", null: false
    t.bigint "invited_by_id", null: false
    t.bigint "partner_id", null: false
    t.string "email", null: false
    t.jsonb "roles", null: false
    t.string "token", null: false
    t.datetime "expired_at", null: false
    t.string "state", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["invited_by_type", "invited_by_id"], name: "index_partner_access_invitations_on_invited_by"
    t.index ["partner_id"], name: "index_partner_access_invitations_on_partner_id"
  end

  create_table "partner_admins", force: :cascade do |t|
    t.bigint "user_id"
    t.string "login", null: false
    t.string "password_digest", null: false
    t.string "timezone", null: false
    t.jsonb "access_details", default: {}, null: false
    t.string "state", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_partner_admins_on_user_id"
  end

  create_table "partner_content_policies", force: :cascade do |t|
    t.bigint "partner_id", null: false
    t.boolean "is_allowing_post", default: true, null: false
    t.boolean "is_allowing_series", default: true, null: false
    t.boolean "is_allowing_widget", default: true, null: false
    t.boolean "is_allowing_live", default: true, null: false
    t.boolean "is_allowing_ad", default: true, null: false
    t.boolean "is_post_ad_compulsory", default: false, null: false
    t.boolean "is_series_ad_compulsory", default: false, null: false
    t.boolean "is_widget_ad_compulsory", default: false, null: false
    t.boolean "is_live_ad_compulsory", default: false, null: false
    t.boolean "is_review_required", default: false, null: false
    t.jsonb "default_delivery_locations", default: {}, null: false
    t.integer "post_limit_per_day", default: 10, null: false
    t.integer "post_limit_per_month", default: 300, null: false
    t.integer "series_limit_per_day", default: 10, null: false
    t.integer "series_limit_per_month", default: 30, null: false
    t.integer "widget_limit_per_day", default: 10, null: false
    t.integer "widget_limit_per_month", default: 100, null: false
    t.integer "video_max_sec", default: 1000, null: false
    t.integer "video_max_kb", default: 500000, null: false
    t.integer "image_max_kb", default: 5000, null: false
    t.boolean "is_active", null: false
    t.text "remarks"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["partner_id"], name: "index_partner_content_policies_on_partner_id"
  end

  create_table "partner_managements", force: :cascade do |t|
    t.bigint "admin_id", null: false
    t.bigint "partner_id", null: false
    t.boolean "is_active", null: false
    t.text "remarks"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["admin_id"], name: "index_partner_managements_on_admin_id"
    t.index ["partner_id"], name: "index_partner_managements_on_partner_id"
  end

  create_table "partner_payout_policies", force: :cascade do |t|
    t.bigint "partner_id", null: false
    t.jsonb "fee_rate", default: {}, null: false
    t.jsonb "fee_fix", default: {}, null: false
    t.string "currency", null: false
    t.decimal "rpe", precision: 6, scale: 3
    t.boolean "is_active", null: false
    t.text "remarks"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["partner_id"], name: "index_partner_payout_policies_on_partner_id"
  end

  create_table "partner_profiles", force: :cascade do |t|
    t.bigint "partner_id", null: false
    t.string "external_id", null: false
    t.jsonb "display_name", default: {}, null: false
    t.jsonb "short_note", default: {}, null: false
    t.jsonb "long_note", default: {}, null: false
    t.jsonb "links", default: {}, null: false
    t.boolean "is_badged", default: false, null: false
    t.jsonb "media_details", default: {}, null: false
    t.jsonb "interaction_details", default: {}, null: false
    t.jsonb "score_details", default: {}, null: false
    t.boolean "is_ad_disabled", null: false
    t.boolean "is_running_ad", null: false
    t.string "base_url", null: false
    t.string "state", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["partner_id"], name: "index_partner_profiles_on_partner_id"
  end

  create_table "partners", force: :cascade do |t|
    t.string "external_id", null: false
    t.string "login", null: false
    t.string "password_digest", null: false
    t.jsonb "display_name", null: false
    t.string "preferred_language", null: false
    t.string "preferred_currency", null: false
    t.string "stripe_account_id"
    t.string "state", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "posts", force: :cascade do |t|
    t.string "created_by_type"
    t.bigint "created_by_id"
    t.bigint "managed_by_id", null: false
    t.string "external_id", null: false
    t.string "post_type", null: false
    t.jsonb "title", default: {}, null: false
    t.jsonb "note"
    t.boolean "is_masked", null: false
    t.boolean "is_emoji_blocked", null: false
    t.boolean "is_comment_blocked", null: false
    t.boolean "is_capture_blocked", null: false
    t.jsonb "permitted_emojis", default: [], null: false
    t.jsonb "widget_details", default: {}, null: false
    t.jsonb "media_details", default: {}, null: false
    t.jsonb "interaction_details", default: {}, null: false
    t.jsonb "score_details", default: {}, null: false
    t.boolean "is_ad_disabled", null: false
    t.boolean "is_running_ad", null: false
    t.boolean "is_counting_rpe", null: false
    t.string "base_url", null: false
    t.string "state", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["created_by_type", "created_by_id"], name: "index_posts_on_created_by_type_and_created_by_id"
    t.index ["managed_by_id"], name: "index_posts_on_managed_by_id"
  end

  create_table "posts_series", id: false, force: :cascade do |t|
    t.bigint "post_id", null: false
    t.bigint "series_id", null: false
    t.boolean "is_active", default: true, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["post_id", "series_id"], name: "index_posts_series_on_post_id_and_series_id", unique: true
    t.index ["post_id"], name: "index_posts_series_on_post_id"
    t.index ["series_id", "post_id"], name: "index_posts_series_on_series_id_and_post_id", unique: true
    t.index ["series_id"], name: "index_posts_series_on_series_id"
  end

  create_table "posts_tags", id: false, force: :cascade do |t|
    t.bigint "post_id", null: false
    t.bigint "tag_id", null: false
    t.boolean "is_active", default: true, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["post_id", "tag_id"], name: "index_posts_tags_on_unique_key", unique: true
    t.index ["post_id"], name: "index_posts_tags_on_post_id"
    t.index ["tag_id"], name: "index_posts_tags_on_tag_id"
  end

  create_table "rpush_apps", force: :cascade do |t|
    t.string "name", null: false
    t.string "environment"
    t.text "certificate"
    t.string "password"
    t.integer "connections", default: 1, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "type", null: false
    t.string "auth_key"
    t.string "client_id"
    t.string "client_secret"
    t.string "access_token"
    t.datetime "access_token_expiration"
    t.text "apn_key"
    t.string "apn_key_id"
    t.string "team_id"
    t.string "bundle_id"
    t.boolean "feedback_enabled", default: true
  end

  create_table "rpush_feedback", force: :cascade do |t|
    t.string "device_token"
    t.datetime "failed_at", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "app_id"
    t.index ["device_token"], name: "index_rpush_feedback_on_device_token"
  end

  create_table "rpush_notifications", force: :cascade do |t|
    t.integer "badge"
    t.string "device_token"
    t.string "sound"
    t.text "alert"
    t.text "data"
    t.integer "expiry", default: 86400
    t.boolean "delivered", default: false, null: false
    t.datetime "delivered_at"
    t.boolean "failed", default: false, null: false
    t.datetime "failed_at"
    t.integer "error_code"
    t.text "error_description"
    t.datetime "deliver_after"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "alert_is_json", default: false, null: false
    t.string "type", null: false
    t.string "collapse_key"
    t.boolean "delay_while_idle", default: false, null: false
    t.text "registration_ids"
    t.integer "app_id", null: false
    t.integer "retries", default: 0
    t.string "uri"
    t.datetime "fail_after"
    t.boolean "processing", default: false, null: false
    t.integer "priority"
    t.text "url_args"
    t.string "category"
    t.boolean "content_available", default: false, null: false
    t.text "notification"
    t.boolean "mutable_content", default: false, null: false
    t.string "external_device_id"
    t.string "thread_id"
    t.boolean "dry_run", default: false, null: false
    t.index ["delivered", "failed", "processing", "deliver_after", "created_at"], name: "index_rpush_notifications_multi", where: "((NOT delivered) AND (NOT failed))"
  end

  create_table "series", force: :cascade do |t|
    t.string "created_by_type"
    t.bigint "created_by_id"
    t.bigint "parent_id"
    t.jsonb "hierarchy"
    t.string "external_id", null: false
    t.jsonb "title", null: false
    t.jsonb "short_note"
    t.jsonb "long_note"
    t.jsonb "color_scheme", default: {}, null: false
    t.jsonb "media_details", default: {}, null: false
    t.jsonb "interaction_details", default: {}, null: false
    t.jsonb "score_details", default: {}, null: false
    t.boolean "is_ad_disabled", null: false
    t.boolean "is_running_ad", null: false
    t.string "base_url", null: false
    t.string "state", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["created_by_type", "created_by_id"], name: "index_series_on_created_by_type_and_created_by_id"
    t.index ["parent_id"], name: "index_series_on_parent_id"
  end

  create_table "series_access", force: :cascade do |t|
    t.bigint "partner_id", null: false
    t.bigint "series_id", null: false
    t.boolean "is_owner", null: false
    t.boolean "is_editing_allowed", null: false
    t.boolean "is_contribution_allowed", null: false
    t.boolean "is_active", default: true, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["partner_id", "series_id"], name: "index_series_access_on_partner_id_and_series_id", unique: true
    t.index ["partner_id"], name: "index_series_access_on_partner_id"
    t.index ["series_id"], name: "index_series_access_on_series_id"
  end

  create_table "tags", force: :cascade do |t|
    t.string "external_id", null: false
    t.string "title", null: false
    t.jsonb "color_scheme", default: {}, null: false
    t.jsonb "interaction_details", default: {}, null: false
    t.jsonb "score_details", default: {}, null: false
    t.string "base_url", null: false
    t.boolean "is_active", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "thunder_offers", force: :cascade do |t|
    t.string "created_by_type", null: false
    t.bigint "created_by_id", null: false
    t.bigint "partner_id", null: false
    t.bigint "post_id"
    t.string "external_id", null: false
    t.jsonb "note", default: {}, null: false
    t.boolean "is_stripe_payout", null: false
    t.string "destination_account"
    t.jsonb "amount", default: {}, null: false
    t.jsonb "fee", default: {}, null: false
    t.boolean "is_active", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["created_by_type", "created_by_id"], name: "index_thunder_offers_on_created_by_type_and_created_by_id"
    t.index ["partner_id"], name: "index_thunder_offers_on_partner_id"
    t.index ["post_id"], name: "index_thunder_offers_on_post_id"
  end

  create_table "thunder_transactions", force: :cascade do |t|
    t.bigint "thunder_offer_id", null: false
    t.bigint "user_id"
    t.string "stripe_customer_id"
    t.string "stripe_charge_id"
    t.string "stripe_transfer_id"
    t.boolean "is_stripe_payout", null: false
    t.string "destination_account"
    t.string "currency", null: false
    t.decimal "amount", precision: 9, scale: 3, null: false
    t.decimal "fee", precision: 8, scale: 3, null: false
    t.boolean "is_captured", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["thunder_offer_id"], name: "index_thunder_transactions_on_thunder_offer_id"
    t.index ["user_id"], name: "index_thunder_transactions_on_user_id"
  end

  create_table "user_device_push_notifications", id: false, force: :cascade do |t|
    t.bigint "user_id"
    t.string "device_id", null: false
    t.string "service", null: false
    t.boolean "is_active", null: false
    t.index ["device_id"], name: "index_user_device_push_notifications_on_device_id"
    t.index ["user_id", "device_id", "service"], name: "index_user_device_push_notifications_on_unique_key", unique: true
    t.index ["user_id"], name: "index_user_device_push_notifications_on_user_id"
  end

  create_table "user_device_sessions", id: false, force: :cascade do |t|
    t.bigint "user_id"
    t.string "device_id", null: false
    t.datetime "signin_at", null: false
    t.datetime "signout_at"
    t.index ["device_id"], name: "index_user_device_sessions_on_device_id"
    t.index ["user_id", "device_id", "signin_at"], name: "index_user_device_sessions_on_unique_key", unique: true
    t.index ["user_id"], name: "index_user_device_sessions_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "login", null: false
    t.string "password_digest", null: false
    t.string "display_name", null: false
    t.string "gender"
    t.integer "dob_year"
    t.integer "dob_month"
    t.integer "dob_day"
    t.integer "timezone", null: false
    t.string "location"
    t.string "phone"
    t.boolean "phone_verified_at"
    t.string "email"
    t.boolean "email_verified_at"
    t.jsonb "connected_profiles", default: {}, null: false
    t.string "preferred_currency", null: false
    t.boolean "is_hd_preferred", default: true, null: false
    t.boolean "is_video_autoplay", default: false, null: false
    t.jsonb "partner_admin_details", default: {}, null: false
    t.string "state", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users_devices", id: false, force: :cascade do |t|
    t.bigint "user_id", null: false
    t.string "device_id", null: false
    t.boolean "is_active", default: true, null: false
    t.index ["device_id"], name: "index_users_devices_on_device_id"
    t.index ["user_id", "device_id"], name: "index_users_devices_on_unique_key", unique: true
    t.index ["user_id"], name: "index_users_devices_on_user_id"
  end

  create_table "videos", force: :cascade do |t|
    t.integer "length", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "widgets", force: :cascade do |t|
    t.string "created_by_type"
    t.bigint "created_by_id"
    t.bigint "managed_by_id", null: false
    t.string "external_id", null: false
    t.string "widget_type", null: false
    t.jsonb "title", default: {}, null: false
    t.jsonb "headline", default: {}, null: false
    t.jsonb "conversion_text", default: {}, null: false
    t.string "directory_type"
    t.string "directory"
    t.jsonb "color_scheme", default: {}, null: false
    t.jsonb "media_details", default: {}, null: false
    t.jsonb "interaction_details", default: {}, null: false
    t.jsonb "score_details", default: {}, null: false
    t.boolean "is_ad_disabled", null: false
    t.boolean "is_running_ad", null: false
    t.string "state", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["created_by_type", "created_by_id"], name: "index_widgets_on_created_by_type_and_created_by_id"
    t.index ["managed_by_id"], name: "index_widgets_on_managed_by_id"
  end

  add_foreign_key "admin_profiles", "admins"
  add_foreign_key "attached_emojis", "posts"
  add_foreign_key "attached_emojis", "users"
  add_foreign_key "attached_media", "media"
  add_foreign_key "attached_widgets", "posts"
  add_foreign_key "attached_widgets", "widgets"
  add_foreign_key "captures", "messages"
  add_foreign_key "captures", "videos"
  add_foreign_key "card_question_responses", "card_questions", column: "question_id"
  add_foreign_key "card_question_responses", "devices", primary_key: "device_id"
  add_foreign_key "card_question_responses", "users"
  add_foreign_key "card_questions", "cards"
  add_foreign_key "cards", "partners", column: "managed_by_id"
  add_foreign_key "cards", "posts"
  add_foreign_key "cards_posts", "cards"
  add_foreign_key "cards_posts", "posts"
  add_foreign_key "columns_posts", "columns"
  add_foreign_key "columns_posts", "posts"
  add_foreign_key "comments", "comments", column: "parent_id"
  add_foreign_key "comments", "posts"
  add_foreign_key "delivered_components", "page_sessions"
  add_foreign_key "delivery_inventories", "devices", primary_key: "device_id"
  add_foreign_key "delivery_inventories", "users"
  add_foreign_key "engagements", "devices", primary_key: "device_id"
  add_foreign_key "engagements", "media"
  add_foreign_key "engagements", "page_sessions"
  add_foreign_key "engagements", "users"
  add_foreign_key "media", "partners", column: "managed_by_id"
  add_foreign_key "message_deliveries", "messages"
  add_foreign_key "message_deliveries", "users", column: "recipient_id"
  add_foreign_key "messages", "users", column: "sent_by_id"
  add_foreign_key "page_sessions", "devices", primary_key: "device_id"
  add_foreign_key "page_sessions", "users"
  add_foreign_key "partner_access", "partner_access_invitations", column: "invitation_id"
  add_foreign_key "partner_access", "partner_admins"
  add_foreign_key "partner_access", "partners"
  add_foreign_key "partner_access_invitations", "partners"
  add_foreign_key "partner_admins", "users"
  add_foreign_key "partner_content_policies", "partners"
  add_foreign_key "partner_managements", "admins"
  add_foreign_key "partner_managements", "partners"
  add_foreign_key "partner_payout_policies", "partners"
  add_foreign_key "partner_profiles", "partners"
  add_foreign_key "posts", "partners", column: "managed_by_id"
  add_foreign_key "posts_series", "posts"
  add_foreign_key "posts_series", "series"
  add_foreign_key "posts_tags", "posts"
  add_foreign_key "posts_tags", "tags"
  add_foreign_key "series", "series", column: "parent_id"
  add_foreign_key "series_access", "partners"
  add_foreign_key "series_access", "series"
  add_foreign_key "thunder_offers", "partners"
  add_foreign_key "thunder_offers", "posts"
  add_foreign_key "thunder_transactions", "thunder_offers"
  add_foreign_key "thunder_transactions", "users"
  add_foreign_key "user_device_push_notifications", "devices", primary_key: "device_id"
  add_foreign_key "user_device_push_notifications", "users"
  add_foreign_key "user_device_sessions", "devices", primary_key: "device_id"
  add_foreign_key "user_device_sessions", "users"
  add_foreign_key "users_devices", "devices", primary_key: "device_id"
  add_foreign_key "users_devices", "users"
  add_foreign_key "widgets", "partners", column: "managed_by_id"
end
