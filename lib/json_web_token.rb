class JSONWebToken
  # our secret key to encode our jwt

  class << self
    def encode(payload, secret_key)
      # set token expiration time 
      payload[:exp] = payload[:time] + ENV['JWT_EXPIRE_AFTER'].to_i
      
       # this encodes the user data(payload) with our secret key
      JWT.encode(payload, secret_key, 'HS512')
    end

    def decode(token, secret_key)
      #decodes the token to get user data (payload)
      body = JWT.decode(token, secret_key, true, { algorithm: 'HS512' })[0]
      HashWithIndifferentAccess.new body

    # raise custom error to be handled by custom handler
    rescue JWT::ExpiredSignature, JWT::VerificationError => e
      raise ExceptionHandler::ExpiredSignature, e.message
    rescue JWT::DecodeError, JWT::VerificationError => e
      raise ExceptionHandler::DecodeError, e.message
    end
  end
end
