class Integer

  include ActionView::Helpers::NumberHelper

  def numeric?
    true
  end

  def positive?
    self > 0
  end

  def negative?
    self < 0
  end

  def nonpositive?
    self.zero? || self.negative?
  end

  def nonnegative?
    self.zero? || self.positive?
  end

  def to_formatted_f
    number_with_delimiter(number_with_precision(self.to_f, precision: Settings.decimal_places))
  end

  def minus(x)
    (BigDecimal.new(self.to_s) - BigDecimal.new(x.to_s)).to_f
  end

  def humanize
    self
  end

end
