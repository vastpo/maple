class Symbol

  def titleize
    self.to_s.titleize
  end

end
