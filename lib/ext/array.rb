class Array

  def deep_symbolize_keys
    self.map do |e|
      if e.is_a? Array
        e.deep_symbolize_keys
      elsif e.is_a? Hash
        e.deep_symbolize_keys
      else
        e
      end
    end
  end

end
