#Alias the default boolean? operator of ruby for everything so all objects responds_to? :to_bool
#...then further refine it for specific classes later
class Object
  def to_bool
    return !!self
  end
end

#Lets give String a custom to_bool that is in line with the meaning for our specific program
class String
  def to_bool
    return true if self == true || self =~ (/(true|t|yes|y|1)$/i)
    return false if self == false || self.blank? || self =~ (/(false|f|no|n|0)$/i)
    raise ArgumentError.new("invalid value for Boolean: \"#{self}\"")
  end
end

class Integer
  def to_bool
    !self.zero?
  end
end

class Float
  def to_bool
    !self.zero?
  end
end

class Array
  def to_bool
    !self.empty?
  end
end

class Hash
  def to_bool
    !self.empty?
  end
end
