class Hash

  def deep_flatten
    self.each_with_object({}) do |(k, v), h|
      if v.is_a? Hash
        v.deep_flatten.map do |h_k, h_v|
          h["#{k}.#{h_k}".to_sym] = h_v
        end
      else
        h[k] = v
      end
    end
  end

end
