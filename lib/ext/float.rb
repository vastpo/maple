class Float

  include ActionView::Helpers::NumberHelper

  def numeric?
    true
  end

  def positive?
    self > 0
  end

  def negative?
    self < 0
  end

  def nonpositive?
    self.zero? || self.negative?
  end

  def nonnegative?
    self.zero? || self.positive?
  end

  def to_formatted_f
    number_with_delimiter(number_with_precision(self, precision: Settings.decimal_places))
  end

  def minus(x)
    (BigDecimal.new(self.to_s) - BigDecimal.new(x.to_s)).to_f
  end

  def humanize
    /(?<integral>\d*)\.(?<decimal>\d*)/ =~ self.to_s
    sign1, digits1, base1, exponent1 = BigDecimal.new(self.to_s).split
    sign2, digits2, base2, exponent2 = BigDecimal.new(integral.to_s).split
    return self.to_i if (digits1 == digits2 && integral.length - exponent1 == 0)
    return self
  end

end
