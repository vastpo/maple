class NilClass
  def empty?
    true
  end

  def positive?
    false
  end

  def negative?
    false
  end

  def nonpositive?
    false
  end

  def nonnegative?
    false
  end
end
