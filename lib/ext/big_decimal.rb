class BigDecimal

  include ActionView::Helpers::NumberHelper

  def to_formatted_f
    number_with_delimiter(number_with_precision(self, precision: Settings.decimal_places))
  end

end
