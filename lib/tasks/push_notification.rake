namespace :push_notification do

  namespace :apn do
    desc 'Init'
    task init: :environment do
      if !Rpush::Apnsp8::App.find_by_name(ENV['APP_NAME']).present?
        app = Rpush::Apnsp8::App.new
        app.name = ENV['APP_NAME']
        app.apn_key = File.read(Rails.root.join(ENV['APN_APP_KEY']))
        app.environment = ENV['APN_ENVIRONMENT'] # APNs environment.
        app.apn_key_id = ENV['APN_KEY_ID']
        app.team_id = ENV['APN_TEAM_ID']
        app.bundle_id = ENV['APN_BUNDLE_ID']
        app.connections = ENV['APN_CONNECTIONS']
        app.save!
      end
    end
  end

  namespace :fcm do
    desc 'Init'
    task init: :environment do
      if !Rpush::Gcm::App.find_by_name(ENV['APP_NAME']).present?
        app = Rpush::Gcm::App.new
        app.name = ENV['APP_NAME']
        app.auth_key = ENV['FCM_AUTH_KEY']
        app.connections = ENV['FCM_CONNECTIONS']
        app.save!
      end
    end
  end

end
