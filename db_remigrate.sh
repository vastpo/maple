#!/bin/bash

bin/rails db:environment:set RAILS_ENV=development
RAILS_ENV=development rake db:drop db:create db:migrate
bin/rails db:environment:set RAILS_ENV=test
RAILS_ENV=test rake db:drop db:create db:migrate
# RAILS_ENV=production rake db:drop db:create db:migrate
